package com.tic.controlador;

import com.tic.carpetas.dto.CarpetaDTO;
import com.tic.carpetas.servicio.CarpetaServicio;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.ejb.EJB;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.pdfbox.pdmodel.PDDocument;

/**
 * Servlet implementation class GetDetails
 */
@WebServlet(name = "FileReadNotarioPdf", urlPatterns = {"/viewpdfnotario"})
public class FileReadNotarioPdf extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @EJB
    private CarpetaServicio carpetaServicio;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileReadNotarioPdf() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("");

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub

        ServletOutputStream sos;

        String parameter = (String) request.getParameter("valor");

        CarpetaDTO documento = carpetaServicio.buscarCarpetaPorNumero(Integer.parseInt(parameter));

        response.setContentType("application/pdf");

        String pdfFileName = documento.getNombrepdfnotario();
        response.setHeader("Content-disposition", "inline; filename=" + pdfFileName);

        String directorio = documento.getRutapdfnotario();
        File pdfFile = new File(directorio + "/" + pdfFileName);

        response.setContentLength((int) pdfFile.length());
        sos = response.getOutputStream();

        PDDocument doc = PDDocument.load(pdfFile);
        int count = doc.getNumberOfPages();
        
        System.out.println(count);

        FileInputStream fileInputStream = new FileInputStream(pdfFile);
        int bytes;
        while ((bytes = fileInputStream.read()) != -1) {
            sos.write(bytes);
        }
        sos.flush();
        sos.close();
    }

}
