/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.controlador;

import com.tic.carpetas.dao.TipoDocumentoDAO;
import com.tic.carpetas.dao.VersionDAO;
import com.tic.carpetas.dominio.TipoDocumento;
import com.tic.carpetas.dto.CarpetaDTO;
import com.tic.carpetas.dto.DocumentoDTO;
import com.tic.carpetas.servicio.CarpetaServicio;
import com.tic.carpetas.servicio.DocumentoServicio;
import com.tic.rbac.servicio.LlavePerfilServicio;
import com.tic.rbac.servicio.UsuarioServicio;
import com.tic.utils.TablaValores;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.pdfbox.pdmodel.PDDocument;

public class FileUploadServlet extends HttpServlet {
    
    @EJB
    private UsuarioServicio usuarioServicio;
    
    @EJB
    private DocumentoServicio documentoServicio;
    
    @EJB
    private CarpetaServicio carpetaServicio;
    
    @EJB
    private VersionDAO versionDao;
  
    @EJB
    private TipoDocumentoDAO tipoDocumentoDao;
    
    @EJB
    private LlavePerfilServicio llavePerfilServicio;
    
    private static final String SAVE_DIR = "/pdf/prueba";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        String fileName = "";
        String creacion = "";
        int doc_id = 0;
        int tipodoc = Integer.parseInt(request.getParameter("tipodoc"));
        int linea = Integer.parseInt(request.getParameter("linea"));
        if (!request.getParameter("doc_id").equals("undefined") && request.getParameter("doc_id") != "") {
            doc_id = Integer.parseInt(request.getParameter("doc_id"));
        }
        String carpeta = request.getParameter("carpeta");
        String caja = request.getParameter("caja");
        String comision = request.getParameter("comision");
        String califica = request.getParameter("califica");
        
        String folioinicial = request.getParameter("folioinicial");
        String foliofinal = request.getParameter("foliofinal");
        String numeropaginas = request.getParameter("numeropaginas");
        String letra_caja = request.getParameter("letra_caja");
        String letra_carpeta = request.getParameter("letra_carpeta");
        String serie = request.getParameter("serie");
        int version_id = Integer.parseInt(request.getParameter("version_id"));
        boolean declara = false;
        if(request.getParameter("declara").equals("true")){
            declara = true;
        }
        boolean preservacion = false;
        String comentario = "";
        String paginaspdf = "0";
        
        if (request.getParameter("preservacion").equals("true")) {
            preservacion = true;
        }
        if (!request.getParameter("paginaspdf").equals("undefined")) {
            paginaspdf = request.getParameter("paginaspdf");
        }
        
        if (!request.getParameter("comentario").equals("undefined")) {
            String item = request.getParameter("comentario");
            byte[] bytes = item.getBytes(StandardCharsets.ISO_8859_1);
            comentario = new String(bytes, StandardCharsets.UTF_8);
//            comentario = request.getParameter("comentario");
        }
        
        try {
            String ruta = TablaValores.getValor("almacenamiento.parametros", "ARCHIVO", "ruta");
            File uploadsRuta = new File(ruta);
            if (!uploadsRuta.exists()) {
                uploadsRuta.mkdir();
            }
            creacion = ruta + "/" + caja + carpeta + comision + califica + letra_caja + letra_carpeta;
            File uploads = new File(creacion);
            if (!uploads.exists()) {
                uploads.mkdir();
            }
            
            File file = null;
            for (Part part : request.getParts()) {
                if (part.getContentType() != null) {
                    fileName = extractFileName(part);
                    file = new File(uploads, fileName);
                    try (InputStream input = part.getInputStream()) {
                        Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    }
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        CarpetaDTO carpetaDTO = new CarpetaDTO();
        carpetaDTO.setCarpeta(Integer.parseInt(carpeta));
        carpetaDTO.setNro_caja(Integer.parseInt(caja));
        carpetaDTO.setComision(Integer.parseInt(comision));
        carpetaDTO.setCalifica(Boolean.parseBoolean(califica));
        carpetaDTO.setLetra_caja(letra_caja);
        carpetaDTO.setLetra_carpeta(letra_carpeta);
        carpetaDTO.setSerie(serie);
        carpetaDTO.setDv('a');
        int idcarpeta = carpetaServicio.crearCarpeta(carpetaDTO);
        if (declara){
            tipodoc = versionDao.obtenerCategoria();
        }
        DocumentoDTO documentoDTO = new DocumentoDTO();
        documentoDTO.setCarpeta_id(idcarpeta);
        documentoDTO.setTipo_id(tipodoc);
        documentoDTO.setFolioInicial(Integer.parseInt(folioinicial));
        documentoDTO.setFolioFinal(Integer.parseInt(foliofinal));
        documentoDTO.setPaginas(Integer.parseInt(numeropaginas));
        documentoDTO.setPreservacion(preservacion);
        documentoDTO.setComentario(comentario);
        documentoDTO.setNumeropdf(Integer.parseInt(paginaspdf));
        documentoDTO.setDocumento_id(doc_id);
        documentoDTO.setLetra_carpeta(letra_carpeta);
        documentoDTO.setLetra_caja(letra_caja);
        
        if (fileName != "") {
            
            documentoDTO.setRutaDocumento(creacion);
            documentoDTO.setNombreDocumento(fileName);
            
            String pdfFileName = documentoDTO.getNombreDocumento();
            response.setHeader("Content-disposition", "inline; filename=" + pdfFileName);
            
            String directorio = documentoDTO.getRutaDocumento();
            File pdfFile = new File(directorio + "/" + pdfFileName);
            
            PDDocument doc = new PDDocument();
            int count = 0;
            try {
                doc = PDDocument.load(pdfFile);
                count = doc.getNumberOfPages();
            } finally {
                if (doc != null) {
                    doc.close();
                }
            }
            documentoDTO.setNumeropdf(count);
            int id = documentoServicio.crearDocumento(documentoDTO);
            documentoServicio.actualizaTotales(idcarpeta);
            if(declara){
                versionDao.cierraEstadoCarpeta(idcarpeta);
                versionDao.cierraEstadoVersion(version_id, fileName, id);
            }
            
            response.getWriter().write(linea + "-" + id + "-" + idcarpeta + "-success-true-" + count + "-false-true");
            
            System.out.println("id_carpeta" + id);
        } else {
            
            documentoDTO.setRutaDocumento("");
            documentoDTO.setNombreDocumento("");
            
            int id = documentoServicio.crearDocumento(documentoDTO);
            documentoServicio.actualizaTotales(idcarpeta);
            
            String pdfFileName = "";
            response.setHeader("Content-disposition", "inline; filename=" + pdfFileName);
            if (Integer.parseInt(paginaspdf) < 1) {
                response.getWriter().write(linea + "-" + id + "-" + idcarpeta + "-success-false-" + "0-true-true" + letra_caja + "-" + letra_carpeta + "-" + serie);
            } else {
                response.getWriter().write(linea + "-" + id + "-" + idcarpeta + "-success-false-" + Integer.parseInt(paginaspdf) + "-false-true" + letra_caja + "-" + letra_carpeta + "-" + serie);
            }
            
            System.out.println("id_carpeta" + id);
            
        }
        
    }
    
    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }
}
