/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.controlador;

import com.tic.carpetas.dominio.CategoriaDocumento;
import com.tic.carpetas.dto.CarpetaDTO;
import com.tic.carpetas.dto.DocumentoDTO;
import com.tic.carpetas.servicio.CarpetaServicio;
import com.tic.carpetas.servicio.CategoriaDocumentoServicio;
import com.tic.carpetas.servicio.DocumentoServicio;
import com.tic.rbac.servicio.LlavePerfilServicio;
import com.tic.rbac.servicio.UsuarioServicio;
import com.tic.utils.TablaValores;
import com.tic.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.pdfbox.pdmodel.PDDocument;

public class FileUploadNotarioServlet extends HttpServlet {

    @EJB
    private UsuarioServicio usuarioServicio;

    @EJB
    private DocumentoServicio documentoServicio;

    @EJB
    private CarpetaServicio carpetaServicio;

    @EJB
    private LlavePerfilServicio llavePerfilServicio;

    private static final String SAVE_DIR = "/pdf/prueba";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        String fileName = "";
        String creacion = "";
        String carpeta = request.getParameter("carpeta");
        String carpetaDocumento = "/" + Utils.eliminaEspacios(carpeta);

        CarpetaDTO carpetaDTO = new CarpetaDTO();
        carpetaDTO.setCarpeta(carpetaServicio.buscarCarpeta(Integer.parseInt(carpeta)).getCarpeta());

        try {
            String ruta = TablaValores.getValor("almacenamiento.parametros", "ARCHIVO", "ruta");
            
            File uploadsRuta = new File(ruta);
            if (!uploadsRuta.exists()) {
                uploadsRuta.mkdir();
            }
            creacion = ruta +"//"+ carpetaDTO.getNro_caja() + carpetaDTO.getCarpeta() + carpetaDTO.getComision() + carpetaDTO.isCalifica();
            File uploads = new File(creacion);
            if (!uploads.exists()) {
                uploads.mkdir();
            }

            File file = null;
            for (Part part : request.getParts()) {
                fileName = extractFileName(part);
                file = new File(uploads, fileName);

                try (InputStream input = part.getInputStream()) {
                    Files.copy(input, file.toPath());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (fileName != "") {

            carpetaDTO.setDv('a');
            carpetaDTO.setRutapdfnotario(creacion);
            carpetaDTO.setNombrepdfnotario(fileName);

            int idcarpeta = carpetaServicio.crearCarpeta(carpetaDTO);

            response.getWriter().write("true-" + carpetaDTO.getCarpeta());

        }

    }

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }
}
