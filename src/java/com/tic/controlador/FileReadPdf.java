package com.tic.controlador;

import com.tic.carpetas.dto.DocumentoDTO;
import com.tic.carpetas.servicio.DocumentoServicio;
import com.tic.utils.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.ejb.EJB;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.pdfbox.pdmodel.PDDocument;

/**
 * Servlet implementation class GetDetails
 */
@WebServlet(name = "FileReadPdf", urlPatterns = {"/viewpdf"})
public class FileReadPdf extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @EJB
    private DocumentoServicio documentoServicio;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileReadPdf() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("");

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub

        ServletOutputStream sos;

        String parameter = (String) request.getParameter("valor");

        DocumentoDTO documento = documentoServicio.buscarDocumento(Integer.parseInt(parameter));

        response.setContentType("application/pdf");

        String pdfFileName = documento.getNombreDocumento();
        response.setHeader("Content-disposition", "inline; filename=" + pdfFileName);

        String directorio = documento.getRutaDocumento();
        File pdfFile = new File(directorio + "/" + pdfFileName);

        response.setContentLength((int) pdfFile.length());
        sos = response.getOutputStream();

        PDDocument doc = PDDocument.load(pdfFile);
        int count = doc.getNumberOfPages();
        
        System.out.println(count);

        FileInputStream fileInputStream = new FileInputStream(pdfFile);
        int bytes;
        while ((bytes = fileInputStream.read()) != -1) {
            sos.write(bytes);
        }
        sos.flush();
        sos.close();
    }

}
