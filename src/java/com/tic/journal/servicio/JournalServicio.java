/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.journal.servicio;

import com.tic.journal.dominio.*;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Rothkegel
 */
@Entity
@Table(name="journal")
public class JournalServicio {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_journal")
    private int id_journal;
    
    @Column(name="id_modulo")
    private int id_modulo;
    
    @Column(name="id_evento")
    private int id_evento;
    
    @Column(name="id_usuario")
    private int id_usuario;
    
    @Column(name="fecha_evento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha_evento;
    
    
    
    
    
}
