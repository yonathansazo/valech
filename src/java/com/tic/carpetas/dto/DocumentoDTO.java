/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dto;

import com.tic.carpetas.dominio.Carpeta;
import com.tic.carpetas.dominio.Documento;

/**
 *
 * @author Rothkegel
 */
public class DocumentoDTO {

    private int carpeta_id;

    private int tipo_id;

    private String nombreTipoDocumento;

    private int documento_id;

    private int paginas;

    private int folioInicial;

    private int folioFinal;

    private String comentario;

    private String nombreDocumento;

    private String rutaDocumento;

    private CarpetaDTO carpetaDTO;

    private boolean activa;
    
    private boolean preservacion;

    private TipoDocumentoDTO tipodocumento;
    
    private String letra_carpeta;

    private String letra_caja;
    
    private int numeropdf;

    public DocumentoDTO() {
    }

    public DocumentoDTO(Documento doc, Carpeta carpeta) {
        System.out.println("DocumentoDTO(Documento doc, Carpeta carpeta)");

        carpeta_id = doc.getCarpeta_id();
        tipo_id = doc.getTipo_id();
        nombreTipoDocumento = doc.getTipodocumento().getNombreTipoDocumento();
        documento_id = doc.getDocumento_id();
        paginas = doc.getPaginas();
        folioInicial = doc.getFolioInicial();
        folioFinal = doc.getFolioFinal();
        comentario = doc.getComentario();
        nombreDocumento = doc.getNombreDocumento();
        rutaDocumento = doc.getRutaDocumento();
        preservacion = doc.isPreservacion();
        numeropdf = doc.getNumeropdf();
        letra_carpeta = doc.getLetra_carpeta();
        letra_caja = doc.getLetra_caja();

        
    }

    public DocumentoDTO(Documento doc) {
        System.out.println("DocumentoDTO(Documento doc)");
        carpeta_id = doc.getCarpeta_id();
        tipo_id = doc.getTipo_id();
        documento_id = doc.getDocumento_id();
        paginas = doc.getPaginas();
        folioInicial = doc.getFolioInicial();
        folioFinal = doc.getFolioFinal();
        comentario = doc.getComentario();
        nombreDocumento = doc.getNombreDocumento();
        rutaDocumento = doc.getRutaDocumento();
        nombreTipoDocumento = doc.getTipodocumento().getNombreTipoDocumento();
        preservacion = doc.isPreservacion();
        numeropdf = doc.getNumeropdf();
        letra_carpeta = doc.getLetra_carpeta();
        letra_caja = doc.getLetra_caja();

    }

    public DocumentoDTO(int carpeta_id, int tipo_id, String nombreTipoDocumento, int documento_id, int paginas, int folioInicial, int folioFinal, String comentario, String nombreDocumento, String rutaDocumento, CarpetaDTO carpetaDTO, boolean activa, boolean preservacion, TipoDocumentoDTO tipodocumento, int numeropdf, String letra_carpeta, String letra_caja) {
        this.carpeta_id = carpeta_id;
        this.tipo_id = tipo_id;
        this.nombreTipoDocumento = nombreTipoDocumento;
        this.documento_id = documento_id;
        this.paginas = paginas;
        this.folioInicial = folioInicial;
        this.folioFinal = folioFinal;
        this.comentario = comentario;
        this.nombreDocumento = nombreDocumento;
        this.rutaDocumento = rutaDocumento;
        this.carpetaDTO = carpetaDTO;
        this.activa = activa;
        this.preservacion = preservacion;
        this.tipodocumento = tipodocumento;
        this.numeropdf = numeropdf;
        this.letra_carpeta = letra_carpeta;
        this.letra_caja = letra_caja;
    }

    public String getLetra_carpeta() {
        return letra_carpeta;
    }

    public void setLetra_carpeta(String letra_carpeta) {
        this.letra_carpeta = letra_carpeta;
    }

    public String getLetra_caja() {
        return letra_caja;
    }

    public void setLetra_caja(String letra_caja) {
        this.letra_caja = letra_caja;
    }
    
    public int getCarpeta_id() {
        return carpeta_id;
    }

    public void setCarpeta_id(int carpeta_id) {
        this.carpeta_id = carpeta_id;
    }

    public int getTipo_id() {
        return tipo_id;
    }

    public void setTipo_id(int tipo_id) {
        this.tipo_id = tipo_id;
    }

    public int getDocumento_id() {
        return documento_id;
    }

    public void setDocumento_id(int documento_id) {
        this.documento_id = documento_id;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    public int getFolioInicial() {
        return folioInicial;
    }

    public void setFolioInicial(int folioInicial) {
        this.folioInicial = folioInicial;
    }

    public int getFolioFinal() {
        return folioFinal;
    }

    public void setFolioFinal(int folioFinal) {
        this.folioFinal = folioFinal;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public String getRutaDocumento() {
        return rutaDocumento;
    }

    public void setRutaDocumento(String rutaDocumento) {
        this.rutaDocumento = rutaDocumento;
    }

    public CarpetaDTO getCarpeta() {
        return carpetaDTO;
    }

    public void setCarpeta(CarpetaDTO carpetaDTO) {
        this.carpetaDTO = carpetaDTO;
    }

    public boolean isActiva() {
        return activa;
    }

    public void setActiva(boolean activa) {
        this.activa = activa;
    }

    public TipoDocumentoDTO getTipodocumento() {
        return tipodocumento;
    }

    public void setTipodocumento(TipoDocumentoDTO tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    public String getNombreTipoDocumento() {
        return nombreTipoDocumento;
    }

    public void setNombreTipoDocumento(String nombreTipoDocumento) {
        this.nombreTipoDocumento = nombreTipoDocumento;
    }

    public CarpetaDTO getCarpetaDTO() {
        return carpetaDTO;
    }

    public void setCarpetaDTO(CarpetaDTO carpetaDTO) {
        this.carpetaDTO = carpetaDTO;
    }

    public boolean isPreservacion() {
        return preservacion;
    }

    public void setPreservacion(boolean preservacion) {
        this.preservacion = preservacion;
    }

    public int getNumeropdf() {
        return numeropdf;
    }

    public void setNumeropdf(int numeropdf) {
        this.numeropdf = numeropdf;
    }
}
