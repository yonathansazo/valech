/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dto;

import com.tic.carpetas.dominio.TipoDocumento;
import java.util.List;

/**
 *
 * @author Rothkegel
 */
public class CategoriaDocumentoDTO {
    
    
    private int categoria_id;
    
    private String nombreCategoria;

    private List<TipoDocumentoDTO> tipodocumentos;

    public CategoriaDocumentoDTO() {
    }

    public CategoriaDocumentoDTO(int categoria_id, String nombreCategoria, List<TipoDocumentoDTO> tipodocumentos) {
        this.categoria_id = categoria_id;
        this.nombreCategoria = nombreCategoria;
        this.tipodocumentos = tipodocumentos;
    }
    

    public int getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(int categoria_id) {
        this.categoria_id = categoria_id;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public List<TipoDocumentoDTO> getTipodocumentos() {
        return tipodocumentos;
    }

    public void setTipodocumentos(List<TipoDocumentoDTO> tipodocumentos) {
        this.tipodocumentos = tipodocumentos;
    }

    

}
