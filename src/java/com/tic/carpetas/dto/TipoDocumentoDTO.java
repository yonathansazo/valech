/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dto;

import com.tic.carpetas.dominio.CategoriaDocumento;
import com.tic.carpetas.dominio.TipoDocumento;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rothkegel
 */
public class TipoDocumentoDTO {
    
    private int tipo_id;
    
    private String nombreTipoDocumento;
    
    private List<DocumentoDTO> documentos;
    
    private int categoria_id;
    
    private String nombreCategoria;

    public TipoDocumentoDTO() {
    }
    
    public TipoDocumentoDTO(TipoDocumento tipo, CategoriaDocumento cat) {
        this.tipo_id = tipo.getTipo_id();
        this.nombreTipoDocumento = tipo.getNombreTipoDocumento();
        this.documentos = new ArrayList<DocumentoDTO>();
        this.categoria_id = cat.getCategoria_id();
        this.nombreCategoria = cat.getNombreCategoria();
    }

    public TipoDocumentoDTO(int tipo_id, String nombreTipoDocumento, List<DocumentoDTO> documentos, int categoria_id, String nombreCategoria) {
        this.tipo_id = tipo_id;
        this.nombreTipoDocumento = nombreTipoDocumento;
        this.documentos = documentos;
        this.categoria_id = categoria_id;
        this.nombreCategoria = nombreCategoria;
    }

    

    public int getTipo_id() {
        return tipo_id;
    }

    public void setTipo_id(int tipo_id) {
        this.tipo_id = tipo_id;
    }

    public String getNombreTipoDocumento() {
        return nombreTipoDocumento;
    }

    public void setNombreTipoDocumento(String nombreTipoDocumento) {
        this.nombreTipoDocumento = nombreTipoDocumento;
    }

    public List<DocumentoDTO> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<DocumentoDTO> documentos) {
        this.documentos = documentos;
    }

    public int getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(int categoria_id) {
        this.categoria_id = categoria_id;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }


}
