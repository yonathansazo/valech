/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dto;

import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author jsilva
 */
public class VersionDTO {
    
    private int version_id;
    
    private List<VersionDocumentoDTO> versiondocumento;
    
    private String nro_interno;

    private int caja;

    private int carpeta;

    private String letra_caja;

    private String letra_carpeta;

    private Timestamp fecha;

    private String estado;

    private String declaracion;
    
    private int carpeta_id;
    
    private String nombre;
    
    private String rut;
    
    private String identificador;
    
    private String encabezado;
    
    private String conclusion;

    private String tipo_carpeta;
    
    private String serie;

    private int comision;

    private boolean califica;

    private boolean apela;
    
    private int documento_id;
    
    private int numeropaginas;
    
    public VersionDTO() {
    }

    public VersionDTO(int version_id, List<VersionDocumentoDTO> versiondocumento, String nro_interno, int caja, int carpeta, String letra_caja, String letra_carpeta, Timestamp fecha, String estado, String declaracion, int carpeta_id, String nombre, String rut, String identificador, String encabezado, String conclusion, String tipo_carpeta, String serie, int comision, boolean califica, boolean apela, int documento_id) {
        this.version_id = version_id;
        this.versiondocumento = versiondocumento;
        this.nro_interno = nro_interno;
        this.caja = caja;
        this.carpeta = carpeta;
        this.letra_caja = letra_caja;
        this.letra_carpeta = letra_carpeta;
        this.fecha = fecha;
        this.estado = estado;
        this.declaracion = declaracion;
        this.carpeta_id = carpeta_id;
        this.nombre = nombre;
        this.rut = rut;
        this.identificador = identificador;
        this.encabezado = encabezado;
        this.conclusion = conclusion;
        this.tipo_carpeta = tipo_carpeta;
        this.serie = serie;
        this.comision = comision;
        this.califica = califica;
        this.apela = apela;
        this.documento_id = documento_id;
        this.numeropaginas = 0;
    }

    public VersionDTO(int version_id, List<VersionDocumentoDTO> versiondocumento, String nro_interno, int caja, int carpeta, String letra_caja, String letra_carpeta, Timestamp fecha, String estado, String declaracion, int carpeta_id, String nombre, String rut, String identificador, String encabezado, String conclusion, String tipo_carpeta, String serie, int comision, boolean califica, boolean apela, int documento_id, int numeropaginas) {
        this.version_id = version_id;
        this.versiondocumento = versiondocumento;
        this.nro_interno = nro_interno;
        this.caja = caja;
        this.carpeta = carpeta;
        this.letra_caja = letra_caja;
        this.letra_carpeta = letra_carpeta;
        this.fecha = fecha;
        this.estado = estado;
        this.declaracion = declaracion;
        this.carpeta_id = carpeta_id;
        this.nombre = nombre;
        this.rut = rut;
        this.identificador = identificador;
        this.encabezado = encabezado;
        this.conclusion = conclusion;
        this.tipo_carpeta = tipo_carpeta;
        this.serie = serie;
        this.comision = comision;
        this.califica = califica;
        this.apela = apela;
        this.documento_id = documento_id;
        this.numeropaginas = numeropaginas;
    }

    public int getNumeropaginas() {
        return numeropaginas;
    }

    public void setNumeropaginas(int numeropaginas) {
        this.numeropaginas = numeropaginas;
    }
    
    public int getDocumento_id() {
        return documento_id;
    }

    public void setDocumento_id(int documento_id) {
        this.documento_id = documento_id;
    }

    public String getTipo_carpeta() {
        return tipo_carpeta;
    }

    public void setTipo_carpeta(String tipo_carpeta) {
        this.tipo_carpeta = tipo_carpeta;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getComision() {
        return comision;
    }

    public void setComision(int comision) {
        this.comision = comision;
    }

    public boolean isCalifica() {
        return califica;
    }

    public void setCalifica(boolean califica) {
        this.califica = califica;
    }

    public boolean isApela() {
        return apela;
    }

    public void setApela(boolean apela) {
        this.apela = apela;
    }
    
    public List<VersionDocumentoDTO> getVersiondocumento() {
        return versiondocumento;
    }

    public void setVersiondocumento(List<VersionDocumentoDTO> versiondocumento) {
        this.versiondocumento = versiondocumento;
    }

    public String getNro_interno() {
        return nro_interno;
    }

    public void setNro_interno(String nro_interno) {
        this.nro_interno = nro_interno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getEncabezado() {
        return encabezado;
    }

    public void setEncabezado(String encabezado) {
        this.encabezado = encabezado;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public int getCarpeta_id() {
        return carpeta_id;
    }

    public void setCarpeta_id(int carpeta_id) {
        this.carpeta_id = carpeta_id;
    }
    
    public int getVersion_id() {
        return version_id;
    }

    public void setVersion_id(int version_id) {
        this.version_id = version_id;
    }

    public int getCaja() {
        return caja;
    }

    public void setCaja(int caja) {
        this.caja = caja;
    }

    public int getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(int carpeta) {
        this.carpeta = carpeta;
    }

    public String getLetra_caja() {
        return letra_caja;
    }

    public void setLetra_caja(String letra_caja) {
        this.letra_caja = letra_caja;
    }

    public String getLetra_carpeta() {
        return letra_carpeta;
    }

    public void setLetra_carpeta(String letra_carpeta) {
        this.letra_carpeta = letra_carpeta;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDeclaracion() {
        return declaracion;
    }

    public void setDeclaracion(String declaracion) {
        this.declaracion = declaracion;
    }
    
}
