/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dto;

import java.util.List;

/**
 *
 * @author Rothkegel
 */
public class CarpetaDTO {
    
    private int carpeta_id;
    
    private List<DocumentoDTO> documentos;
    
    private int nro_caja;
    
    private int carpeta;
    
    private String nombres;
    
    private String apellidos;
    
    private int comision;
    
    private boolean califica;
    
    private boolean apela;

    private String rut;
    
    private char dv;
    
    private String identificador;
    
    private int numeropdf;
    
    private int numerodocumentos;
    
    private int numeropaginas;
    
    private String rutapdfnotario;
    
    private String nombrepdfnotario;
    
    private String tipo_carpeta;
    
    private String serie;
    
    private String letra_carpeta;
    
    private String letra_caja;
    
    private String declara;
    
    private String tipo_id;

    public CarpetaDTO() {
    }

    public CarpetaDTO(int carpeta_id, List<DocumentoDTO> documentos, int nro_caja, int carpeta, String nombres, String apellidos, int comision, boolean califica, boolean apela, String rut, char dv, String identificador, int numeropdf, int numerodocumentos, int numeropaginas, String rutapdfnotario, String nombrepdfnotario, String tipo_carpeta, String serie, String letra_carpeta, String letra_caja, String declara) {
        this.carpeta_id = carpeta_id;
        this.documentos = documentos;
        this.nro_caja = nro_caja;
        this.carpeta = carpeta;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.comision = comision;
        this.califica = califica;
        this.apela = apela;
        this.rut = rut;
        this.dv = dv;
        this.identificador = identificador;
        this.numeropdf = numeropdf;
        this.numerodocumentos = numerodocumentos;
        this.numeropaginas = numeropaginas;
        this.rutapdfnotario = rutapdfnotario;
        this.nombrepdfnotario = nombrepdfnotario;
        this.tipo_carpeta = tipo_carpeta;
        this.serie = serie;
        this.letra_carpeta = letra_carpeta;
        this.letra_caja = letra_caja;
        this.declara = declara;
        this.tipo_id = null;
    }

    
    public CarpetaDTO(int carpeta_id, List<DocumentoDTO> documentos, int nro_caja, int carpeta, String nombres, String apellidos, int comision, boolean califica, boolean apela, String rut, char dv, String identificador, int numeropdf, int numerodocumentos, int numeropaginas, String rutapdfnotario, String nombrepdfnotario, String tipo_carpeta, String serie, String letra_carpeta, String letra_caja, String declara, String tipo_id) {
        this.carpeta_id = carpeta_id;
        this.documentos = documentos;
        this.nro_caja = nro_caja;
        this.carpeta = carpeta;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.comision = comision;
        this.califica = califica;
        this.apela = apela;
        this.rut = rut;
        this.dv = dv;
        this.identificador = identificador;
        this.numeropdf = numeropdf;
        this.numerodocumentos = numerodocumentos;
        this.numeropaginas = numeropaginas;
        this.rutapdfnotario = rutapdfnotario;
        this.nombrepdfnotario = nombrepdfnotario;
        this.tipo_carpeta = tipo_carpeta;
        this.serie = serie;
        this.letra_carpeta = letra_carpeta;
        this.letra_caja = letra_caja;
        this.declara = declara;
        this.tipo_id = tipo_id;
    }

    public String getTipo_id() {
        return tipo_id;
    }

    public void setTipo_id(String tipo_id) {
        this.tipo_id = tipo_id;
    }
    
    public String getDeclara() {
        return declara;
    }

    public void setDeclara(String declara) {
        this.declara = declara;
    }

    public String getLetra_caja() {
        return letra_caja;
    }

    public void setLetra_caja(String letra_caja) {
        this.letra_caja = letra_caja;
    }

    public String getTipo_carpeta() {
        return tipo_carpeta;
    }

    public void setTipo_carpeta(String tipo_carpeta) {
        this.tipo_carpeta = tipo_carpeta;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getLetra_carpeta() {
        return letra_carpeta;
    }

    public void setLetra_carpeta(String letra_carpeta) {
        this.letra_carpeta = letra_carpeta;
    }

    public int getCarpeta_id() {
        return carpeta_id;
    }

    public void setCarpeta_id(int carpeta_id) {
        this.carpeta_id = carpeta_id;
    }

    public List<DocumentoDTO> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<DocumentoDTO> documentos) {
        this.documentos = documentos;
    }

    public int getNro_caja() {
        return nro_caja;
    }

    public void setNro_caja(int nro_caja) {
        this.nro_caja = nro_caja;
    }

    public int getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(int carpeta) {
        this.carpeta = carpeta;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getComision() {
        return comision;
    }

    public void setComision(int comision) {
        this.comision = comision;
    }

    public boolean isCalifica() {
        return califica;
    }

    public void setCalifica(boolean califica) {
        this.califica = califica;
    }

    public boolean isApela() {
        return apela;
    }

    public void setApela(boolean apela) {
        this.apela = apela;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public char getDv() {
        return dv;
    }

    public void setDv(char dv) {
        this.dv = dv;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public int getNumeropdf() {
        return numeropdf;
    }

    public void setNumeropdf(int numeropdf) {
        this.numeropdf = numeropdf;
    }

    public int getNumerodocumentos() {
        return numerodocumentos;
    }

    public void setNumerodocumentos(int numerodocumentos) {
        this.numerodocumentos = numerodocumentos;
    }

    public int getNumeropaginas() {
        return numeropaginas;
    }

    public void setNumeropaginas(int numeropaginas) {
        this.numeropaginas = numeropaginas;
    }

    public String getRutapdfnotario() {
        return rutapdfnotario;
    }

    public void setRutapdfnotario(String rutapdfnotario) {
        this.rutapdfnotario = rutapdfnotario;
    }

    public String getNombrepdfnotario() {
        return nombrepdfnotario;
    }

    public void setNombrepdfnotario(String nombrepdfnotario) {
        this.nombrepdfnotario = nombrepdfnotario;
    }
    
    

}
