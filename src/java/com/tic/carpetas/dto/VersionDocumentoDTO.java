/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dto;

/**
 *
 * @author jsilva
 */
public class VersionDocumentoDTO {
    
    private int version_documento_id;
    
    private int version_id;
    
    private int documento_id;
    
    private String identificador;
    
    private String nombre_documento;
    
    private int pagina;
        
    private String ruta_documento;

    public VersionDocumentoDTO() {
    }

    public VersionDocumentoDTO(int version_documento_id, int version_id, int documento_id, String identificador, String nombre_documento, int pagina, String ruta_documento) {
        this.version_documento_id = version_documento_id;
        this.version_id = version_id;
        this.documento_id = documento_id;
        this.identificador = identificador;
        this.nombre_documento = nombre_documento;
        this.pagina = pagina;
        this.ruta_documento = ruta_documento;
    }

    public int getVersion_documento_id() {
        return version_documento_id;
    }

    public void setVersion_documento_id(int version_documento_id) {
        this.version_documento_id = version_documento_id;
    }

    public int getVersion_id() {
        return version_id;
    }

    public void setVersion_id(int version_id) {
        this.version_id = version_id;
    }

    public int getDocumento_id() {
        return documento_id;
    }

    public void setDocumento_id(int documento_id) {
        this.documento_id = documento_id;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getNombre_documento() {
        return nombre_documento;
    }

    public void setNombre_documento(String nombre_docuemnto) {
        this.nombre_documento = nombre_docuemnto;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }

    public String getRuta_documento() {
        return ruta_documento;
    }

    public void setRuta_documento(String ruta_documento) {
        this.ruta_documento = ruta_documento;
    }

}
