/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dto;

/**
 *
 * @author soporte
 */
public class ArregloLlaveValor {
    
    String llave;
    String valor;

    public ArregloLlaveValor() {}

    public ArregloLlaveValor(String llave, String valor)
    {
      this.llave = llave;
      this.valor = valor;
    }

    public String getLlave()
    {
      return llave;
    }

    public void setLlave(String llave)
    {
      this.llave = llave;
    }

    public String getValor()
    {
      return valor;
    }

    public void setValor(String valor)
    {
      this.valor = valor;
    }
    
}
