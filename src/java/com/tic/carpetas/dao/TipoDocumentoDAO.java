/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dao;

import com.tic.carpetas.dominio.TipoDocumento;
import com.tic.carpetas.dto.TipoDocumentoDTO;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Rothkegel
 */
@Stateless
public class TipoDocumentoDAO {

    @PersistenceContext
    private EntityManager em;

    public int crearTipoDocumento(TipoDocumento tipoDocumento) {
        em.persist(tipoDocumento);
        em.flush();
        return tipoDocumento.getTipo_id();
    }

    public TipoDocumento buscarTipoDocumento(int id) {
        TipoDocumento p = em.find(TipoDocumento.class, id);
        return p;
    }

    public void actualizarTipoDocumento(TipoDocumento tipoDocumento) {
        em.merge(tipoDocumento);
    }

    public void eliminarTipoDocumento(int id) {
        em.remove(buscarTipoDocumento(id));
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<TipoDocumento> listarTipoDocumentos() {
        List<TipoDocumento> lista = em.createNamedQuery("TipoDocumento.buscarTodos", TipoDocumento.class)
                .getResultList();
        return lista;
    }

    public void insertarTipoDocumentos(TipoDocumentoDTO dto) {
        em.createNativeQuery("INSERT INTO tipodocumento(nombre_tipo_documento, categoria_id) VALUES ('" 
                + dto.getNombreTipoDocumento() + "', " +dto.getCategoria_id()+ ")")
                .executeUpdate();
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<TipoDocumentoDTO> listarTodosTipoDocumentos() {
        List<TipoDocumentoDTO> lista = em.createNamedQuery("TipoDocumento.buscarTodosTipoDocumento", TipoDocumentoDTO.class)
                .getResultList();
        return lista;
    }
    
    public void eliminarPorId(int tipo_id) {
        em.createNamedQuery("TipoDocumento.eliminarPorId", TipoDocumentoDTO.class).setParameter("tipo_id", tipo_id)
                .executeUpdate();
    }
    
    public void eliminarTipoPorCategoriaId(int tipo_id) {
        em.createNativeQuery("DELETE FROM TipoDocumento td WHERE "
                    + " td.categoria_id = " + tipo_id).executeUpdate();
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<TipoDocumento> buscarTodosTipoDocumentoPorTexto(String texto) {
        List<TipoDocumento> lista = em.createNamedQuery("TipoDocumento.buscarTodosTipoDocumentoPorTexto", TipoDocumento.class).setParameter("nombre_tipo_documento", "%" + texto + "%")
                .getResultList();
        return lista;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<TipoDocumento> getTipoDocumentoPorIdCategoria(int categoria_id) {
        List<TipoDocumento> lista = em.createNamedQuery("TipoDocumento.getTipoDocumentoPorIdCategoria", TipoDocumento.class).setParameter("categoria_id", categoria_id)
                .getResultList();
        return lista;
    }

}
