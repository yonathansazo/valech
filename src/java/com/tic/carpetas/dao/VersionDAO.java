/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dao;

import com.tic.carpetas.dominio.Carpeta;
import com.tic.carpetas.dominio.Version;
import com.tic.carpetas.dto.ArregloLlaveValor;
import com.tic.carpetas.dto.VersionDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jsilva
 */
@Stateless
public class VersionDAO {
    
     @PersistenceContext
    private EntityManager em;

    public int crearVersion(Version version) {

        em.persist(version);
        em.flush();
        return version.getVersion_id();

    }

    public Version buscarVersion(int id) {

        Version version = em.find(Version.class, id);
        return version;

    }
    
    public List<Version> listarVersionesCarpeta(int id) {
        String query = "SELECT * FROM public.version where carpeta_id=" + id;
        List<Version> lista = em.createNativeQuery(query, Version.class).getResultList();
        return lista;
    }
    
     public void cierraEstadoCarpeta(int id) {
         String query = "UPDATE public.carpeta set declara='Terminada' where carpeta_id=" + id;
         em.createNativeQuery(query).executeUpdate();
     }
     
     public void setEstadoCarpeta(int id) {
         String query = "UPDATE public.carpeta set declara='Iniciada' where carpeta_id=" + id;
         em.createNativeQuery(query).executeUpdate();
     }
     
     public void setEstadoCarpetaDVersion(int id) {
        
         String query;
         String query1 = "select carpeta_id from public.version where version_id=" + id;
          long carpeta = ((Number)em.createNativeQuery(query1).getSingleResult()).longValue();
         String query2 = "select count(1) from public.version where carpeta_id=" + carpeta;
          long cont = ((Number)em.createNativeQuery(query2).getSingleResult()).longValue();
         if (cont > 2) {
             query = "UPDATE public.carpeta set declara='Terminada' where carpeta_id=" + carpeta;
         }else {
             query = "UPDATE public.carpeta set declara=null where carpeta_id=" + carpeta;
         }
         em.createNativeQuery(query).executeUpdate();
         
         
     }
     
     public void cierraEstadoVersion(int id, String name, int id_doc) {
         String query = "UPDATE public.version set estado = 'Terminada', declaracion='" + name + "', documento_id=" + id_doc + " where version_id=" + id;
         em.createNativeQuery(query).executeUpdate();
     }
     
    
     public List<Version> listarVersiones() {
        String query = "SELECT * FROM public.version";
        List<Version> lista = em.createNativeQuery(query, Version.class).getResultList();
        return lista;
    }
            
    public int actualizarVersion(Version version) {

        em.merge(version);
        em.flush();
        return version.getVersion_id();

    }
    
    public List<Version> listarCarpetaPaginada(VersionDTO version, int cantidadPorPagina, int numeroPagina, String order) {
        int nrocarpeta = 0;
        int caja = 0;
        int comision = 0;
        boolean califica = false;
        String nombre = "";
        String apellido = "";
        String rut = "";

        String query = "SELECT * FROM public.version ";

        List<ArregloLlaveValor> campos = new ArrayList();
        if (version.getSerie()!= null) {
            campos.add(new ArregloLlaveValor("serie", " = '" + version.getSerie() + "'"));
        }
        if (version.getComision()!= 0) {
            campos.add(new ArregloLlaveValor("comision", " = " + version.getComision()));
        }
        if (version.getCaja()!= 0) {
            campos.add(new ArregloLlaveValor("caja", " = " + version.getCaja()));
        }
        if (version.getLetra_caja()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(letra_caja)", " like LOWER('%" + version.getLetra_caja()+ "%')"));
        }
        if (version.getCarpeta()!= 0) {
            campos.add(new ArregloLlaveValor("carpeta", " = " + version.getCarpeta()));
        }
        if (version.getLetra_carpeta()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(letra_carpeta)", " like LOWER('%" + version.getLetra_carpeta()+ "%')"));
        }
        if (version.getNombre()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(nombre)", " like LOWER('%" + version.getNombre() + "%')"));
        }
        if (version.getNro_interno()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(nro_interno)", " like LOWER('%" + version.getNro_interno() + "%')"));
        }
        if (version.getRut() != null) {
            campos.add(new ArregloLlaveValor("LOWER(rut)", " like LOWER('%" + version.getRut() + "%')"));
        }
        if (version.getTipo_carpeta()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(tipo_carpeta)", " like LOWER('%" + version.getTipo_carpeta()+ "%')"));
        }
        if (version.getIdentificador()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(identificador)", " like LOWER('%" + version.getIdentificador() + "%')"));
        }
        if (version.getEstado()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(estado)", " like LOWER('%" + version.getEstado() + "%')"));
        }
        for (int i = 0; i < campos.size(); i++) {
            if (i == 0) {
                query = query + "Where ";
            }
            query = query + ((ArregloLlaveValor) campos.get(i)).getLlave() + ((ArregloLlaveValor) campos.get(i)).getValor();
            if ((campos.size() > 1) && (i < campos.size() - 1)) {
                query = query + " AND ";
            }
        }
        System.out.println(query);
        List<Version> lista = em.createNativeQuery(query + "" + " LIMIT " + cantidadPorPagina + " OFFSET (" + cantidadPorPagina + " * (" + numeroPagina + "-1));", Version.class).getResultList();
        System.out.println(lista);
        return lista;
    }

    public VersionDTO listarPaginacion(VersionDTO version, int cantidadPorPagina) {
        int nrocarpeta = 0;
        int caja = 0;
        int comision = 0;
        boolean califica = false;
        String nombre = "";
        String apellido = "";
        String rut = "";
        String letra_caja = "";
        String letra_carpeta = "";
        String tipo_carpeta = "";
        String serie = "";

        String query = "SELECT count(1) FROM public.version ";

        List<ArregloLlaveValor> campos = new ArrayList();
 if (version.getSerie()!= null) {
            campos.add(new ArregloLlaveValor("serie", " = '" + version.getSerie() + "'"));
        }
        if (version.getComision()!= 0) {
            campos.add(new ArregloLlaveValor("comision", " = " + version.getComision()));
        }
        if (version.getCaja()!= 0) {
            campos.add(new ArregloLlaveValor("caja", " = " + version.getCaja()));
        }
        if (version.getLetra_caja()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(letra_caja)", " like LOWER('%" + version.getLetra_caja()+ "%')"));
        }
        if (version.getCarpeta()!= 0) {
            campos.add(new ArregloLlaveValor("carpeta", " = " + version.getCarpeta()));
        }
        if (version.getLetra_carpeta()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(letra_carpeta)", " like LOWER('%" + version.getLetra_carpeta()+ "%')"));
        }
        if (version.getNombre()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(nombre)", " like LOWER('%" + version.getNombre() + "%')"));
        }
        if (version.getNro_interno()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(nro_interno)", " like LOWER('%" + version.getNro_interno() + "%')"));
        }
        if (version.getRut() != null) {
            campos.add(new ArregloLlaveValor("LOWER(rut)", " like LOWER('%" + version.getRut() + "%')"));
        }
        if (version.getTipo_carpeta()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(tipo_carpeta)", " like LOWER('%" + version.getTipo_carpeta()+ "%')"));
        }
        if (version.getIdentificador()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(identificador)", " like LOWER('%" + version.getIdentificador() + "%')"));
        }
        if (version.getEstado()!= null) {
            campos.add(new ArregloLlaveValor("LOWER(estado)", " like LOWER('%" + version.getEstado() + "%')"));
        }
        for (int i = 0; i < campos.size(); i++) {
            if (i == 0) {
                query = query + "Where ";
            }
            query = query + ((ArregloLlaveValor) campos.get(i)).getLlave() + ((ArregloLlaveValor) campos.get(i)).getValor();
            if ((campos.size() > 1) && (i < campos.size() - 1)) {
                query = query + " AND ";
            }
        }
        System.out.println(query);
        Long cantidadRegistros = (Long) em.createNativeQuery(query).getSingleResult();

        double numeroPaginas = cantidadRegistros.longValue() / cantidadPorPagina;
        numeroPaginas += (cantidadRegistros.longValue() % cantidadPorPagina > 0L ? 1.0D : 0.0D);
        version.setNumeropaginas((int)numeroPaginas);
        return version;
    }
    
    public void eliminarVersionPorVersionId (int version_id){
        em.createNativeQuery("Delete from public.version where version_id  = " + version_id + "").executeUpdate();
    }
    
    public int obtenerCategoria () {
        int cont = (int)((Number) em.createNativeQuery("select tipo_id from public.tipodocumento where nombre_tipo_documento = 'Declaracion'").getSingleResult()).longValue();
        return cont;
    }
    
}
