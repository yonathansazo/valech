/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dao;

import com.tic.carpetas.dominio.Carpeta;
import com.tic.carpetas.dominio.Documento;
import com.tic.carpetas.dto.DocumentoDTO;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author Rothkegel
 */
@Stateless
public class DocumentoDAO {

    @PersistenceContext
    private EntityManager em;

    public int crearDocumento(Documento documento) {

        em.persist(documento);
        em.flush();
        return documento.getDocumento_id();

    }

    public Documento buscarDocumento(int id) {

        Documento documento = em.find(Documento.class, id);
        return documento;

    }

    public Documento actualizarDocumento(Documento documento) {
        if (documento.getRutaDocumento().equals("") && documento.getNombreDocumento().equals("")) {
            Documento doc = this.buscarDocumentoPorId(documento.getDocumento_id());
            try {
                doc.getRutaDocumento().equals("");
            } catch (NullPointerException e) {
                doc.setRutaDocumento("");
                doc.setNombreDocumento("");
            }
            
            if((!doc.getRutaDocumento().equals("") ) && !doc.getNombreDocumento().equals("")){
                documento.setNombreDocumento(doc.getNombreDocumento());
                documento.setRutaDocumento(doc.getRutaDocumento());
                documento.setNumeropdf(doc.getNumeropdf());
            }
        }
        
        em.createNativeQuery("UPDATE documento SET tipo_id=" + documento.getTipo_id()
                + ", carpeta_id=" + documento.getCarpeta_id()
                + ", activa=" + documento.isActiva()
                + ", comentario='" + documento.getComentario()
                + "', folio_final=" + documento.getFolioFinal()
                + ", folio_inicial=" + documento.getFolioInicial()
                + ", nombre_documento='" + documento.getNombreDocumento()
                + "', numero_pdf=" + documento.getNumeropdf()
                + ", paginas=" + documento.getPaginas()
                + ", preservacion=" + documento.isPreservacion()
                + ", ruta_documento='" + documento.getRutaDocumento()
                + "' WHERE documento_id = " + documento.getDocumento_id()).executeUpdate();
        em.flush();
//        em.merge(documento);
//        em.flush();

        return documento;
    }

    public void actualizarDocumentoNative(Documento documento) {
        em.createNativeQuery("UPDATE documento SET tipo_id=" + documento.getTipo_id()
                + ", carpeta_id=" + documento.getCarpeta_id()
                + ", activa=" + documento.isActiva()
                + ", comentario='" + documento.getComentario()
                + "', folio_final=" + documento.getFolioFinal()
                + ", folio_inicial=" + documento.getFolioInicial()
                + ", nombre_documento='" + documento.getNombreDocumento()
                + "', numero_pdf=" + documento.getNumeropdf()
                + ", paginas=" + documento.getPaginas()
                + ", preservacion=" + documento.isPreservacion()
                + ", ruta_documento='" + documento.getRutaDocumento()
                + "' WHERE documento_id = " + documento.getDocumento_id()).executeUpdate();
        System.out.println("documento comentario " + documento.getComentario());
        em.flush();

    }

    public void eliminarDocumento(int id) {
        em.remove(buscarDocumentoPorId(id));
//        this.eliminarDocumentoId(id);

    }

    public List<Documento> listarDocumentos() {

        List<Documento> lista = em
                .createQuery("SELECT doc FROM Documento doc ORDER BY doc.folioInicial")
                .getResultList();
        return lista;

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Documento> listarDocumentosPorCarpeta(int carpeta_id) {
        try {
            return em.createNamedQuery("Documento.buscarTodosPorCarpeta", Documento.class).setParameter("carpeta_id", carpeta_id)
                    .getResultList();

        } catch (IllegalArgumentException e) {
            // Answer:
            e.getCause().printStackTrace();
            return null;
        } catch (Exception e) {
            // generic exception handling
            e.getCause().printStackTrace();
            return null;
        }

    }

    public List<Documento> listarDocumentosPorCarpetaNativa(int carpeta_id) {
        return (List<Documento>) em.createNativeQuery("SELECT tipo_id, documento_id, carpeta_id, "
                + "activa, comentario, folio_final, folio_inicial, nombre_documento, "
                + "paginas, ruta_documento FROM documento WHERE  "
                + "carpeta_id =  " + carpeta_id, Documento.class).getResultList();

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Documento> buscarDocumentoPorCarpetaNativa(int carpeta_id) {

        return em.createNativeQuery("SELECT tipo_id, documento_id, carpeta_id, activa, comentario, "
                + "folio_final, folio_inicial, nombre_documento, numero_pdf, paginas, preservacion, "
                + "ruta_documento FROM documento WHERE  carpeta_id =  " + carpeta_id + " ORDER BY folio_inicial ASC", Documento.class).getResultList();

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public DocumentoDTO buscarDocumentoDtoPorId(int id) {
        return (DocumentoDTO) em.createNamedQuery("Documento.buscarDocumentoDtoPorId", DocumentoDTO.class).setParameter("id", id).getSingleResult();
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Documento buscarDocumentoPorId(int id) {

        try {
            return (Documento) em.createNamedQuery("Documento.buscarDocumentoPorId", Documento.class).setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Documento buscarDocumentoPorIdNativo(int id) {

        return (Documento) em.createNativeQuery("SELECT * FROM documento WHERE  documento_id =  " + id, Documento.class).getSingleResult();

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Documento> buscarDocumentoPorTipoDocumento(int tipo_id) {

        return em.createNativeQuery("SELECT tipo_id, documento_id, carpeta_id, activa, comentario, folio_final, folio_inicial, nombre_documento, paginas, ruta_documento FROM documento WHERE  tipo_id =  " + tipo_id, Documento.class).getResultList();

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void eliminarDocumentoId(int id_documento) {

        em.createNamedQuery("Documento.eliminarDocumentoId", Documento.class).setParameter("id_documento", id_documento).executeUpdate();

    }

    public void eliminarDocumentoRestar(int id) {

        em.createNativeQuery("Delete from documento where documento_id  = '" + id + "'").executeUpdate();

    }

    public void eliminarDocumentoPorCarpetaId(int id_carpeta) {
        em.createNativeQuery("Delete from documento where carpeta_id  = '" + id_carpeta + "'; Delete from carpeta where carpeta_id = '" + id_carpeta + "'")
                .executeUpdate();

    }
}
