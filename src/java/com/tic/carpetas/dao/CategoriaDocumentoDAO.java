/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dao;

import com.tic.carpetas.dominio.CategoriaDocumento;
import com.tic.carpetas.dominio.TipoDocumento;
import com.tic.rbac.dominio.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Rothkegel
 */
@Stateless
public class CategoriaDocumentoDAO {

    @PersistenceContext
    private EntityManager em;

    public int crearCategoriaDocumento(CategoriaDocumento categoriaDocumento) {

        em.persist(categoriaDocumento);
//        em.flush();
        return categoriaDocumento.getCategoria_id();

    }

    public CategoriaDocumento buscarCategoriaDocumento(int id) {

        CategoriaDocumento categoriaDocumento = em.find(CategoriaDocumento.class, id);
        return categoriaDocumento;

    }

    public void actualizarCategoriaDocumento(CategoriaDocumento categoriaDocumento) {

        em.merge(categoriaDocumento);

    }

    public void eliminarCategoriaDocumento(int id) {

        em.remove(buscarCategoriaDocumento(id));

    }

    public List<CategoriaDocumento> listarCategoriaDocumentos() {

        List<CategoriaDocumento> lista = em
                .createQuery("SELECT catdoc FROM CategoriaDocumento catdoc ORDER BY catdoc.categoria_id")
                .getResultList();
        return lista;

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public CategoriaDocumento getTipoDocumentoPorIdCategoria(int categoria_id) {
        return em.createNamedQuery("CategoriaDocumento.getTipoDocumentoPorIdCategoria", CategoriaDocumento.class).setParameter("categoria_id", categoria_id)
                .getSingleResult();
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public CategoriaDocumento buscarCategoriaDocumentoPalabra() {
        return em.createNamedQuery("CategoriaDocumento.buscarCategoriaDocumento", CategoriaDocumento.class)
                .getSingleResult();
    }
}
