    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dao;

import com.tic.carpetas.dominio.Carpeta;
import com.tic.carpetas.dto.ArregloLlaveValor;
import com.tic.carpetas.dto.CarpetaDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Rothkegel
 */
@Stateless
public class CarpetaDAO {

    @PersistenceContext
    private EntityManager em;

    public int crearCarpeta(Carpeta carpeta) {

        em.persist(carpeta);
        em.flush();
        return carpeta.getCarpeta_id();

    }

    public Carpeta buscarCarpeta(int id) {

        Carpeta carpeta = em.find(Carpeta.class, id);
        return carpeta;

    }

    public int actualizarCarpeta(Carpeta carpeta) {

        em.merge(carpeta);
        em.flush();
        return carpeta.getCarpeta_id();

    }

    public void eliminarCarpeta(int id) {

        em.remove(buscarCarpeta(id));

    }

    public List<Carpeta> listarCarpetas() {

        return null;

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Carpeta buscarCarpetaPorNumero(int carpeta) {
        try {
            return em.createNamedQuery("Carpeta.buscarCarpetaPorNumero", Carpeta.class).setParameter("carpeta", carpeta)
                    .getSingleResult();
        } catch (Exception e) {
        }
        return null;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void eliminarCarpetaPorCarpetaId(int id_carpeta) {
        em.createNamedQuery("Carpeta.eliminarCarpetaId", Carpeta.class)
                .setParameter("carpeta_id", id_carpeta)
                .executeUpdate();

    }

    public int buscarCarpetaPorLlaves(int carpeta, int caja, int comision, boolean califica, String letra_caja, String letra_carpeta, String serie) {

        boolean existe = false;
        int carpeta_id = 0;
        try {
            
            if (letra_caja == null || letra_caja == "") {
                carpeta_id = (Integer) em.createQuery("SELECT car.carpeta_id FROM Carpeta car WHERE car.carpeta = :carpeta AND car.nro_caja = :nro_caja AND car.comision = :comision AND car.califica = :califica AND car.serie = :serie AND car.letra_caja = '' AND car.letra_carpeta = ''")
                        .setParameter("carpeta", carpeta)
                        .setParameter("nro_caja", caja)
                        .setParameter("comision", comision)
                        .setParameter("califica", califica)
                        .setParameter("serie", serie)
                        .getSingleResult();
            }else {
                carpeta_id = (Integer) em.createQuery("SELECT car.carpeta_id FROM Carpeta car WHERE car.carpeta = :carpeta AND car.nro_caja = :nro_caja AND car.comision = :comision AND car.califica = :califica AND car.letra_caja = :letra_caja AND car.letra_carpeta = :letra_carpeta AND car.serie = :serie ")
                        .setParameter("carpeta", carpeta)
                        .setParameter("nro_caja", caja)
                        .setParameter("comision", comision)
                        .setParameter("califica", califica)
                        .setParameter("letra_caja", letra_caja)
                        .setParameter("letra_carpeta", letra_carpeta)
                        .setParameter("serie", serie)
                        .getSingleResult();
            }
        } catch (Exception e) {

            return carpeta_id;

        }

        return carpeta_id;

    }

    public List<Carpeta> listarCarpetaPaginada(CarpetaDTO carpeta, int cantidadPorPagina, int numeroPagina, String order) {
        int nrocarpeta = 0;
        int caja = 0;
        int comision = 0;
        boolean califica = false;
        String nombre = "";
        String apellido = "";
        String rut = "";
        String query = "";
        String tipo_id = carpeta.getTipo_id();
        if (tipo_id != null){
            query = "SELECT * FROM public.carpeta where carpeta_id in(select carpeta_id from documento where tipo_id IN " + tipo_id + " group by carpeta_id) ";
            
        }else {
            query = "SELECT * FROM public.carpeta ";
        }
        List<ArregloLlaveValor> campos = new ArrayList();
        if (carpeta.getCarpeta() != 0) {
            campos.add(new ArregloLlaveValor("carpeta", " = " + carpeta.getCarpeta()));
        }
        if (carpeta.getNombres() != null) {
            campos.add(new ArregloLlaveValor("LOWER(sp_ascii(nombres))", " ILIKE LOWER(sp_ascii('%" + carpeta.getNombres() + "%'))"));
        }
        if (carpeta.getApellidos() != null) {
            campos.add(new ArregloLlaveValor("LOWER(sp_ascii(apellidos))", " ILIKE LOWER(sp_ascii('%" + carpeta.getApellidos() + "%'))"));
        }
        if (carpeta.getRut() != null) {
            campos.add(new ArregloLlaveValor("LOWER(rut)", " like LOWER('%" + carpeta.getRut() + "%')"));
        }
        if (carpeta.getNro_caja() != 0) {
            campos.add(new ArregloLlaveValor("nro_caja", " = " + carpeta.getNro_caja()));
        }
        if (carpeta.getSerie() != null) {
            campos.add(new ArregloLlaveValor("serie", " = '" + carpeta.getSerie() + "'"));
        }
        if (carpeta.getComision() != 0) {
            campos.add(new ArregloLlaveValor("comision", " = " + carpeta.getComision()));
        }
        if (carpeta.getLetra_caja() != null) {
            campos.add(new ArregloLlaveValor("letra_caja", " = '" + carpeta.getLetra_caja()+ "'"));
        }
        if (carpeta.getLetra_carpeta()!= null) {
            campos.add(new ArregloLlaveValor("letra_carpeta", " = '" + carpeta.getLetra_carpeta() + "'"));
        }
        if (carpeta.getTipo_carpeta()!= null) {
            campos.add(new ArregloLlaveValor("tipo_carpeta", " = '" + carpeta.getTipo_carpeta() + "'"));
        }
        if (carpeta.getIdentificador() != null) {
            campos.add(new ArregloLlaveValor("identificador", " = '" + carpeta.getIdentificador() + "'"));
        }
        for (int i = 0; i < campos.size(); i++) {
            if (i == 0 && tipo_id == null) {
                query = query + "Where ";
            }else if(i == 0 && tipo_id != null){
                query = query + " AND ";
            }
            query = query + ((ArregloLlaveValor) campos.get(i)).getLlave() + ((ArregloLlaveValor) campos.get(i)).getValor();
            if ((campos.size() > 1) && (i < campos.size() - 1)) {
                query = query + " AND ";
            }
        }
        System.out.println(query);
        List<Carpeta> lista = em.createNativeQuery(query + " ORDER BY nro_caja, carpeta, nombres, apellidos ASC" + " LIMIT " + cantidadPorPagina + " OFFSET (" + cantidadPorPagina + " * (" + numeroPagina + "-1));", Carpeta.class).getResultList();
        System.out.println(lista);
        return lista;
    }

    public CarpetaDTO listarPaginacion(CarpetaDTO carpeta, int cantidadPorPagina) {
        int nrocarpeta = 0;
        int caja = 0;
        int comision = 0;
        boolean califica = false;
        String nombre = "";
        String apellido = "";
        String rut = "";
        String letra_caja = "";
        String letra_carpeta = "";
        String tipo_carpeta = "";
        String serie = "";
        String query = "";
        String tipo_id = carpeta.getTipo_id();
        if (tipo_id != null){
            query = "SELECT count(1) FROM public.carpeta where carpeta_id in(select carpeta_id from documento where tipo_id IN " + tipo_id + " group by carpeta_id) ";
        }else {
            query = "SELECT count(1) FROM public.carpeta ";
        }
        List<ArregloLlaveValor> campos = new ArrayList();
        if (carpeta.getCarpeta() != 0) {
            campos.add(new ArregloLlaveValor("carpeta", " = " + carpeta.getCarpeta()));
        }
        if (carpeta.getNombres() != null) {
            campos.add(new ArregloLlaveValor("LOWER(sp_ascii(nombres))", " ILIKE LOWER(sp_ascii('%" + carpeta.getNombres() + "%'))"));
        }
        if (carpeta.getApellidos() != null) {
            campos.add(new ArregloLlaveValor("LOWER(sp_ascii(apellidos))", " ILIKE LOWER(sp_ascii('%" + carpeta.getApellidos() + "%'))"));
        }
        if (carpeta.getRut() != null) {
            campos.add(new ArregloLlaveValor("LOWER(rut)", " like LOWER('%" + carpeta.getRut() + "%')"));
        }
        if (carpeta.getNro_caja() != 0) {
            campos.add(new ArregloLlaveValor("nro_caja", " = " + carpeta.getNro_caja()));
        }
        if (carpeta.getSerie() != null) {
            campos.add(new ArregloLlaveValor("serie", " = '" + carpeta.getSerie() + "'"));
        }
        if (carpeta.getComision() != 0) {
            campos.add(new ArregloLlaveValor("comision", " = " + carpeta.getComision()));
        }
        if (carpeta.getLetra_caja() != null) {
            campos.add(new ArregloLlaveValor("letra_caja", " = '" + carpeta.getLetra_caja()+ "'"));
        }
        if (carpeta.getLetra_carpeta()!= null) {
            campos.add(new ArregloLlaveValor("letra_carpeta", " = '" + carpeta.getLetra_carpeta() + "'"));
        }
        if (carpeta.getTipo_carpeta()!= null) {
            campos.add(new ArregloLlaveValor("tipo_carpeta", " = '" + carpeta.getTipo_carpeta() + "'"));
        }
        if (carpeta.getIdentificador() != null) {
            campos.add(new ArregloLlaveValor("identificador", " = '" + carpeta.getIdentificador() + "'"));
        }
        for (int i = 0; i < campos.size(); i++) {
            if (i == 0 && tipo_id == null) {
                query = query + "Where ";
            }else if(i == 0 && tipo_id != null){
                query = query + " AND ";
            }
            query = query + ((ArregloLlaveValor) campos.get(i)).getLlave() + ((ArregloLlaveValor) campos.get(i)).getValor();
            if ((campos.size() > 1) && (i < campos.size() - 1)) {
                query = query + " AND ";
            }
        }
        System.out.println(query);
        Long cantidadRegistros = (Long) em.createNativeQuery(query).getSingleResult();

        double numeroPaginas = cantidadRegistros.longValue() / cantidadPorPagina;
        numeroPaginas += (cantidadRegistros.longValue() % cantidadPorPagina > 0L ? 1.0D : 0.0D);

        carpeta.setNumeropaginas((int) numeroPaginas);

        return carpeta;
    }

    public void eliminarDocPorCarpetas(int carpeta_id) {

        Carpeta carpeta = buscarCarpeta(carpeta_id);
        carpeta.setNumerodocumentos(0);
        carpeta.setNumeropaginas(0);
        carpeta.setNumeropdf(0);
        this.actualizarCarpeta(carpeta);
        em.createNativeQuery("Delete from documento where carpeta_id  = '" + carpeta_id + "'").executeUpdate();

    }
}
