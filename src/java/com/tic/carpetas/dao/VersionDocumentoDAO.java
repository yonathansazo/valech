/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dao;

import com.tic.carpetas.dominio.VersionDocumento;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jsilva
 */
@Stateless
public class VersionDocumentoDAO {
    
    @PersistenceContext
    private EntityManager em;

    public int crearVersionDocumento(VersionDocumento versionDocumento) {

        em.persist(versionDocumento);
        em.flush();
        return versionDocumento.getVersion_documento_id();

    }
    
    public List<VersionDocumento> listarVersionesDocumento(int version_id) {
                
        String query = "SELECT * FROM public.versiondocumento where version_id=" + version_id;
        List<VersionDocumento> lista = em.createNativeQuery(query, VersionDocumento.class).getResultList();
        return lista;
        
    }
    
    public void eliminarVersionDocumentos(int version_id) {
        
        em.createNativeQuery("Delete from public.versiondocumento where version_id  = " + version_id + "").executeUpdate();
        
    }
    
}
