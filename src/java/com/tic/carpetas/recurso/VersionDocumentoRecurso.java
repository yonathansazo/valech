/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.recurso;

import com.tic.carpetas.dto.VersionDocumentoDTO;
import com.tic.carpetas.servicio.VersionDocumentoServicio;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jsilva
 */
@Path("versiondocumento")
public class VersionDocumentoRecurso {
    
     @EJB
    private VersionDocumentoServicio versionDocumentoServicio;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{version_id}")
    public Response getVersionDocumento(@PathParam("version_id") int version_id) {
        //no se usa
        ArrayList<VersionDocumentoDTO> lista = versionDocumentoServicio.listarVersionesDocumentos(version_id);
        GenericEntity<ArrayList<VersionDocumentoDTO>> entidad;
        entidad = new GenericEntity<ArrayList<VersionDocumentoDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putVersion(VersionDocumentoDTO dto) {
        versionDocumentoServicio.crearVersionDocumento(dto);
        return Response
                .ok()
                .entity(dto)
                .build();
    }
    
}
