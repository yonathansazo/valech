/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.recurso;

import com.tic.carpetas.dto.CarpetaDTO;
import com.tic.carpetas.dto.DocumentoDTO;
import com.tic.carpetas.servicio.CarpetaServicio;
import com.tic.carpetas.servicio.DocumentoServicio;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Arothkegel
 */
@Path("carpeta")
public class CarpetaRecurso {

    @EJB
    private CarpetaServicio carpetaServicio;

    @EJB
    private DocumentoServicio documentoServicio;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCarpetas() {
        //no se usa
        ArrayList<CarpetaDTO> lista = carpetaServicio.listarCarpetas();
        GenericEntity<ArrayList<CarpetaDTO>> entidad;
        entidad = new GenericEntity<ArrayList<CarpetaDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{carpeta_id}")
    public Response getCarpetasId(@PathParam("carpeta_id") int carpeta_id) {
            CarpetaDTO carpeta = carpetaServicio.buscarCarpeta(carpeta_id);
        System.out.println("");
        return Response
                .ok()
                .entity(carpeta)
                .build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{carpeta_id}")
    public Response addCarpetas(@PathParam("carpeta_id") int carpeta_id) {
        carpetaServicio.eliminarPdfNotario(carpeta_id);
        return Response
                .ok()
                .entity("true")
                .build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addCarpetas(CarpetaDTO dto) {
        //no se usa
        int carpeta_id = carpetaServicio.buscarCarpetaPorLlaves(dto.getCarpeta(), dto.getNro_caja(), dto.getComision(), dto.isCalifica(), dto.getLetra_caja(), dto.getLetra_carpeta(), dto.getSerie());
        return Response
                .ok()
                .entity(carpeta_id)
                .build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putCarpetas(CarpetaDTO dto) {
        carpetaServicio.actualizarCarpeta(dto);
        return Response
                .ok()
                .entity(dto)
                .build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("carpetaPaginacion")
    public Response putCarpetasPaginacion(CarpetaDTO dto) {
        carpetaServicio.listarPaginacion(dto);
        return Response.ok()
                .entity(dto)
                .build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("carpetaPaginada")
    public Response putCarpetasPaginadas(CarpetaDTO dto) {
        if (dto.getNumeropdf() < 1) {
            dto.setNumeropdf(1);
        }
        List<CarpetaDTO> lista = carpetaServicio.listarCarpetasPaginadas(dto);
        GenericEntity<List<CarpetaDTO>> entidad;

        entidad = new GenericEntity<List<CarpetaDTO>>(lista) {
        };

        return Response.ok().entity(entidad).build();
    }

    @POST
    @Path("eliminaDocumentos/{carpeta_id}")
    public Response putEliminaDocPorCarpetas(@PathParam("carpeta_id") int carpeta_id) {
        carpetaServicio.eliminarDocPorCarpetas(carpeta_id);
        return Response.ok()
                .entity(carpeta_id)
                .build();
    }

    @DELETE
    @Path("{carpeta_id}")
    public Response deleteCarpetas(@PathParam("carpeta_id") int carpeta_id) {
        documentoServicio.eliminarDocumentoPorCarpetaId(carpeta_id);
        return Response
                .ok()
                .entity(true)
                .build();
    }
}
