/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.recurso;

import com.tic.carpetas.dto.CategoriaDocumentoDTO;
import com.tic.carpetas.servicio.CategoriaDocumentoServicio;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
/**
 *
 * @author Arothkegel
 */
@Path("categoria")
public class CategoriaDocumentoRecurso {

    @EJB
    private CategoriaDocumentoServicio categoriaDocumentoServicio;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPersonas() {

        ArrayList<CategoriaDocumentoDTO> lista = categoriaDocumentoServicio.listarCategoriaDocumentos();
        GenericEntity<ArrayList<CategoriaDocumentoDTO>> entidad;
        entidad = new GenericEntity<ArrayList<CategoriaDocumentoDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }
    

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPersonas(CategoriaDocumentoDTO dto) {
        categoriaDocumentoServicio.crearCategoriaDocumento(dto);
        return Response
                .ok()
                .entity(dto)
                .build();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{categoria_id}")
    public Response updatePersonas(@PathParam("categoria_id")int categoria_id,  CategoriaDocumentoDTO dto) {
        dto.setCategoria_id(categoria_id);
        categoriaDocumentoServicio.actualizarCategoriaDocumento(dto);
        return Response
                .ok()
                .entity(dto)
                .build();
    }
    
    @DELETE
    @Produces
    @Path("{categoria_id}")
    public Response deletePersonas(@PathParam("categoria_id")int categoria_id) {
        categoriaDocumentoServicio.eliminarCategoriaDocumento(categoria_id);
        return Response
                .ok()
                .build();
    }
}
