/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.recurso;

import com.tic.carpetas.dto.VersionDTO;
import com.tic.carpetas.dto.VersionDocumentoDTO;
import com.tic.carpetas.servicio.VersionDocumentoServicio;
import com.tic.carpetas.servicio.VersionServicio;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jsilva
 */
@Path("version")
public class VersionRecurso {
    @EJB
    private VersionServicio versionServicio;
    
    @EJB
    private VersionDocumentoServicio versionDocumentoServicio;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVersiones() {
        //no se usa
        ArrayList<VersionDTO> lista = versionServicio.listarVersiones();
        GenericEntity<ArrayList<VersionDTO>> entidad;
        entidad = new GenericEntity<ArrayList<VersionDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }

//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    @Path("{version_id}")
//    public Response getVersionId(@PathParam("version_id") int version_id) {
//        VersionDTO carpeta = versionServicio.buscarVersion(version_id);
//        return Response
//                .ok()
//                .entity(carpeta)
//                .build();
//    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{carpeta_id}")
    public Response getVersionCarpeta(@PathParam("carpeta_id") int carpeta_id) {
        //no se usa
        ArrayList<VersionDTO> lista = versionServicio.listarVersionesCarpeta(carpeta_id);
        GenericEntity<ArrayList<VersionDTO>> entidad;
        entidad = new GenericEntity<ArrayList<VersionDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putVersion(VersionDTO dto) {
        List<VersionDocumentoDTO> lista = dto.getVersiondocumento();
        int version_id = dto.getVersion_id();
        if (version_id != 0) {
            versionDocumentoServicio.eliminarVersionDocumentoPorVersionId(version_id);
        }   
        int id = versionServicio.crearVersion(dto);
        versionDocumentoServicio.crearVersionDocumentos(lista, id);
        return Response
                .ok()
                .entity(dto)
                .build();
    }
    
     @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("versionPaginacion")
    public Response putCarpetasPaginacion(VersionDTO dto) {
        versionServicio.listarPaginacion(dto);
        return Response.ok()
                .entity(dto)
                .build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("versionPaginada")
    public Response putCarpetasPaginadas(VersionDTO dto) {
        List<VersionDTO> lista = versionServicio.listarVersionesPaginadas(dto);
        GenericEntity<List<VersionDTO>> entidad;

        entidad = new GenericEntity<List<VersionDTO>>(lista) {
        };

        return Response.ok().entity(entidad).build();
    }
    
    @DELETE
    @Path("{version_id}")
    public Response deleteVersion(@PathParam("version_id") int version_id) {
        versionDocumentoServicio.eliminarVersionDocumentoPorVersionId(version_id);
        versionServicio.eliminarVersionPorVersionId(version_id);
        return Response
                .ok()
                .entity(true)
                .build();
    }
}
