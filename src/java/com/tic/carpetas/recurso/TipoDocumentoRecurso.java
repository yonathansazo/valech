/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.recurso;

import com.tic.carpetas.dto.TipoDocumentoDTO;
import com.tic.carpetas.servicio.TipoDocumentoServicio;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Arothkegel
 */
@Path("tipodocumento")
public class TipoDocumentoRecurso {

    @EJB
    private TipoDocumentoServicio tipoDocumentoServicio;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTipoDocumento() {

        List<TipoDocumentoDTO> lista = tipoDocumentoServicio.listarTipoDocumentos();
        GenericEntity<List<TipoDocumentoDTO>> entidad;
        entidad = new GenericEntity<List<TipoDocumentoDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{categoria_id}")
    public Response getTipoDocumentoPorIdCategoria(@PathParam("categoria_id")int categoria_id) {

        List<TipoDocumentoDTO> lista = tipoDocumentoServicio.getTipoDocumentoPorIdCategoria(categoria_id);
        GenericEntity<List<TipoDocumentoDTO>> entidad;
        entidad = new GenericEntity<List<TipoDocumentoDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{texto}")
    public Response getTipoDocumentoPorTexto(@PathParam("texto")String texto) {

        List<TipoDocumentoDTO> listaDos = tipoDocumentoServicio.listarTipoDocumentosPorTexto(texto);
        GenericEntity<List<TipoDocumentoDTO>> entidadDos;
        entidadDos = new GenericEntity<List<TipoDocumentoDTO>>(listaDos) {
        };

        return Response
                .ok()
                .entity(entidadDos)
                .build();

    }
    

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addTipoDocumento(TipoDocumentoDTO dto) {
        tipoDocumentoServicio.crearTipoDocumento(dto);
        return Response
                .ok()
                .entity(dto)
                .build();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{tipo_id}")
    public Response updateTipoDocumento(@PathParam("tipo_id")int tipo_id,  TipoDocumentoDTO dto) {
        dto.setTipo_id(tipo_id);
        tipoDocumentoServicio.actualizarTipoDocumento(dto);
        return Response
                .ok()
                .entity(dto)
                .build();
    }
    
    @DELETE
    @Produces
    @Path("{tipo_id}")
    public Response deleteTipoDocumento(@PathParam("tipo_id")int tipo_id) {
        tipoDocumentoServicio.eliminarTipoDocumento(tipo_id);
        return Response
                .ok()
                .build();
    }
}
