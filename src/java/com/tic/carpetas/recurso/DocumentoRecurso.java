/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.recurso;

import com.tic.carpetas.dto.DocumentoDTO;
import com.tic.carpetas.servicio.DocumentoServicio;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Arothkegel
 */
@Path("documento")
public class DocumentoRecurso {

    @EJB
    private DocumentoServicio documentoServicio;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDocumento() {

        ArrayList<DocumentoDTO> lista = documentoServicio.listarDocumentos();
        GenericEntity<ArrayList<DocumentoDTO>> entidad;
        entidad = new GenericEntity<ArrayList<DocumentoDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{carpeta_id}")
    public Response getDocumentoPorCarpeta(@PathParam("carpeta_id")int carpeta_id) {

        List<DocumentoDTO> lista = documentoServicio.listarDocumentosPorCarpeta(carpeta_id);
        GenericEntity<List<DocumentoDTO>> entidad;
        entidad = new GenericEntity<List<DocumentoDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addDocumento(DocumentoDTO dto) {
        documentoServicio.crearDocumento(dto);
        return Response
                .ok()
                .entity(dto)
                .build();
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateDocumento(List<DocumentoDTO> dto) {
        documentoServicio.actualizarDocumento(dto);
        return Response
                .ok()
                .build();
    }
    
    @DELETE
    @Path("{documento_id}")
    public Response deleteDocumento(@PathParam("documento_id")int documento_id) {
        DocumentoDTO doc = documentoServicio.buscarDocumento(documento_id);
        documentoServicio.eliminarDocumento(documento_id);
        documentoServicio.actualizaTotales(doc.getCarpeta_id());
        return Response
                .ok()
                .build();
    }
}
