/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dominio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Rothkegel
 */
@Entity
@Table(name = "categoriadocumento")
@NamedQueries({
    @NamedQuery(
            name = "CategoriaDocumento.buscarCategoriaDocumento",
            query = "SELECT u FROM CategoriaDocumento u WHERE u.nombreCategoria = :categoria"),
        @NamedQuery(
            name = "CategoriaDocumento.getTipoDocumentoPorIdCategoria",
            query = "SELECT u FROM CategoriaDocumento u WHERE u.categoria_id = :categoria_id")
})
public class CategoriaDocumento implements Serializable{
    
    @Id
    @Column(name="categoria_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int categoria_id;
    
    @Column(name="nombre_categoria")
    private String nombreCategoria;
    
    @OneToMany(mappedBy = "categoriadocumento", cascade = CascadeType.ALL)
    private List<TipoDocumento> tipodocumentos;

    public CategoriaDocumento() {
    }

    public int getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(int categoria_id) {
        this.categoria_id = categoria_id;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public List<TipoDocumento> getTipodocumentos() {
        return tipodocumentos;
    }

    public void setTipodocumentos(List<TipoDocumento> tipodocumentos) {
        this.tipodocumentos = tipodocumentos;
    }

}
