/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dominio;

import com.tic.rbac.dominio.PerfilAssociationId;

/**
 *
 * @author Rothkegel
 */
public class DocumentoAssociationId {
    
    private int carpeta_id;

    private int tipo_id;
    
    private int documento_id;

    public int hashCode() {
        return (int) (carpeta_id + tipo_id + documento_id);
    }

    public boolean equals(Object object) {
        if (object instanceof DocumentoAssociationId) {
            DocumentoAssociationId otherId = (DocumentoAssociationId) object;
            return (otherId.carpeta_id == this.carpeta_id) && (otherId.tipo_id == this.tipo_id) && (otherId.documento_id == this.documento_id);
        }
        return false;
    }
    
}
