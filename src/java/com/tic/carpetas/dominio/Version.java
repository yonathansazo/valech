/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dominio;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author jsilva
 */
@Entity
@Table(name="version")
public class Version implements Serializable{
    
    @Id
    @Column(name = "version_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int version_id;
    
    private List<VersionDocumento> versionDocumento;
    
    @Column(name = "nro_interno")
    private String nro_interno;
    
    @Column(name = "caja")
    private int caja;

    @Column(name = "carpeta")
    private int carpeta;

    @Column(name = "letra_caja")
    private String letra_caja;

    @Column(name = "letra_carpeta")
    private String letra_carpeta;

    @Column(name = "fecha")
    private Timestamp fecha;

    @Column(name = "estado")
    private String estado;

    @Column(name = "declaracion")
    private String declaracion;
    
    @Column(name = "carpeta_id")
    private int carpeta_id;
    
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "rut")
    private String rut;
    
    @Column(name = "identificador")
    private String identificador;
    
    @Column(name = "encabezado")
    private String encabezado;
    
    @Column(name = "conclusion")
    private String conclusion;
    
    @Column(name = "tipo_carpeta")
    private String tipo_carpeta;
    
    @Column(name = "serie")
    private String serie;
    
    @Column(name = "comision")
    private int comision;

    @Column(name = "califica")
    private boolean califica;

    @Column(name = "apela")
    private boolean apela;
   
    @Column(name = "documento_id")
    private int documento_id;

    public Version() {
    }

    public Version(int version_id, List<VersionDocumento> versionDocumento, String nro_interno, int caja, int carpeta, String letra_caja, String letra_carpeta, Timestamp fecha, String estado, String declaracion, int carpeta_id, String nombre, String rut, String identificador, String encabezado, String conclusion, String tipo_carpeta, String serie, int comision, boolean califica, boolean apela, int documento_id) {
        this.version_id = version_id;
        this.versionDocumento = versionDocumento;
        this.nro_interno = nro_interno;
        this.caja = caja;
        this.carpeta = carpeta;
        this.letra_caja = letra_caja;
        this.letra_carpeta = letra_carpeta;
        this.fecha = fecha;
        this.estado = estado;
        this.declaracion = declaracion;
        this.carpeta_id = carpeta_id;
        this.nombre = nombre;
        this.rut = rut;
        this.identificador = identificador;
        this.encabezado = encabezado;
        this.conclusion = conclusion;
        this.tipo_carpeta = tipo_carpeta;
        this.serie = serie;
        this.comision = comision;
        this.califica = califica;
        this.apela = apela;
        this.documento_id = documento_id;
    }

    public int getDocumento_id() {
        return documento_id;
    }

    public void setDocumento_id(int documento_id) {
        this.documento_id = documento_id;
    }

    public String getTipo_carpeta() {
        return tipo_carpeta;
    }

    public void setTipo_carpeta(String tipo_carpeta) {
        this.tipo_carpeta = tipo_carpeta;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getComision() {
        return comision;
    }

    public void setComision(int comision) {
        this.comision = comision;
    }

    public boolean isCalifica() {
        return califica;
    }

    public void setCalifica(boolean califica) {
        this.califica = califica;
    }

    public boolean isApela() {
        return apela;
    }

    public void setApela(boolean apela) {
        this.apela = apela;
    }
    
    public List<VersionDocumento> getVersionDocumento() {
        return versionDocumento;
    }

    public void setVersionDocumento(List<VersionDocumento> versionDocumento) {
        this.versionDocumento = versionDocumento;
    }
    
    public String getNro_interno() {
        return nro_interno;
    }

    public void setNro_interno(String nro_interno) {
        this.nro_interno = nro_interno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getEncabezado() {
        return encabezado;
    }

    public void setEncabezado(String encabezado) {
        this.encabezado = encabezado;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public int getCarpeta_id() {
        return carpeta_id;
    }

    public void setCarpeta_id(int carpeta_id) {
        this.carpeta_id = carpeta_id;
    }

    public int getVersion_id() {
        return version_id;
    }

    public void setVersion_id(int version_id) {
        this.version_id = version_id;
    }

    public int getCaja() {
        return caja;
    }

    public void setCaja(int caja) {
        this.caja = caja;
    }

    public int getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(int carpeta) {
        this.carpeta = carpeta;
    }

    public String getLetra_caja() {
        return letra_caja;
    }

    public void setLetra_caja(String letra_caja) {
        this.letra_caja = letra_caja;
    }

    public String getLetra_carpeta() {
        return letra_carpeta;
    }

    public void setLetra_carpeta(String letra_carpeta) {
        this.letra_carpeta = letra_carpeta;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDeclaracion() {
        return declaracion;
    }

    public void setDeclaracion(String declaracion) {
        this.declaracion = declaracion;
    }

    
    
}
