/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dominio;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Rothkegel
 */
 @Entity
 @Table(name="carpeta")
// @NamedStoredProcedureQuery(name="Carpeta.obtenerCarpetasPaginadas", procedureName="fn_obtenercarpetas", parameters={@StoredProcedureParameter(mode=javax.persistence.ParameterMode.IN, type=Integer.class, name="paginacion_tamaniopagina"), @StoredProcedureParameter(mode=javax.persistence.ParameterMode.IN, type=Integer.class, name="paginacion_numeropagina"), @StoredProcedureParameter(mode=javax.persistence.ParameterMode.IN, type=String.class, name="paginacion_ordena")})
 @NamedQueries({@NamedQuery(name="Carpeta.buscarCarpetaPorNumero", query="SELECT u FROM Carpeta u WHERE u.carpeta = :carpeta"), @NamedQuery(name="Carpeta.eliminarCarpetaId", query="DELETE FROM Carpeta p WHERE p.carpeta_id = :carpeta_id")})
public class Carpeta implements Serializable {

    @Id
    @Column(name = "carpeta_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int carpeta_id;

    @OneToMany(mappedBy = "carpeta", cascade = CascadeType.ALL)
    private List<Documento> documentos;

    @Column(name = "nro_caja")
    private int nro_caja;

    @Column(name = "carpeta")
    private int carpeta;

    @Column(name = "nombres")
    private String nombres;

    @Column(name = "apellidos")
    private String apellidos;

    @Column(name = "comision")
    private int comision;

    @Column(name = "califica")
    private boolean califica;

    @Column(name = "apela")
    private boolean apela;

    @Column(name = "rut")
    private String rut;

    @Column(name = "dv")
    private char dv;

    @Column(name = "identificador")
    private String identificador;

    @Column(name = "activa")
    private boolean activa;

    @Column(name = "numeropdf")
    private int numeropdf;

    @Column(name = "numerodocumentos")
    private int numerodocumentos;

    @Column(name = "numeropaginas")
    private int numeropaginas;

    @Column(name = "rutapdfnotario")
    private String rutapdfnotario;

     @Column(name = "nombrepdfnotario")
    private String nombrepdfnotario;
     
     @Column(name = "serie")
    private String serie;
     
     @Column(name = "letra_caja")
    private String letra_caja;
     
     @Column(name = "letra_carpeta")
    private String letra_carpeta;
     
     @Column(name = "tipo_carpeta")
    private String tipo_carpeta;

    @Column(name = "declara")
    private String declara;

    public Carpeta() {
    }

    public Carpeta(int carpeta_id, List<Documento> documentos, int nro_caja, int carpeta, String nombres, String apellidos, int comision, boolean califica, boolean apela, String rut, char dv, String identificador, boolean activa, int numeropdf, int numerodocumentos, int numeropaginas, String rutapdfnotario, String nombrepdfnotario, String serie, String letra_caja, String letra_carpeta, String tipo_carpeta, String declara) {
        this.carpeta_id = carpeta_id;
        this.documentos = documentos;
        this.nro_caja = nro_caja;
        this.carpeta = carpeta;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.comision = comision;
        this.califica = califica;
        this.apela = apela;
        this.rut = rut;
        this.dv = dv;
        this.identificador = identificador;
        this.activa = activa;
        this.numeropdf = numeropdf;
        this.numerodocumentos = numerodocumentos;
        this.numeropaginas = numeropaginas;
        this.rutapdfnotario = rutapdfnotario;
        this.nombrepdfnotario = nombrepdfnotario;
        this.serie = serie;
        this.letra_caja = letra_caja;
        this.letra_carpeta = letra_carpeta;
        this.tipo_carpeta = tipo_carpeta;
        this.declara = declara;
    }

    public String getDeclara() {
        return declara;
    }

    public void setDeclara(String declara) {
        this.declara = declara;
    }
    
    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getLetra_caja() {
        return letra_caja;
    }

    public void setLetra_caja(String letra_caja) {
        this.letra_caja = letra_caja;
    }

    public String getLetra_carpeta() {
        return letra_carpeta;
    }

    public void setLetra_carpeta(String letra_carpeta) {
        this.letra_carpeta = letra_carpeta;
    }

    public String getTipo_carpeta() {
        return tipo_carpeta;
    }

    public void setTipo_carpeta(String tipo_carpeta) {
        this.tipo_carpeta = tipo_carpeta;
    }

    public int getCarpeta_id() {
        return carpeta_id;
    }

    public void setCarpeta_id(int carpeta_id) {
        this.carpeta_id = carpeta_id;
    }

    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Documento> documentos) {
        this.documentos = documentos;
    }

    public int getNro_caja() {
        return nro_caja;
    }

    public void setNro_caja(int nro_caja) {
        this.nro_caja = nro_caja;
    }

    public int getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(int carpeta) {
        this.carpeta = carpeta;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getComision() {
        return comision;
    }

    public void setComision(int comision) {
        this.comision = comision;
    }

    public boolean isCalifica() {
        return califica;
    }

    public void setCalifica(boolean califica) {
        this.califica = califica;
    }

    public boolean isApela() {
        return apela;
    }

    public void setApela(boolean apela) {
        this.apela = apela;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public char getDv() {
        return dv;
    }

    public void setDv(char dv) {
        this.dv = dv;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public boolean isActiva() {
        return activa;
    }

    public void setActiva(boolean activa) {
        this.activa = activa;
    }

    public int getNumeropdf() {
        return numeropdf;
    }

    public void setNumeropdf(int numeropdf) {
        this.numeropdf = numeropdf;
    }

    public int getNumerodocumentos() {
        return numerodocumentos;
    }

    public void setNumerodocumentos(int numerodocumentos) {
        this.numerodocumentos = numerodocumentos;
    }

    public int getNumeropaginas() {
        return numeropaginas;
    }

    public void setNumeropaginas(int numeropaginas) {
        this.numeropaginas = numeropaginas;
    }

    public String getRutapdfnotario() {
        return rutapdfnotario;
    }

    public void setRutapdfnotario(String rutapdfnotario) {
        this.rutapdfnotario = rutapdfnotario;
    }

    public String getNombrepdfnotario() {
        return nombrepdfnotario;
    }

    public void setNombrepdfnotario(String nombrepdfnotario) {
        this.nombrepdfnotario = nombrepdfnotario;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.carpeta_id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Carpeta other = (Carpeta) obj;
        if (!Objects.equals(this.carpeta_id, other.carpeta_id)) {
            return false;
        }
        return true;
    }
}
