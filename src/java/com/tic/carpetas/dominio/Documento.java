/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dominio;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Rothkegel
 */
@Entity
@Table(name = "documento")
@IdClass(DocumentoAssociationId.class)
@NamedQueries({
        @NamedQuery(name = "Documento.buscarTodosPorCarpeta",
                query = "SELECT p FROM Documento p WHERE p.carpeta_id = :carpeta_id ORDER BY p.documento_id "),
        @NamedQuery(name = "Documento.buscarDocumentoDtoPorId",
                query = "SELECT NEW com.tic.carpetas.dto.DocumentoDTO(p) FROM Documento p WHERE p.documento_id = :id"),
        @NamedQuery(name = "Documento.buscarDocumentoPorId",
                query = "SELECT p FROM Documento p WHERE p.documento_id = :id"),
        @NamedQuery(name = "Documento.eliminarDocumentoId",
                query = "DELETE FROM Documento p WHERE p.documento_id = :id_documento"),
        @NamedQuery(name = "Documento.eliminarDocumentoPorCarpetaId",
                query = "DELETE FROM Documento p WHERE p.carpeta_id = :carpeta_id")
})
public class Documento implements Serializable {

    @Id
    private int carpeta_id;
    @Id
    private int tipo_id;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "documento_id")
    private int documento_id;

    @Column(name = "paginas")
    private int paginas;

    @Column(name = "folio_inicial")
    private int folioInicial;

    @Column(name = "folio_final")
    private int folioFinal;
    
    @Column(name = "numero_pdf")
    private int numeropdf;

    @Column(name = "comentario")
    private String comentario;

    @Column(name = "nombre_documento")
    private String nombreDocumento;

    @Column(name = "ruta_documento")
    private String rutaDocumento;

    @Column(name = "activa")
    private boolean activa;
    
    @Column(name = "preservacion")
    private boolean preservacion;
    
    @Column(name = "letra_carpeta")
    private String letra_carpeta;
    
    @Column(name = "letra_caja")
    private String letra_caja;

    @ManyToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "carpeta_id", referencedColumnName = "carpeta_id")
    private Carpeta carpeta;

    @ManyToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "tipo_id", referencedColumnName = "tipo_id")
    private TipoDocumento tipodocumento;

    public Documento() {
    }

    public Documento(int carpeta_id, int tipo_id, int documento_id, int paginas, int folioInicial, int folioFinal, int numeropdf, String comentario, String nombreDocumento, String rutaDocumento, boolean activa, boolean preservacion, Carpeta carpeta, TipoDocumento tipodocumento, String letra_carpeta, String letra_caja) {
        this.carpeta_id = carpeta_id;
        this.tipo_id = tipo_id;
        this.documento_id = documento_id;
        this.paginas = paginas;
        this.folioInicial = folioInicial;
        this.folioFinal = folioFinal;
        this.numeropdf = numeropdf;
        this.comentario = comentario;
        this.nombreDocumento = nombreDocumento;
        this.rutaDocumento = rutaDocumento;
        this.activa = activa;
        this.preservacion = preservacion;
        this.carpeta = carpeta;
        this.tipodocumento = tipodocumento;
        this.letra_carpeta = letra_carpeta;
        this.letra_caja = letra_caja;
    }

    public String getLetra_carpeta() {
        return letra_carpeta;
    }

    public void setLetra_carpeta(String letra_carpeta) {
        this.letra_carpeta = letra_carpeta;
    }

    public String getLetra_caja() {
        return letra_caja;
    }

    public void setLetra_caja(String letra_caja) {
        this.letra_caja = letra_caja;
    }

    public int getCarpeta_id() {
        return carpeta_id;
    }

    public void setCarpeta_id(int carpeta_id) {
        this.carpeta_id = carpeta_id;
    }

    public int getTipo_id() {
        return tipo_id;
    }

    public void setTipo_id(int tipo_id) {
        this.tipo_id = tipo_id;
    }

    public int getDocumento_id() {
        return documento_id;
    }

    public void setDocumento_id(int documento_id) {
        this.documento_id = documento_id;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    public int getFolioInicial() {
        return folioInicial;
    }

    public void setFolioInicial(int folioInicial) {
        this.folioInicial = folioInicial;
    }

    public int getFolioFinal() {
        return folioFinal;
    }

    public void setFolioFinal(int folioFinal) {
        this.folioFinal = folioFinal;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public String getRutaDocumento() {
        return rutaDocumento;
    }

    public void setRutaDocumento(String rutaDocumento) {
        this.rutaDocumento = rutaDocumento;
    }

    public boolean isActiva() {
        return activa;
    }

    public void setActiva(boolean activa) {
        this.activa = activa;
    }

    public Carpeta getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(Carpeta carpeta) {
        this.carpeta = carpeta;
    }

    public TipoDocumento getTipodocumento() {
        return tipodocumento;
    }

    public void setTipodocumento(TipoDocumento tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    public boolean isPreservacion() {
        return preservacion;
    }

    public void setPreservacion(boolean preservacion) {
        this.preservacion = preservacion;
    }

    public int getNumeropdf() {
        return numeropdf;
    }

    public void setNumeropdf(int numeropdf) {
        this.numeropdf = numeropdf;
    }

    
    
}
