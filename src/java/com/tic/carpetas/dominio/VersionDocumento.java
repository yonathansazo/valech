/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dominio;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author jsilva
 */
@Entity
@Table(name="versiondocumento")
public class VersionDocumento implements Serializable{
    
    @Id
    @Column(name = "version_documento_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int version_documento_id;

    @Column(name = "version_id")
    private int version_id;
    
    @Column(name = "documento_id")
    private int documento_id;
    
    @Column(name = "Identificador")
    private String identificador;
    
    @Column(name = "nombre_documento")
    private String nombre_documento;
    
    @Column(name = "pagina")
    private int pagina;
        
    @Column(name = "ruta_documento")
    private String ruta_documento;

    public VersionDocumento() {
    }

    public VersionDocumento(int version_documento_id, int version_id, int documento_id, String identificador, String nombre_documento, int pagina, String ruta_documento) {
        this.version_documento_id = version_documento_id;
        this.version_id = version_id;
        this.documento_id = documento_id;
        this.identificador = identificador;
        this.nombre_documento = nombre_documento;
        this.pagina = pagina;
        this.ruta_documento = ruta_documento;
    }

    public int getVersion_documento_id() {
        return version_documento_id;
    }

    public void setVersion_documento_id(int version_documento_id) {
        this.version_documento_id = version_documento_id;
    }

    public int getVersion_id() {
        return version_id;
    }

    public void setVersion_id(int version_id) {
        this.version_id = version_id;
    }

    public int getDocumento_id() {
        return documento_id;
    }

    public void setDocumento_id(int documento_id) {
        this.documento_id = documento_id;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getNombre_documento() {
        return nombre_documento;
    }

    public void setNombre_documento(String nombre_docuemnto) {
        this.nombre_documento = nombre_docuemnto;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }

    public String getRuta_documento() {
        return ruta_documento;
    }

    public void setRuta_documento(String ruta_documento) {
        this.ruta_documento = ruta_documento;
    }
    
}
