/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.dominio;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Rothkegel
 */
@Entity
        @Table(name = "tipodocumento")
        @NamedQueries({
            @NamedQuery(name = "TipoDocumento.buscarTodos",
                    query = "SELECT td "
                    + "        FROM TipoDocumento td "
                    + "        ORDER BY td.tipo_id"),
            @NamedQuery(name = "TipoDocumento.buscarTodosTipoDocumento",
                    query = "SELECT NEW com.tic.carpetas.dto.TipoDocumentoDTO(p, p.categoriadocumento)"
                            + " FROM TipoDocumento p ORDER BY p.tipo_id "),
            @NamedQuery(name = "TipoDocumento.getTipoDocumentoPorIdCategoria",
                    query = "SELECT td FROM TipoDocumento td WHERE"
                            + " td.categoriadocumento.categoria_id = :categoria_id"),
            @NamedQuery(name = "TipoDocumento.buscarTodosTipoDocumentoPorTexto",
                    query = "SELECT td FROM TipoDocumento td WHERE "
                    + "lower(td.nombreTipoDocumento) like :nombre_tipo_documento" ),
            @NamedQuery(name = "TipoDocumento.eliminarPorId",
                    query = "DELETE FROM TipoDocumento td WHERE "
                    + " td.tipo_id = :tipo_id" )
})
public class TipoDocumento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tipo_id")
    private int tipo_id;

    @Column(name = "nombre_tipo_documento")
    private String nombreTipoDocumento;

    @OneToMany(mappedBy = "tipodocumento", cascade = CascadeType.MERGE)
    private List<Documento> documentos;

    @ManyToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "categoria_id", referencedColumnName = "categoria_id")
    private CategoriaDocumento categoriadocumento;

    public TipoDocumento() {
    }

    public TipoDocumento(int tipo_id, String nombreTipoDocumento, List<Documento> documentos, CategoriaDocumento categoriadocumento) {
        this.tipo_id = tipo_id;
        this.nombreTipoDocumento = nombreTipoDocumento;
        this.documentos = documentos;
        this.categoriadocumento = categoriadocumento;
    }

    public int getTipo_id() {
        return tipo_id;
    }

    public void setTipo_id(int tipo_id) {
        this.tipo_id = tipo_id;
    }

    public String getNombreTipoDocumento() {
        return nombreTipoDocumento;
    }

    public void setNombreTipoDocumento(String nombreTipoDocumento) {
        this.nombreTipoDocumento = nombreTipoDocumento;
    }

    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Documento> documentos) {
        this.documentos = documentos;
    }

    public CategoriaDocumento getCategoriadocumento() {
        return categoriadocumento;
    }

    public void setCategoriadocumento(CategoriaDocumento categoriadocumento) {
        this.categoriadocumento = categoriadocumento;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.tipo_id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoDocumento other = (TipoDocumento) obj;
        if (!Objects.equals(this.tipo_id, other.tipo_id)) {
            return false;
        }
        return true;
    }

}
