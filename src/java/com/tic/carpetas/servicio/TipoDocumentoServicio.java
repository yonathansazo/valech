package com.tic.carpetas.servicio;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.tic.carpetas.dao.CategoriaDocumentoDAO;
import com.tic.carpetas.dao.DocumentoDAO;
import com.tic.carpetas.dao.TipoDocumentoDAO;
import com.tic.carpetas.dominio.CategoriaDocumento;
import com.tic.carpetas.dominio.Documento;
import com.tic.carpetas.dominio.TipoDocumento;
import com.tic.carpetas.dto.TipoDocumentoDTO;
import com.tic.rbac.servicio.PlantillaServicio;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Rothkegel
 */
@Stateless
public class TipoDocumentoServicio {

    @EJB
    private TipoDocumentoDAO tipoDocumentoDao;

    @EJB
    private DocumentoServicio documentoServicio;
    
    @EJB
    private DocumentoDAO documentoDAO;

    @EJB
    private CategoriaDocumentoServicio categoriaDocumentoServicio;

    @EJB
    private CategoriaDocumentoDAO categoriaDocumentoDAO;

    @EJB
    private PlantillaServicio plantillaServicio;

    public int crearTipoDocumento(TipoDocumentoDTO dto) {
        System.out.println("crearTipoDocumento");
        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento = convertirDTOATipoDocumento(dto);
        tipoDocumentoDao.insertarTipoDocumentos(dto);
        return 1;
    }

    public TipoDocumentoDTO buscarTipoDocumento(int id) {
        System.out.println("buscarTipoDocumento");
        TipoDocumentoDTO subDTO = new TipoDocumentoDTO();
        subDTO = this.convertirTipoDocumentoADTO(tipoDocumentoDao.buscarTipoDocumento(id));

        return subDTO;

    }

    public void actualizarTipoDocumento(TipoDocumentoDTO dto) {
        System.out.println("actualizarTipoDocumento");
        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento = convertirDTOATipoDocumento(dto);
        tipoDocumentoDao.actualizarTipoDocumento(tipoDocumento);

    }

    public void eliminarTipoDocumento(int id) {
        System.out.println("eliminarTipoDocumento");
        List<Documento> lis = documentoDAO.buscarDocumentoPorTipoDocumento(id);

        if (lis.size() < 1) {
            plantillaServicio.eliminarPlantillaPorId(id);
            tipoDocumentoDao.eliminarPorId(id);
        }

    }

    public List<TipoDocumentoDTO> listarTipoDocumentos() {
        System.out.println("listarTipoDocumentos");
        List<TipoDocumentoDTO> listaDTO = new ArrayList<TipoDocumentoDTO>();
        listaDTO = tipoDocumentoDao.listarTodosTipoDocumentos();
        return listaDTO;

    }

    public List<TipoDocumentoDTO> getTipoDocumentoPorIdCategoria(int categoria_id) {
        System.out.println("getTipoDocumentoPorIdCategoria");

        List<TipoDocumentoDTO> listaDTO = new ArrayList<TipoDocumentoDTO>();
        listaDTO = convertirArregloTipoDocumentoADTO(tipoDocumentoDao.getTipoDocumentoPorIdCategoria(categoria_id));
        return listaDTO;

    }

    public List<TipoDocumentoDTO> listarTipoDocumentosPorTexto(String texto) {
        System.out.println("listarTipoDocumentosPorTexto");
        List<TipoDocumentoDTO> listaDTO = new ArrayList<TipoDocumentoDTO>();
        listaDTO = convertirArregloTipoDocumentoADTO(tipoDocumentoDao.buscarTodosTipoDocumentoPorTexto(texto));
        return listaDTO;

    }

    public TipoDocumento convertirDTOATipoDocumento(TipoDocumentoDTO dto) {
        System.out.println("convertirDTOATipoDocumento");
        TipoDocumento tipoDocumento = new TipoDocumento();
        CategoriaDocumento categoriaDocumento = new CategoriaDocumento();
        categoriaDocumento.setCategoria_id(dto.getCategoria_id());
        categoriaDocumento.setNombreCategoria(dto.getNombreCategoria());
        tipoDocumento.setCategoriadocumento(categoriaDocumento);
        tipoDocumento.setDocumentos(new ArrayList<Documento>());
        tipoDocumento.setNombreTipoDocumento(dto.getNombreTipoDocumento());
        tipoDocumento.setTipo_id(dto.getTipo_id());
        return tipoDocumento;
    }

    public List<TipoDocumento> convertirArregloDTOATipoDocumento(List<TipoDocumentoDTO> dto) {
        System.out.println("convertirArregloDTOATipoDocumento");
        List<TipoDocumento> listTipoDocumento = new ArrayList<TipoDocumento>();
        for (TipoDocumentoDTO listTipoDocumentoDTO : dto) {
            listTipoDocumento.add(convertirDTOATipoDocumento(listTipoDocumentoDTO));
        }
        return listTipoDocumento;
    }

    public TipoDocumentoDTO convertirTipoDocumentoADTO(TipoDocumento tipoDocumento) {
        System.out.println("convertirTipoDocumentoADTO");
        TipoDocumentoDTO tipoDocumentoDTO = new TipoDocumentoDTO();
        tipoDocumentoDTO.setTipo_id(tipoDocumento.getTipo_id());
        tipoDocumentoDTO.setNombreTipoDocumento(tipoDocumento.getNombreTipoDocumento());
        return tipoDocumentoDTO;
    }

    public ArrayList<TipoDocumentoDTO> convertirArregloTipoDocumentoADTO(List<TipoDocumento> tipoDocumento) {
        System.out.println("convertirArregloTipoDocumentoADTO");
        ArrayList<TipoDocumentoDTO> listTipoDocumentoDTO = new ArrayList<TipoDocumentoDTO>();
        for (TipoDocumento listTipoDocumento : tipoDocumento) {
            listTipoDocumentoDTO.add(convertirTipoDocumentoADTO(listTipoDocumento));
        }
        return listTipoDocumentoDTO;
    }

}
