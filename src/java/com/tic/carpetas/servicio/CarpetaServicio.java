/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.servicio;

import com.tic.carpetas.dto.*;
import com.tic.carpetas.dao.*;
import com.tic.carpetas.dominio.Carpeta;
import com.tic.carpetas.dominio.Documento;
import com.tic.utils.TablaValores;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Rothkegel
 */
@Stateless
public class CarpetaServicio {

    @EJB
    private CarpetaDAO carpetaDao;

    @EJB
    private DocumentoDAO documentoDAO;

    @EJB
    private DocumentoServicio documentoServicio;

    public int crearCarpeta(CarpetaDTO dto) {

        System.out.println("crearCarpeta");
        Carpeta carpeta = new Carpeta();
        carpeta = convertirDTOACarpeta(dto);

        int id_carpeta = buscarCarpetaPorLlaves(dto.getCarpeta(), dto.getNro_caja(), dto.getComision(), dto.isCalifica(), dto.getLetra_caja(), dto.getLetra_carpeta(), dto.getSerie());
        CarpetaDTO dtoEncontrada = buscarCarpeta(id_carpeta);
        if (dtoEncontrada == null) {
            return carpetaDao.crearCarpeta(carpeta);
        }
        carpeta = convertirDTOACarpeta(dtoEncontrada);
        carpeta.setRutapdfnotario(dto.getRutapdfnotario());
        carpeta.setNombrepdfnotario(dto.getNombrepdfnotario());
        return carpetaDao.actualizarCarpeta(carpeta);

    }

    public CarpetaDTO buscarCarpetaPorNumero(int numeroCarpeta) {
        System.out.println("buscarCarpetaPorNumero");
        Carpeta carpeta = carpetaDao.buscarCarpetaPorNumero(numeroCarpeta);
        if (carpeta != null) {
            CarpetaDTO caroetaDTO = convertirCarpetaADTO(carpeta);
            return caroetaDTO;
        }
        return null;

    }

    public CarpetaDTO buscarCarpeta(int id) {
        System.out.println("buscarCarpeta");
        Carpeta carpeta = carpetaDao.buscarCarpeta(id);
        if (carpeta != null) {
            CarpetaDTO carpetaDTO = convertirCarpetaADTO(carpeta);
            List<DocumentoDTO> documentosDTO = new ArrayList();
            documentosDTO = documentoServicio.listarDocumentosPorCarpeta(carpeta.getCarpeta_id());
            carpetaDTO.setDocumentos(documentosDTO);
            return carpetaDTO;
        }
        return null;

    }

    public int buscarCarpetaPorLlaves(int carpeta, int caja, int comision, boolean califica, String letra_caja, String letra_carpeta, String serie) {

        int carpeta_id = carpetaDao.buscarCarpetaPorLlaves(carpeta, caja, comision, califica, letra_caja, letra_carpeta, serie);
        return carpeta_id;

    }

    public void actualizarCarpeta(CarpetaDTO dto) {
        int id_carpeta = this.carpetaDao.buscarCarpetaPorLlaves(dto.getCarpeta(), dto.getNro_caja(), dto.getComision(), dto.isCalifica(), dto.getLetra_caja(), dto.getLetra_carpeta(), dto.getSerie());
        if (id_carpeta != 0) {
            Carpeta carpeta = new Carpeta();
            dto.setCarpeta_id(id_carpeta);
            carpeta = convertirDTOACarpeta(dto);
            this.carpetaDao.actualizarCarpeta(carpeta);
            documentoServicio.actualizaTotales(id_carpeta);
        } else {
            Carpeta carpeta = new Carpeta();
            carpeta = convertirDTOACarpeta(dto);
            if(carpeta.getDv()=='\u0000'){
                carpeta.setDv('0');
            }
            this.carpetaDao.crearCarpeta(carpeta);
        }

    }

    public void eliminarCarpeta(int id) {
        System.out.println("eliminarCarpeta");
        documentoDAO.eliminarDocumentoPorCarpetaId(id);
    }

    public void eliminarPdfNotario(int id) {
        System.out.println("eliminarPDFNotario");
        Carpeta carpeta = carpetaDao.buscarCarpeta(id);
        carpeta.setNombrepdfnotario(null);
        carpeta.setRutapdfnotario(null);
        carpetaDao.actualizarCarpeta(carpeta);
    }

    public ArrayList<CarpetaDTO> listarCarpetas() {
        System.out.println("listarCarpetas");
        ArrayList<CarpetaDTO> listaDTO = new ArrayList();
        listaDTO = convertirArregloCarpetaADTO(carpetaDao.listarCarpetas());
        return listaDTO;

    }

    public Carpeta convertirDTOACarpeta(CarpetaDTO dto) {
        System.out.println("convertirDTOACarpeta");
        Carpeta carpeta = new Carpeta();
        carpeta.setNombres(dto.getNombres());
        carpeta.setApellidos(dto.getApellidos());
        carpeta.setCarpeta(dto.getCarpeta());
        carpeta.setIdentificador(dto.getIdentificador());
        carpeta.setApela(dto.isApela());
        carpeta.setCalifica(dto.isCalifica());
        carpeta.setComision(dto.getComision());
        carpeta.setNro_caja(dto.getNro_caja());
        carpeta.setRut(dto.getRut());
        carpeta.setDv(dto.getDv());
        carpeta.setCarpeta_id(dto.getCarpeta_id());
        carpeta.setDocumentos(new ArrayList<Documento>());
        carpeta.setNumeropdf(dto.getNumeropdf());
        carpeta.setNumerodocumentos(dto.getNumerodocumentos());
        carpeta.setNumeropaginas(dto.getNumeropaginas());
        carpeta.setNombrepdfnotario(dto.getNombrepdfnotario());
        carpeta.setRutapdfnotario(dto.getRutapdfnotario());
        carpeta.setLetra_caja(dto.getLetra_caja());
        carpeta.setLetra_carpeta(dto.getLetra_carpeta());
        carpeta.setSerie(dto.getSerie());
        carpeta.setTipo_carpeta(dto.getTipo_carpeta());
        carpeta.setDeclara(dto.getDeclara());

        return carpeta;
    }

    public List<Carpeta> convertirArregloDTOACarpeta(List<CarpetaDTO> dto) {
        System.out.println("convertirArregloDTOACarpeta");
        List<Carpeta> listCarpeta = new ArrayList();
        for (CarpetaDTO listCarpetaDTO : dto) {
            listCarpeta.add(convertirDTOACarpeta(listCarpetaDTO));
        }
        return listCarpeta;
    }

    public CarpetaDTO convertirCarpetaADTO(Carpeta carpeta) {
        System.out.println("convertirCarpetaADTO");
        CarpetaDTO carpetaDTO = new CarpetaDTO();
        carpetaDTO.setDocumentos(new ArrayList<DocumentoDTO>());
        carpetaDTO.setCarpeta_id(carpeta.getCarpeta_id());
        carpetaDTO.setNombres(carpeta.getNombres());
        carpetaDTO.setApellidos(carpeta.getApellidos());
        carpetaDTO.setCarpeta(carpeta.getCarpeta());
        carpetaDTO.setIdentificador(carpeta.getIdentificador());
        carpetaDTO.setApela(carpeta.isApela());
        carpetaDTO.setCalifica(carpeta.isCalifica());
        carpetaDTO.setComision(carpeta.getComision());
        carpetaDTO.setNro_caja(carpeta.getNro_caja());
        carpetaDTO.setRut(carpeta.getRut());
        carpetaDTO.setDv(carpeta.getDv());
        carpetaDTO.setNumeropdf(carpeta.getNumeropdf());
        carpetaDTO.setNumerodocumentos(carpeta.getNumerodocumentos());
        carpetaDTO.setNumeropaginas(carpeta.getNumeropaginas());
        carpetaDTO.setLetra_caja(carpeta.getLetra_caja());
        carpetaDTO.setLetra_carpeta(carpeta.getLetra_carpeta());
        carpetaDTO.setSerie(carpeta.getSerie());
        carpetaDTO.setTipo_carpeta(carpeta.getTipo_carpeta());
        carpetaDTO.setNombrepdfnotario(carpeta.getNombrepdfnotario());
        carpetaDTO.setRutapdfnotario(carpeta.getRutapdfnotario());
        carpetaDTO.setDeclara(carpeta.getDeclara());

        return carpetaDTO;
    }

    public ArrayList<CarpetaDTO> convertirArregloCarpetaADTO(List<Carpeta> carpeta) {
        System.out.println("convertirArregloCarpetaADTO");
        ArrayList<CarpetaDTO> listCarpetaDTO = new ArrayList();
        for (Carpeta listCarpeta : carpeta) {
            listCarpetaDTO.add(convertirCarpetaADTO(listCarpeta));
        }
        return listCarpetaDTO;
    }

    public List<CarpetaDTO> listarCarpetasPaginadas(CarpetaDTO carpeta) {
        System.out.println("listarCarpetasPaginadas");
        int cantidadRegistros = Integer.parseInt(TablaValores.getValor("almacenamiento.parametros", "PAGINACION", "cantidadPagina"));
        return convertirArregloCarpetaADTO(carpetaDao.listarCarpetaPaginada(carpeta, cantidadRegistros, carpeta.getNumeropdf(), "carpeta_id"));
    }

    public CarpetaDTO listarPaginacion(CarpetaDTO carpeta) {
        System.out.println("listarPaginacion");
        int cantidadRegistros = Integer.parseInt(TablaValores.getValor("almacenamiento.parametros", "PAGINACION", "cantidadPagina"));
        return carpetaDao.listarPaginacion(carpeta, cantidadRegistros);
    }

    public void eliminarDocPorCarpetas(int carpeta_id) {
        carpetaDao.eliminarDocPorCarpetas(carpeta_id);
    }

}
