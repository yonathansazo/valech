package com.tic.carpetas.servicio;

import com.tic.carpetas.dao.CarpetaDAO;
import com.tic.carpetas.dao.DocumentoDAO;
import com.tic.carpetas.dominio.Carpeta;
import com.tic.carpetas.dominio.Documento;
import com.tic.carpetas.dto.CarpetaDTO;
import com.tic.carpetas.dto.DocumentoDTO;
import com.tic.carpetas.dto.TipoDocumentoDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rothkegel
 */
@Stateless
public class DocumentoServicio {

    @EJB
    private DocumentoDAO documentoDao;

    @EJB
    private CarpetaServicio carpetaServicio;

    @EJB
    private CarpetaDAO carpetaDao;

    @EJB
    private TipoDocumentoServicio tipoDocumentoServicio;

    public int crearDocumento(DocumentoDTO dto) {
        System.out.println("crearDocumento");
        Documento documento = new Documento();
        documento = convertirDTOADocumento(dto);
        if (documento.getDocumento_id() != 0) {
            documentoDao.actualizarDocumento(documento);
            return documento.getDocumento_id();
        } else {
            return documentoDao.crearDocumento(documento);
        }

    }
    
    public void actualizaTotales(int carpeta_id){
        List<Documento> documentos = this.documentoDao.buscarDocumentoPorCarpetaNativa(carpeta_id);
        int totalPaginas = 0;
        int totalPdf = 0;
        int totalDocumentos = 0;
        System.out.println(documentos.size());
        for(Documento doc: documentos){
            System.out.println(doc.getPaginas());
            totalPaginas += doc.getPaginas();
            totalPdf += doc.getNumeropdf();
        }
        totalDocumentos += documentos.size();
        Carpeta carpeta = this.carpetaDao.buscarCarpeta(carpeta_id);
        
        carpeta.setNumerodocumentos(totalDocumentos);
        carpeta.setNumeropaginas(totalPaginas);
        carpeta.setNumeropdf(totalPdf);
        
        this.carpetaDao.actualizarCarpeta(carpeta);
    
    }

    public DocumentoDTO buscarDocumento(int id) {
        System.out.println("buscarDocumento");
        Documento documento = documentoDao.buscarDocumentoPorId(id);
        DocumentoDTO documentoDTO = convertirDocumentoADTO(documento);
        return documentoDTO;

    }

    public void actualizarDocumento(List<DocumentoDTO> dto1) {
        System.out.println("actualizarDocumento");

        int hojasPDF = 0;
        int hojasDoc = 0;
        List<Documento> lista = new ArrayList<Documento>();
        for (DocumentoDTO dto : dto1) {

            if (dto.getDocumento_id() != 0) {
                Documento documento = new Documento();
                documento = convertirDTOADocumento(dto);
                Documento documentoBusqueda = documentoDao.buscarDocumentoPorId(dto.getDocumento_id());
                documento.setNombreDocumento(documentoBusqueda.getNombreDocumento());
                documento.setRutaDocumento(documentoBusqueda.getRutaDocumento());
                hojasPDF = hojasPDF + dto.getNumeropdf();
                hojasDoc = hojasDoc + dto.getPaginas();
                lista.add(documentoDao.actualizarDocumento(documento));
            }

        }


        Carpeta carpeta = carpetaDao.buscarCarpeta(dto1.get(0).getCarpeta_id());

        carpeta.setNumeropdf(hojasPDF);
        carpeta.setNumeropaginas(hojasDoc);
        carpeta.setNumerodocumentos(dto1.size());

        carpetaDao.actualizarCarpeta(carpeta);

    }

    public void eliminarDocumento(int id) {
        System.out.println("eliminarDocumento");
        documentoDao.eliminarDocumentoRestar(id);
        
    }

    public void eliminarDocumentoPorCarpetaId(int id) {
        System.out.println("eliminarDocumentoPorCarpetaId");
        documentoDao.eliminarDocumentoPorCarpetaId(id);
    }

    public ArrayList<DocumentoDTO> listarDocumentos() {
        System.out.println("listarDocumentos");
        ArrayList<DocumentoDTO> listaDTO = new ArrayList<DocumentoDTO>();
        listaDTO = convertirArregloDocumentoADTO(documentoDao.listarDocumentos());
        return listaDTO;

    }

    public List<DocumentoDTO> listarDocumentosPorCarpeta(int id_carpeta) {
        Carpeta carpeta = carpetaDao.buscarCarpeta(id_carpeta);
        List<Documento> docs = new ArrayList<Documento>();
        List<DocumentoDTO> docu = new ArrayList<DocumentoDTO>();
        docs = documentoDao.buscarDocumentoPorCarpetaNativa(carpeta.getCarpeta_id());
        for (Documento doc : docs) {
            docu.add(convertirDocumentoADTO(documentoDao.buscarDocumentoPorIdNativo(doc.getDocumento_id())));
        }
        return docu;

    }

    public List<DocumentoDTO> listarDocumentosPorIdCarpeta(int id_carpeta) {

        return convertirArregloDocumentoADTO(documentoDao.buscarDocumentoPorCarpetaNativa(id_carpeta));
    }

    public Documento convertirDTOADocumento(DocumentoDTO dto) {
        System.out.println("convertirDTOADocumento");
        Documento documento = new Documento();
        documento.setCarpeta_id(dto.getCarpeta_id());
        documento.setComentario(dto.getComentario());
        documento.setDocumento_id(dto.getDocumento_id());
        documento.setFolioFinal(dto.getFolioFinal());
        documento.setFolioInicial(dto.getFolioInicial());
        documento.setNombreDocumento(dto.getNombreDocumento());
        documento.setPaginas(dto.getPaginas());
        documento.setRutaDocumento(dto.getRutaDocumento());
        documento.setTipo_id(dto.getTipo_id());
        documento.setActiva(dto.isActiva());
        documento.setPreservacion(dto.isPreservacion());
        documento.setNumeropdf(dto.getNumeropdf());
        documento.setLetra_caja(dto.getLetra_caja());
        documento.setLetra_carpeta(dto.getLetra_carpeta());

        return documento;
    }

    public List<Documento> convertirArregloDTOADocumento(List<DocumentoDTO> dto) {
        System.out.println("convertirArregloDTOADocumento");
        List<Documento> listDocumento = new ArrayList<Documento>();
        for (DocumentoDTO listDocumentoDTO : dto) {
            listDocumento.add(convertirDTOADocumento(listDocumentoDTO));
        }
        return listDocumento;
    }

    public DocumentoDTO convertirDocumentoADTO(Documento documento) {
        System.out.println("convertirDocumentoADTO");
        DocumentoDTO documentoDTO = new DocumentoDTO();
        documentoDTO.setCarpeta_id(documento.getCarpeta_id());
        documentoDTO.setComentario(documento.getComentario());
        documentoDTO.setDocumento_id(documento.getDocumento_id());
        documentoDTO.setFolioFinal(documento.getFolioFinal());
        documentoDTO.setFolioInicial(documento.getFolioInicial());
        documentoDTO.setNombreDocumento(documento.getNombreDocumento());
        documentoDTO.setPaginas(documento.getPaginas());
        documentoDTO.setActiva(documento.isActiva());
        documentoDTO.setRutaDocumento(documento.getRutaDocumento());
        documentoDTO.setTipo_id(documento.getTipo_id());
        documentoDTO.setPreservacion(documento.isPreservacion());
        documentoDTO.setNumeropdf(documento.getNumeropdf());
        documentoDTO.setNombreTipoDocumento(tipoDocumentoServicio.buscarTipoDocumento(documento.getTipo_id()).getNombreTipoDocumento());
        return documentoDTO;
    }

    public ArrayList<DocumentoDTO> convertirArregloDocumentoADTO(List<Documento> documento) {
        System.out.println("convertirArregloDocumentoADTO");
        ArrayList<DocumentoDTO> listDocumentoDTO = new ArrayList<DocumentoDTO>();
        for (Documento listDocumento : documento) {
            listDocumentoDTO.add(convertirDocumentoADTO(listDocumento));
        }
        return listDocumentoDTO;
    }

}
