/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.servicio;

import com.tic.carpetas.dao.VersionDocumentoDAO;
import com.tic.carpetas.dominio.VersionDocumento;
import com.tic.carpetas.dto.VersionDocumentoDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author jsilva
 */
@Stateless
public class VersionDocumentoServicio {
    
    @EJB
    private VersionDocumentoDAO versionDocumentoDao;
    
     public int crearVersionDocumento(VersionDocumentoDTO dto) {

        System.out.println("crearVersionDocumento");
        VersionDocumento versionDocumento = new VersionDocumento();
        versionDocumento = convertirDTOAVersionDocumento(dto);
        int id_version_documento = dto.getVersion_documento_id();
        
        if (id_version_documento == 0) {
            return versionDocumentoDao.crearVersionDocumento(versionDocumento);
        }
        return 0;
    };
     
     public int crearVersionDocumentos(List<VersionDocumentoDTO> lista, int id_version) {
         ArrayList<VersionDocumento> listVersionDocumento = new ArrayList();
         for (VersionDocumentoDTO item : lista) {
             versionDocumentoDao.crearVersionDocumento(convertirDTOAVersionDocumentoConID(item, id_version));
         }
         return 0;
     };
       
    public ArrayList<VersionDocumentoDTO> listarVersionesDocumentos(int id) {
       System.out.println("listarVersiones");
       ArrayList<VersionDocumentoDTO> listaDTO = new ArrayList();
       listaDTO = convertirArregloVersionADTO(versionDocumentoDao.listarVersionesDocumento(id));
       return listaDTO;

    }    
    
       
    public ArrayList<VersionDocumentoDTO> convertirArregloVersionADTO(List<VersionDocumento> versionDocumento) {
        System.out.println("convertirArregloVersionADTO");
        ArrayList<VersionDocumentoDTO> listVersionDocumentoDTO = new ArrayList();
        for (VersionDocumento listVersionDocumento : versionDocumento) {
            listVersionDocumentoDTO.add(convertirVersionDocumentoADTO(listVersionDocumento));
        }
        return listVersionDocumentoDTO;
    }
   
     public ArrayList<VersionDocumento> convertirArregloDTOVersionDocumento(List<VersionDocumentoDTO> versionDocumentoDTO) {
        System.out.println("convertirArregloVersionADTO");
        ArrayList<VersionDocumento> listVersionDocumento = new ArrayList();
        for (VersionDocumentoDTO listVersionDocumentoDTO : versionDocumentoDTO) {
            listVersionDocumento.add(convertirDTOAVersionDocumento(listVersionDocumentoDTO));
        }
        return listVersionDocumento;
    }
    
    public VersionDocumento convertirDTOAVersionDocumento(VersionDocumentoDTO dto) {
        System.out.println("convertirDTOAVersionDocumento");
        VersionDocumento versionDocumento = new VersionDocumento();
         versionDocumento.setVersion_id(dto.getVersion_id());
         versionDocumento.setDocumento_id(dto.getDocumento_id());
         versionDocumento.setIdentificador(dto.getIdentificador());
         versionDocumento.setNombre_documento(dto.getNombre_documento());
         versionDocumento.setPagina(dto.getPagina());
         versionDocumento.setRuta_documento(dto.getRuta_documento());
        
        return versionDocumento;
    }

    public VersionDocumento convertirDTOAVersionDocumentoConID(VersionDocumentoDTO dto, int id) {
        System.out.println("convertirDTOAVersionDocumentoConID");
        VersionDocumento versionDocumento = new VersionDocumento();
        versionDocumento.setVersion_id(id);
        versionDocumento.setDocumento_id(dto.getDocumento_id());
        versionDocumento.setIdentificador(dto.getIdentificador());
        versionDocumento.setNombre_documento(dto.getNombre_documento());
        versionDocumento.setPagina(dto.getPagina());
        versionDocumento.setRuta_documento(dto.getRuta_documento());
        
        return versionDocumento;
    }
    
    public VersionDocumentoDTO convertirVersionDocumentoADTO(VersionDocumento versionDocumento) {
        System.out.println("convertirVersionDocumentoADTO");
        VersionDocumentoDTO versionDocumentoDTO = new VersionDocumentoDTO();
        versionDocumentoDTO.setVersion_id(versionDocumento.getVersion_id());
        versionDocumentoDTO.setDocumento_id(versionDocumento.getDocumento_id());
        versionDocumentoDTO.setIdentificador(versionDocumento.getIdentificador());
        versionDocumentoDTO.setNombre_documento(versionDocumento.getNombre_documento());
        versionDocumentoDTO.setPagina(versionDocumento.getPagina());
        versionDocumentoDTO.setRuta_documento(versionDocumento.getRuta_documento());
     
        return versionDocumentoDTO;
    } 
    
    public void eliminarVersionDocumentoPorVersionId(int version_id) {
        versionDocumentoDao.eliminarVersionDocumentos(version_id);
    }
    
}
