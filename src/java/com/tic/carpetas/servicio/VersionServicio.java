/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.servicio;

import com.tic.carpetas.dao.VersionDAO;
import com.tic.carpetas.dominio.Version;
//import com.tic.carpetas.servicio.VersionDocumentoServicio;
import com.tic.carpetas.dto.VersionDTO;
import com.tic.utils.TablaValores;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author jsilva
 */
@Stateless
public class VersionServicio {
    
    @EJB
    private VersionDAO versionDao;
    
    VersionDocumentoServicio versionDocumentoServicio;
     public int crearVersion(VersionDTO dto) {

        System.out.println("crearVersion");
        Version version = new Version();
        version = convertirDTOAVersion(dto);
        int id_version = dto.getVersion_id();
        
        if (id_version == 0) {
            versionDao.setEstadoCarpeta(version.getCarpeta_id());
            return versionDao.crearVersion(version);
        }
        VersionDTO dtoEncontrada = buscarVersion(id_version);
        version = convertirDTOAVersion(dtoEncontrada);
        return versionDao.actualizarVersion(version);

    }
    
       public VersionDTO buscarVersion(int id) {
        System.out.println("buscarVersion");
        Version version = versionDao.buscarVersion(id);
        if (version != null) {
            VersionDTO versionDTO = convertirVersionADTO(version);
            return versionDTO;
        }
        return null;

    } 
       
    public ArrayList<VersionDTO> listarVersionesCarpeta(int id) {
       System.out.println("listarVersiones");
       ArrayList<VersionDTO> listaDTO = new ArrayList();
       listaDTO = convertirArregloVersionADTO(versionDao.listarVersionesCarpeta(id));
       return listaDTO;

    }    
       
    public ArrayList<VersionDTO> listarVersiones() {
        System.out.println("listarVersiones");
        ArrayList<VersionDTO> listaDTO = new ArrayList();
        listaDTO = convertirArregloVersionADTO(versionDao.listarVersiones());
        return listaDTO;

    } 
       
    public ArrayList<VersionDTO> convertirArregloVersionADTO(List<Version> version) {
        System.out.println("convertirArregloVersionADTO");
        ArrayList<VersionDTO> listVersionDTO = new ArrayList();
        for (Version listVersion : version) {
            listVersionDTO.add(convertirVersionADTO(listVersion));
        }
        return listVersionDTO;
    }
   
     
    public Version convertirDTOAVersion(VersionDTO dto) {
        System.out.println("convertirDTOAVersion");
        Version version = new Version();
        version.setVersion_id(dto.getVersion_id());
        version.setNro_interno(dto.getNro_interno());
        version.setCaja(dto.getCaja());
        version.setCarpeta(dto.getCarpeta());
        version.setLetra_caja(dto.getLetra_caja());
        version.setLetra_carpeta(dto.getLetra_carpeta());
        version.setFecha(dto.getFecha());
        version.setEstado(dto.getEstado());
        version.setDeclaracion(dto.getDeclaracion());
        version.setCarpeta_id(dto.getCarpeta_id());
        version.setEncabezado(dto.getEncabezado());
        version.setConclusion(dto.getConclusion());
        version.setRut(dto.getRut());
        version.setNombre(dto.getNombre());
        version.setIdentificador(dto.getIdentificador());
        version.setTipo_carpeta(dto.getTipo_carpeta());
        version.setSerie(dto.getSerie());
        version.setComision(dto.getComision());
        version.setCalifica(dto.isCalifica());
        version.setApela(dto.isApela());
        version.setDocumento_id(dto.getDocumento_id());
        
        return version;
    }

    
    public VersionDTO convertirVersionADTO(Version version) {
        System.out.println("convertirVersionADTO");
        VersionDTO versionDTO = new VersionDTO();
        versionDTO.setVersion_id(version.getVersion_id());
        versionDTO.setNro_interno(version.getNro_interno());
        versionDTO.setCaja(version.getCaja());
        versionDTO.setCarpeta(version.getCarpeta());
        versionDTO.setLetra_caja(version.getLetra_caja());
        versionDTO.setLetra_carpeta(version.getLetra_carpeta());
        versionDTO.setFecha(version.getFecha());
        versionDTO.setEstado(version.getEstado());
        versionDTO.setDeclaracion(version.getDeclaracion());
        versionDTO.setCarpeta_id(version.getCarpeta_id());
        versionDTO.setEncabezado(version.getEncabezado());
        versionDTO.setConclusion(version.getConclusion());
        versionDTO.setRut(version.getRut());
        versionDTO.setNombre(version.getNombre());
        versionDTO.setIdentificador(version.getIdentificador());
        versionDTO.setTipo_carpeta(version.getTipo_carpeta());
        versionDTO.setSerie(version.getSerie());
        versionDTO.setComision(version.getComision());
        versionDTO.setCalifica(version.isCalifica());
        versionDTO.setApela(version.isApela());
        versionDTO.setDocumento_id(version.getDocumento_id());
        return versionDTO;
    } 
     
    public List<VersionDTO> listarVersionesPaginadas(VersionDTO version) {
        System.out.println("listarCarpetasPaginadas");
        int cantidadRegistros = Integer.parseInt(TablaValores.getValor("almacenamiento.parametros", "PAGINACION", "cantidadPagina"));
        return convertirArregloVersionADTO(versionDao.listarCarpetaPaginada(version, cantidadRegistros, 1,"version_id"));
    }

    public VersionDTO listarPaginacion(VersionDTO version) {
        System.out.println("listarPaginacion");
        int cantidadRegistros = Integer.parseInt(TablaValores.getValor("almacenamiento.parametros", "PAGINACION", "cantidadPagina"));
        return versionDao.listarPaginacion(version, cantidadRegistros);
    }
    
    public void eliminarVersionPorVersionId(int version_id){
        versionDao.setEstadoCarpetaDVersion(version_id);
        versionDao.eliminarVersionPorVersionId(version_id);
    }
    
}
