/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.carpetas.servicio;

import com.tic.carpetas.dto.*;
import com.tic.carpetas.dao.*;
import com.tic.carpetas.dominio.CategoriaDocumento;
import com.tic.carpetas.dominio.TipoDocumento;
import com.tic.rbac.servicio.PlantillaServicio;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Rothkegel
 */
@Stateless
public class CategoriaDocumentoServicio {

    @EJB
    private CategoriaDocumentoDAO categoriaDocumentoDao;

    @EJB
    private TipoDocumentoServicio tipoDocumentoServicio;

    @EJB
    private TipoDocumentoDAO tipoDocumentoDAO;

    @EJB
    private PlantillaServicio plantillaServicio;

    public int crearCategoriaDocumento(CategoriaDocumentoDTO dto) {
        System.out.println("crearCategoriaDocumento");
        CategoriaDocumento categoriaDocumento = new CategoriaDocumento();
        categoriaDocumento = convertirDTOACategoriaDocumento(dto);
        return categoriaDocumentoDao.crearCategoriaDocumento(categoriaDocumento);
    }

    public CategoriaDocumentoDTO buscarCategoriaDocumento(int id) {
        System.out.println("buscarCategoriaDocumento");
        CategoriaDocumento categoriaDocumento = categoriaDocumentoDao.buscarCategoriaDocumento(id);
        CategoriaDocumentoDTO caroetaDTO = convertirCategoriaDocumentoADTO(categoriaDocumento);
        return caroetaDTO;

    }

    public CategoriaDocumentoDTO buscarCategoriaDocumentoPalabra(String id) {
        System.out.println("buscarCategoriaDocumentoPalabra");
        return this.convertirCategoriaDocumentoADTO(categoriaDocumentoDao.buscarCategoriaDocumentoPalabra());

    }

    public void actualizarCategoriaDocumento(CategoriaDocumentoDTO dto) {
        System.out.println("actualizarCategoriaDocumento");
        CategoriaDocumento categoriaDocumento = new CategoriaDocumento();
        categoriaDocumento = convertirDTOACategoriaDocumento(dto);
        categoriaDocumentoDao.actualizarCategoriaDocumento(categoriaDocumento);

    }

    public void eliminarCategoriaDocumento(int id) {
        System.out.println("eliminarCategoriaDocumento");
        tipoDocumentoDAO.eliminarTipoPorCategoriaId(id);
        categoriaDocumentoDao.eliminarCategoriaDocumento(id);
        List<TipoDocumento> lista = tipoDocumentoDAO.getTipoDocumentoPorIdCategoria(id);
        for (TipoDocumento lista1 : lista) {
            plantillaServicio.eliminarPlantillaPorId(lista1.getTipo_id());
        }

    }

    public ArrayList<CategoriaDocumentoDTO> listarCategoriaDocumentos() {
        System.out.println("listarCategoriaDocumentos");
        ArrayList<CategoriaDocumentoDTO> listaDTO = new ArrayList<CategoriaDocumentoDTO>();
        listaDTO = convertirArregloCategoriaDocumentoADTO(categoriaDocumentoDao.listarCategoriaDocumentos());
        return listaDTO;

    }

    public CategoriaDocumento convertirDTOACategoriaDocumento(CategoriaDocumentoDTO dto) {
        System.out.println("convertirDTOACategoriaDocumento");
        CategoriaDocumento categoriaDocumento = new CategoriaDocumento();
        categoriaDocumento.setCategoria_id(dto.getCategoria_id());
        categoriaDocumento.setNombreCategoria(dto.getNombreCategoria());
        categoriaDocumento.setTipodocumentos(new ArrayList<TipoDocumento>());
        return categoriaDocumento;
    }

    public List<CategoriaDocumento> convertirArregloDTOACategoriaDocumento(List<CategoriaDocumentoDTO> dto) {
        System.out.println("convertirArregloDTOACategoriaDocumento");
        List<CategoriaDocumento> listCategoriaDocumento = new ArrayList<CategoriaDocumento>();
        for (CategoriaDocumentoDTO listCategoriaDocumentoDTO : dto) {
            listCategoriaDocumento.add(convertirDTOACategoriaDocumento(listCategoriaDocumentoDTO));
        }
        return listCategoriaDocumento;
    }

    public CategoriaDocumentoDTO convertirCategoriaDocumentoADTO(CategoriaDocumento categoriaDocumento) {
        System.out.println("convertirCategoriaDocumentoADTO");
        CategoriaDocumentoDTO categoriaDocumentoDTO = new CategoriaDocumentoDTO();
        categoriaDocumentoDTO.setCategoria_id(categoriaDocumento.getCategoria_id());
        categoriaDocumentoDTO.setNombreCategoria(categoriaDocumento.getNombreCategoria());

        categoriaDocumentoDTO.setTipodocumentos(new ArrayList<TipoDocumentoDTO>());

        return categoriaDocumentoDTO;
    }

    public ArrayList<CategoriaDocumentoDTO> convertirArregloCategoriaDocumentoADTO(List<CategoriaDocumento> categoriaDocumento) {
        System.out.println("convertirArregloCategoriaDocumentoADTO");
        ArrayList<CategoriaDocumentoDTO> listCategoriaDocumentoDTO = new ArrayList<CategoriaDocumentoDTO>();
        for (CategoriaDocumento listCategoriaDocumento : categoriaDocumento) {
            listCategoriaDocumentoDTO.add(convertirCategoriaDocumentoADTO(listCategoriaDocumento));
        }
        return listCategoriaDocumentoDTO;
    }

}
