package com.tic.utils;

import java.util.*;
import java.io.*;

//public abstract class TablaValores implements Serializable{
public class TablaValores implements Serializable, FileChangeListener{

    private static Hashtable tbl   = new Hashtable();
    private static String p = "/";
    private final static TablaValores tv = new TablaValores();

    public synchronized static Hashtable reLoad(String file){
        if (file.startsWith(p)){
            file = file.substring(p.length());            
        }
        Hashtable tmp = loadTable(file);
        tbl.put(file, tmp);
        return (Hashtable)tbl.get(file);
    }

    public void fileChanged(String fileName) {        
        reLoad(fileName);
    }

    private synchronized static Hashtable loadTable(String archivo) {        
        
        String path = archivo.startsWith(p) ? "" : 
                        archivo.startsWith(System.getProperty("file.separator")) ? "" : p; // Completa Path de Archivo
        String arch = path + archivo; 
        String record = null;
        String tmp = null;
        String mainkey = null;
        String seckey = null;
        String secValue = null;
        Hashtable tabla = new Hashtable();
        Hashtable tblreg = null;
        int pos = 0;
        
        System.out.println("Carga/recarga Archivo con nueva version: tablas" + arch);
        
        try {
            Reader fr = new InputStreamReader(tv.getClass().getResourceAsStream(arch));
            BufferedReader br = new BufferedReader(fr);
            record = new String();

            while ((record = br.readLine()) != null) {
                if (((!record.trim().equals("") ? 1 : 0) & (!record.trim().startsWith("#") ? 1 : 0)) != 0){
                    tblreg = new Hashtable();
                    StringTokenizer stTok = new StringTokenizer(record);
                    tmp = stTok.nextToken(";");
                    mainkey = tmp == null ? "" : tmp.trim().toUpperCase();
                    while (stTok.hasMoreTokens()) {
                        try{
                            tmp = stTok.nextToken("=;");
                            seckey = tmp == null ? "" : tmp.trim().toUpperCase();
                            tmp = stTok.nextToken(";");
                            secValue = tmp == null ? "" : normaliza(tmp.substring(1).trim());
                            tblreg.put(seckey, secValue);
                         }catch(NoSuchElementException e){
                            System.out.println("Error Archivo: " + arch + ", llave: " +  mainkey);
                            secValue = "";
                            e.printStackTrace();
                         }catch(StringIndexOutOfBoundsException e){
                            System.out.println("Error Archivo: " + arch + ", llave: " +  mainkey);
                            secValue = "";
                            e.printStackTrace();
                         }
                    }
                    tblreg.put("_Posicion", String.valueOf(pos));
                    pos++;
                    tabla.put(mainkey, tblreg);
                }
            }            
            try {
	            FileMonitor.getInstance().addFileChangeListener(tv, arch, 10000l);// M�x. 10 Segundos;
	        }catch (FileNotFoundException e) {
	            e.printStackTrace();
	        }

        } catch (IOException e) {
            System.out.println("IOException error: ");
            e.printStackTrace();
        }
        return tabla;
    }
        
        
    public static String normaliza(String linea){
        int a = 0;
	    while ((a = linea.indexOf("\\n", a)) > -1) linea = linea.substring(0, a) + '\n' + linea.substring(a + 2);
		a = 0;
	    while ((a = linea.indexOf("\\t", a)) > -1) linea = linea.substring(0, a) + '\t' + linea.substring(a + 2);
		a = 0;	           
		while ((a = linea.indexOf("\\r", a)) > -1) linea = linea.substring(0, a) + '\r' + linea.substring(a + 2);
		return linea;
    }

    public static String getValor(String file, String clave, String cod) {
        //System.out.println("file: "+ file + " clave: " + clave + " cod: " + cod);
        if (tbl.get(file) == null){
            Hashtable ht = loadTable(file);
            if (tbl.get(file) == null)
                tbl.put(file, ht);
        }
        Hashtable a = (Hashtable)tbl.get(file);
        Hashtable b = (Hashtable)a.get(clave.toUpperCase());
        String ret = null;
        if (b != null)
            ret = (String)b.get(cod.toUpperCase());
        return ret;
    }
    
    public static Hashtable getTabla(String file){
        if (tbl.get(file) == null){
            Hashtable ht = loadTable(file);
            if (tbl.get(file) == null)
                tbl.put(file, ht);
        }
        return (Hashtable)tbl.get(file);
    }

    public static int getCantTablas(){
        return tbl == null ? 0 : tbl.size();
    }
    
    public static Hashtable test(){
        return tbl;
    }
}
