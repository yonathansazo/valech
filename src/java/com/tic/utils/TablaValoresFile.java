package com.tic.utils;


import java.util.*;
import java.io.*;

// Estructura que contiene Informacion de Valores desde Archivo
public class TablaValoresFile implements Serializable {
    
    private FileReader idFile; // Descriptor del Archivo
    private Hashtable tabla; // Tabla Hashing con �ndice de Archivos con Valores
    private String path = "tablas/"; // Ubicaci�n Archivos
    private char separator=';'; //Separador de Valores    
    private String arch = null;
	/**
	 * @SBGen Constructor
	 * 
	 * @since Lunes 17 de Junio de 2002
	 * @deprecated Debe usarse la clase estatica TablaValores.
	 * Esta ulitma recarga los valores de la tablas modificacdas sin necesidad de bajar la VM. Valido para jdk 1.3.1
	 */
	public TablaValoresFile() {
	} 


    /**
     * 
     * @param archivo
     * @since Lunes 17 de Junio de 2002
     * @deprecated Debe usarse la clase estatica TablaValores.
     * Esta ulitma recarga los valores de la tablas modificacdas sin necesidad de bajar la VM. Valido para jdk 1.3.1
     */
    // Constructor principal
    // Constructor a partir del archivo
    public TablaValoresFile(String archivo) {
        this.arch = archivo;
    }
    
    /**
     * 
     * @param newpath
     * @param archivo
     * @param newsep
     * @since Lunes 17 de Junio de 2002
     * @deprecated Debe usarse la clase estatica TablaValores.
     * Esta ulitma recarga los valores de la tablas modificacdas sin necesidad de bajar la VM. Valido para jdk 1.3.1
     */
    // Constructor Multiparametros
    public TablaValoresFile(String newpath, String archivo, char newsep) {      
        this(archivo);
    }
    
    /**
     * 
     * @since Lunes 17 de Junio de 2002
     * @deprecated Debe usarse la clase estatica TablaValores.
     * Esta ulitma recarga los valores de la tablas modificacdas sin necesidad de bajar la VM. Valido para jdk 1.3.1
     */
    // Total de registros de la Tabla
    public int countRegistros() {
        if (TablaValores.getTabla(arch) == null) return 0;
        return TablaValores.getTabla(arch).size();
    }

    /**
     * 
     * @param clave
     * @since Lunes 17 de Junio de 2002
     * @deprecated Debe usarse la clase estatica TablaValores.
     * Esta ulitma recarga los valores de la tablas modificacdas sin necesidad de bajar la VM. Valido para jdk 1.3.1
     */
    // Total de Valores de un Registro dado
    public int countValoresRegistro(String clave) {
        if (TablaValores.getTabla(arch) == null) return 0;
        Hashtable valores = (Hashtable)((Hashtable)TablaValores.getTabla(arch)).get(clave.toUpperCase().trim());
        if (valores != null ) return valores.size() - 1;
        return 0;
    }

    /**
     * 
     * @param clave
     * @since Lunes 17 de Junio de 2002
     * @deprecated Debe usarse la clase estatica TablaValores.
     * Esta ulitma recarga los valores de la tablas modificacdas sin necesidad de bajar la VM. Valido para jdk 1.3.1
     */
    // Entrega la Posicion de un Registro dado
    public int getPosicion(String clave) {
        String strPos = (String)((Hashtable)(TablaValores.getTabla(arch)).get(clave.toUpperCase().trim())).get("_Posicion");
        if (strPos == null) return -1;
        return Integer.valueOf(strPos).intValue();
    }

    /**
     * 
     * @param clave
     * @param cod
     * @since Lunes 17 de Junio de 2002
     * @deprecated Debe usarse la clase estatica TablaValores.
     * Esta ulitma recarga los valores de la tablas modificacdas sin necesidad de bajar la VM. Valido para jdk 1.3.1
     */
    // Entrega el Valor de un Codigo para un Registro dado
    public String getValor(String clave, String cod) {
        return TablaValores.getValor(arch, clave, cod);
    }
   
    /**
     * 
     * @param clave
     * @since Lunes 17 de Junio de 2002
     * @deprecated Debe usarse la clase estatica TablaValores.
     * Esta ulitma recarga los valores de la tablas modificacdas sin necesidad de bajar la VM. Valido para jdk 1.3.1
     */
    // Entrega la Lista de Codigos de un Registro    
    public String[] getRegistro(String clave) {
        if (TablaValores.getTabla(arch) == null) return null;        
        Hashtable valores = (Hashtable)((Hashtable)TablaValores.getTabla(arch)).get(clave.toUpperCase().trim());
        
        if (valores == null) return null;
        
        int tot = valores.size();
        if (tot == 0) return null;
        
        String[] result = new String[tot];
        int i=0;
        for (Enumeration codigos = valores.keys(); codigos.hasMoreElements() ; i++ ) {
            result[i] = (String)codigos.nextElement(); 
        }
        
        return result;
    }

    /**
     * 
     * @param clave
     * @since Lunes 17 de Junio de 2002
     * @deprecated Debe usarse la clase estatica TablaValores.
     * Esta ulitma recarga los valores de la tablas modificacdas sin necesidad de bajar la VM. Valido para jdk 1.3.1
     */
    // Indica si existe el registro o no
    public boolean existeReg(String clave) {
        
        if ((Hashtable)TablaValores.getTabla(arch) == null) return false;        
        return ((Hashtable)TablaValores.getTabla(arch)).containsKey(clave.toUpperCase().trim());
    }

    /**
     * 
     * @since Lunes 17 de Junio de 2002
     * @deprecated Debe usarse la clase estatica TablaValores.
     * Esta ulitma recarga los valores de la tablas modificacdas sin necesidad de bajar la VM. Valido para jdk 1.3.1
     */
    // Retorna la Tabla completa
    public Hashtable getTabla() {
        return (Hashtable)TablaValores.getTabla(arch);
    }

    /**
     * 
     * @since Lunes 17 de Junio de 2002
     * @deprecated Debe usarse la clase estatica TablaValores.
     * Esta ulitma recarga los valores de la tablas modificacdas sin necesidad de bajar la VM. Valido para jdk 1.3.1
     */
    // Retorna la Lista de Claves
    public String[] getClaves() {
        if (TablaValores.getTabla(arch) == null)
            return null;
        int tot = ((Hashtable)TablaValores.getTabla(arch)).size();
        if (tot == 0) return null;

        String[] result = new String[tot];
        int i=0;
        for (Enumeration codigos = ((Hashtable)TablaValores.getTabla(arch)).keys(); codigos.hasMoreElements() ; i++ ) {
            result[i] = (String)codigos.nextElement(); 
        }
        
        return result;
    }

}