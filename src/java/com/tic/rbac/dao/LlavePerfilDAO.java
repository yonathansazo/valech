/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dao;

import com.tic.rbac.dominio.LlavesPerfil;
import com.tic.rbac.dto.RutaDTO;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class LlavePerfilDAO {

    @PersistenceContext
    private EntityManager em;

    public int crearLlavesPerfil(LlavesPerfil llavePerfil) {

        em.persist(llavePerfil);
        em.flush();
        return llavePerfil.getId_llavesperfil();
    }

    public LlavesPerfil buscarLlave(int id) {

        LlavesPerfil llavePerfil = em.find(LlavesPerfil.class, id);
        return llavePerfil;

    }

    public void actualizarPorIdPerfilLlave(int idPerfil, int idLlave) {

        em.createQuery("UPDATE LlavesPerfil l SET l.idLlave = :idLlave WHERE l.idPerfil = :idPerfil")
                .setParameter("idLlave", idLlave)
                .setParameter("idPerfil", idPerfil).executeUpdate();

    }

    public RutaDTO obtenerRuta() {

        RutaDTO ruta = new RutaDTO();
        String rutaNom = (String) em.createNativeQuery("SELECT nombre_ruta FROM ruta").getSingleResult();
        ruta.setRutaArchivo(rutaNom);
        return ruta;
    }

    public void cambiarRuta(RutaDTO ruta) {

        em.createNativeQuery("UPDATE ruta SET nombre_ruta='" + ruta.getRutaArchivo() + "' WHERE id_ruta = 1").executeUpdate();
    }

    public void eliminarRelacion(int id_llave) {

        LlavesPerfil llave = buscarLlave(id_llave);
        em.remove(llave);

    }

    public List<LlavesPerfil> listarTodos() {

        List<LlavesPerfil> lista = em.createQuery("select l from LlavesPerfil l").getResultList();

        return lista;

    }

    public void eliminarTodoPerfilUsuario(int idPerfil) {

        System.out.println("eliminarTodoPerfilUsuario");

        em.createQuery("DELETE FROM LlavesPerfil pu WHERE pu.idPerfil = :idPerfil")
                .setParameter("idPerfil", idPerfil)
                .executeUpdate();

    }

    public List<LlavesPerfil> obtenerLlavesPorPerfil(LlavesPerfil llavesPerfil) {

        System.out.println("obtieneTodoPerfilUsuario");

        List<LlavesPerfil> listado = em.createQuery("SELECT pu FROM LlavesPerfil pu WHERE pu.idPerfil = :idPerfil")
                .setParameter("idPerfil", llavesPerfil.getIdPerfil())
                .getResultList();

        return listado;

    }

    public List<LlavesPerfil> listarTodosPorPerfil(int id_perfil) {

        List<LlavesPerfil> lista = em.createQuery("select l from LlavesPerfil l where l.idPerfil = :id_perfil")
                .setParameter("id_perfil", id_perfil)
                .getResultList();

        return lista;

    }

}
