/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dao;

import com.tic.rbac.dominio.Llaves;
import com.tic.rbac.dominio.LlavesPerfil;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class LlaveDAO {

    @PersistenceContext
    private EntityManager em;

    public int crearModulo(Llaves llave) {

        em.persist(llave);
        em.flush();
        return llave.getId_llaves();

    }
    
    public Llaves buscarLlave(int id){
        
        Llaves llave = em.find(Llaves.class, id);
        return llave;
        
    }
    
    public void actualizarPorIdPerfil(int id_perfil,int id_usuario){
        
        em.createQuery("UPDATE Llaves l SET l.id_usuario = :id_usuario WHERE l.id_perfil = :id_perfil")
                .setParameter("id_usuario", id_usuario)
                .setParameter("id_perfil", id_perfil).executeUpdate();
        
    }
    
    public void eliminarRelacion(int id_llave){
        
        Llaves llave = buscarLlave(id_llave);
        em.remove(llave);
        
    }

    public List<Llaves> listarTodos() {
        
        List<Llaves> lista = em.createQuery("select l from Llaves l order by l.id_llaves").getResultList();
                
        return lista;

    }
    
    public List<Llaves> listarTodosPorId(int id_usuario) {
        
        List<Llaves> lista = em.createQuery("select l from Llaves l where l.id_usuario = :id_usuario")
                .setParameter("id_usuario", id_usuario)
                .getResultList();
                
        return lista;

    }
    
    
    

}
