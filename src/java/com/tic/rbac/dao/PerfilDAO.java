/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dao;

import com.tic.rbac.dominio.Perfil;
import com.tic.rbac.dominio.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class PerfilDAO {

    @PersistenceContext
    private EntityManager em;

    public int crearPerfil(Perfil perfil) {

        em.persist(perfil);
        em.flush();
        return perfil.getId_perfil();

    }

    public Perfil buscarPerfil(int id) {

        Perfil p = em.find(Perfil.class, id);
        return p;

    }

    public void actualizarPerfil(Perfil perfil) {

        em.merge(perfil);

    }

    public void eliminarPerfil(int id) {
        if (id != 1) {
            Perfil p = buscarPerfil(id);
            em.remove(p);
        }

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Perfil> buscarTodosLosPerfiles() {
        return em.createNamedQuery("Perfil.buscarTodos", Perfil.class)
                .getResultList();
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Perfil buscarNombrePerfil(String nombre) {
        try {
            return em.createNamedQuery("Perfil.buscarNombrePerfil", Perfil.class).setParameter("nombre_perfil", nombre)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        }

    }

}
