/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dao;

import com.tic.rbac.dominio.Plantilla;
import com.tic.rbac.dto.PlantillaDTO;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class PlantillaDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    public void crearPlantilla(Plantilla plantilla){
        
        em.persist(plantilla);
       
    }
    
    public Plantilla buscarPlantilla(int id){
        
        Plantilla p = em.find(Plantilla.class, id);
        return p;
        
    }
    
    public void actualizarPlantilla(Plantilla plantilla){
        
        em.merge(plantilla);
        
    }
    
    public void eliminarPlantilla(int id){
        
        Plantilla p = buscarPlantilla(id);
        em.remove(p);
        
    }
    
    public void eliminarPlantillaPorId(int id_subcategoria) {
        em.createNamedQuery("Plantilla.eliminarPlantillaPorIdSubCat", PlantillaDTO.class).setParameter("id_subcategoria", id_subcategoria)
                .executeUpdate();
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Plantilla> buscarTodosLasPlantillas() {
        return em.createNamedQuery("Plantilla.buscarTodos", Plantilla.class)
                .getResultList();
    }
    
}
