/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dao;

import com.tic.rbac.dominio.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class UsuarioDAO {

    @PersistenceContext
    private EntityManager em;

    public int crearUsuario(Usuario usuario) {

        em.persist(usuario);
        em.flush();
        return usuario.getId_usuario();

    }

    public Usuario buscarUsuario(int id) {

        Usuario usuario = em.find(Usuario.class, id);
        return usuario;

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Usuario obtenerPerfilPorUsuario(int id) {
        return em.createNamedQuery("Usuario.buscarPerfiles", Usuario.class)
                .setParameter("id_usuario", id)
                .getSingleResult();
    }

    public void actualizarUsuario(Usuario usuario) {

        em.merge(usuario);

    }

    public void eliminarUsuario(int id) {
        if (id != 1) {
            em.remove(buscarUsuario(id));
        }
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Usuario> buscarTodosLosUsuario() {
        return em.createNamedQuery("Usuario.buscarTodos", Usuario.class)
                .getResultList();
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Usuario buscarPorUsername(Usuario usuario) {
        try {
            return em.createNamedQuery("Usuario.buscarUsername", Usuario.class)
                    .setParameter("username", usuario.getUsername())
                    .setParameter("password", usuario.getPassword())
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        }

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Usuario buscarPorSoloUsername(Usuario usuario) {
        try {
            return em.createNamedQuery("Usuario.buscarSoloUsername", Usuario.class)
                    .setParameter("username", usuario.getUsername())
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        }

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Usuario buscarPorCorreo() {
        return em.createNamedQuery("Usuario.buscarCorreo", Usuario.class)
                .getSingleResult();
    }

}
