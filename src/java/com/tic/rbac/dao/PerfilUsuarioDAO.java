/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dao;

import com.tic.rbac.dominio.PerfilUsuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class PerfilUsuarioDAO {

    @PersistenceContext
    EntityManager em;

    public void crearPerfilUsuario(PerfilUsuario peruser) {

        em.persist(peruser);
        em.flush();

    }

    public PerfilUsuario buscarPerfilUsuario(int id) {

        PerfilUsuario pu = em.find(PerfilUsuario.class, id);
        return pu;

    }

    public void actualizarPerfilUsuario(PerfilUsuario peruser) {

        em.merge(peruser);

    }

    public void eliminarPerfilUsuario(int id) {

            em.remove(id);
        

    }

    public void eliminarTodoPerfilUsuario(int id_usuario) {

        System.out.println("eliminarTodoPerfilUsuario");

        em.createNativeQuery("DELETE FROM PerfilUsuario p WHERE p.id_usuario =" + id_usuario)
                .executeUpdate();

    }

    public List<PerfilUsuario> listarPerfilUsuario() {

        System.out.println("prueba");

        List<PerfilUsuario> lista = em.createQuery("SELECT pu FROM PerfilUsuario pu ORDER BY pu.perfilusuario_id")
                .getResultList();
        return lista;

    }

    public List<PerfilUsuario> listarPerfilUsuarioPorIdUsuario(int id) {

        System.out.println("prueba");

        List<PerfilUsuario> lista = em.createQuery("SELECT pu FROM PerfilUsuario pu WHERE pu.id_usuario = :id_usuario")
                .setParameter("id_usuario", id)
                .getResultList();
        return lista;

    }

    public void eliminarPerfilUsuarioPorIdPerfil(int id_perfil) {
        if (id_perfil != 1) {
            em.createQuery("DELETE FROM PerfilUsuario pu WHERE pu.id_perfil = :id_perfil")
                    .setParameter("id_perfil", id_perfil).executeUpdate();
        }
    }

    public void eliminarPerfilUsuarioPorIdUsuario(int id_usuario) {

        if (id_usuario != 1) {
            em.createQuery("DELETE FROM PerfilUsuario pu WHERE pu.id_usuario = :id_usuario")
                    .setParameter("id_usuario", id_usuario).executeUpdate();
        }

    }

}
