/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dominio;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Rothkegel
 */
@Entity
@Table(name = "usuario")
@NamedQueries({
    @NamedQuery(name = "Usuario.buscarTodos",
            query = "SELECT u "
            + "        FROM Usuario u "
            + "       ORDER BY u.nombre"),
    @NamedQuery(
            name = "Usuario.buscarUsername",
            query = "SELECT u "
                + "        FROM Usuario u "
            + "       WHERE u.username = :username AND u.password = :password"),
    @NamedQuery(
            name = "Usuario.buscarSoloUsername",
            query = "SELECT u "
                + "        FROM Usuario u "
            + "       WHERE u.username = :username"),
    @NamedQuery(
            name = "Usuario.buscarCorreo",
            query = "SELECT u "
            + "        FROM Usuario u "
            + "       WHERE u.correo = :correo"),
    @NamedQuery(
            name = "Usuario.buscarPerfiles",
            query = "SELECT u "
            + "        FROM Usuario u "
            + "       WHERE u.id_usuario = :id_usuario")

})
public class Usuario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private int id_usuario;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "correo")
    private String correo;

    @OneToMany(mappedBy = "usuario", cascade = CascadeType.MERGE)
    private List<PerfilUsuario> perfiles;

    /**
     *
     */
    public Usuario() {
    }

    /**
     *
     * @return
     */
    public int getId_usuario() {
        return id_usuario;
    }

    /**
     *
     * @param id_usuario
     */
    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     */
    public String getCorreo() {
        return correo;
    }

    /**
     *
     * @param correo
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public List<PerfilUsuario> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(List<PerfilUsuario> perfiles) {
        this.perfiles = perfiles;
    }

   

   
    

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id_usuario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.id_usuario, other.id_usuario)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id_usuario=" + id_usuario + ", nombre=" + nombre + ", username=" + username + ", password=" + password + ", correo=" + correo + ", perfiles=" + perfiles + '}';
    }

}
