/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dominio;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Rothkegel
 */
@Entity
@Table(name = "perfil")
@NamedQueries({
    @NamedQuery(name = "Perfil.buscarTodos",
            query = "SELECT p "
            + "        FROM Perfil p "
            + "       ORDER BY p.nombre_perfil"),
    @NamedQuery(
            name = "Perfil.buscarNombrePerfil",
            query = "SELECT p "
                + "        FROM Perfil p "
            + "       WHERE p.nombre_perfil = :nombre_perfil")

})
public class Perfil implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_perfil")
    private int id_perfil;

    @Column(name = "nombre_perfil")
    private String nombre_perfil;

    @OneToMany(mappedBy = "perfil" , cascade = CascadeType.MERGE)
    private List<PerfilUsuario> usuarios;
    
    
    @Column(name = "check_perfil")
    private boolean check;
    
    public Perfil() {
    }

    public int getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    public String getNombre_perfil() {
        return nombre_perfil;
    }

    public void setNombre_perfil(String nombre_perfil) {
        this.nombre_perfil = nombre_perfil;
    }

    public List<PerfilUsuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<PerfilUsuario> usuarios) {
        this.usuarios = usuarios;
    }


    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
    
    


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id_perfil);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Perfil other = (Perfil) obj;
        if (!Objects.equals(this.id_perfil, other.id_perfil)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Perfil{" + "id_perfil=" + id_perfil + ", nombre_perfil=" + nombre_perfil + ", usuarios=" + usuarios + ", check=" + check + '}';
    }

}
