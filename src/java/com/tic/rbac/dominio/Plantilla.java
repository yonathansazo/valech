/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dominio;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "plantilla")
@NamedQueries({
    @NamedQuery(name = "Plantilla.buscarTodos",
            query = "SELECT p "
            + "        FROM Plantilla p "
            + "       ORDER BY p.id_plantilla"),
    @NamedQuery(name = "Plantilla.eliminarPlantillaPorIdSubCat",
            query = "DELETE FROM Plantilla td WHERE "
            + " td.id_subcategoria = :id_subcategoria")
})
public class Plantilla implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_plantilla")
    private int id_plantilla;

    @Column(name = "subcategoria")
    private String subcategoria;

    @Column(name = "id_subcategoria")
    private int id_subcategoria;

    public Plantilla() {
    }

    public Plantilla(int id_plantilla, String subcategoria) {
        this.id_plantilla = id_plantilla;
        this.subcategoria = subcategoria;
    }

    public int getId_subcategoria() {
        return id_subcategoria;
    }

    public void setId_subcategoria(int id_subcategoria) {
        this.id_subcategoria = id_subcategoria;
    }

    public int getId_plantilla() {
        return id_plantilla;
    }

    public void setId_plantilla(int id_plantilla) {
        this.id_plantilla = id_plantilla;
    }

    public String getSubcategoria() {
        return subcategoria;
    }

    public void setSubcategoria(String subcategoria) {
        this.subcategoria = subcategoria;
    }

}
