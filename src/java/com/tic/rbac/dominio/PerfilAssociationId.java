/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dominio;

/**
 *
 * @author Rothkegel
 */
public class PerfilAssociationId {

    private int id_usuario;

    private int id_perfil;
    
    private int perfilusuario_id;

    public int hashCode() {
        return (int) (id_usuario + id_perfil + perfilusuario_id);
    }

    public boolean equals(Object object) {
        if (object instanceof PerfilAssociationId) {
            PerfilAssociationId otherId = (PerfilAssociationId) object;
            return (otherId.id_usuario == this.id_usuario) && (otherId.id_perfil == this.id_perfil) && (otherId.perfilusuario_id == this.perfilusuario_id);
        }
        return false;
    }

}
