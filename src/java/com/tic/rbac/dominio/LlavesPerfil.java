/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dominio;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "llavesperfil")    
public class LlavesPerfil implements Serializable {

    @Id
    @Column(name = "id_llavesperfil")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_llavesperfil;

    @Column(name = "id_llave")
    private int idLlave;
    
    @Column(name = "id_perfil")
    private int idPerfil;

    public LlavesPerfil() {
    }

    public LlavesPerfil(int id_llavesperfil, int idLlave, int idPerfil) {
        this.id_llavesperfil = id_llavesperfil;
        this.idLlave = idLlave;
        this.idPerfil = idPerfil;
    }

    public int getId_llavesperfil() {
        return id_llavesperfil;
    }

    public void setId_llavesperfil(int id_llavesperfil) {
        this.id_llavesperfil = id_llavesperfil;
    }

    public int getIdLlave() {
        return idLlave;
    }

    public void setIdLlave(int idLlave) {
        this.idLlave = idLlave;
    }

    public int getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(int idPerfil) {
        this.idPerfil = idPerfil;
    }
    
    

    

 
}
