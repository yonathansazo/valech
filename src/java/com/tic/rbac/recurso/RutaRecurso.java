/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.recurso;

import com.tic.rbac.dto.LlavesPerfilDTO;
import com.tic.rbac.dto.RutaDTO;
import com.tic.rbac.servicio.LlavePerfilServicio;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Arothkegel
 */
@Path("ruta")
public class RutaRecurso {

    @EJB
    private LlavePerfilServicio llavePerfilServicio;

    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLLavePerfil() {

        RutaDTO ruta = llavePerfilServicio.obtenerRuta();
        GenericEntity<RutaDTO> entidad;
        entidad = new GenericEntity<RutaDTO>(ruta) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }

    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addLlavesPerfil(RutaDTO ruta) {
        llavePerfilServicio.cambiarRuta(ruta);
        GenericEntity<RutaDTO> entidad;
        entidad = new GenericEntity<RutaDTO>(ruta) {
        };
        return Response
                .ok()
                .entity(ruta)
                .build();
    }
    
    
}
