/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.recurso;

import com.tic.rbac.dto.PerfilUsuarioDTO;
import com.tic.rbac.dto.UsuarioDTO;
import com.tic.rbac.servicio.PerfilUsuarioServicio;
import com.tic.rbac.servicio.UsuarioServicio;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Arothkegel
 */
@Path("perfilusuario")
public class PerfilUsuarioRecurso {

    @EJB
    private PerfilUsuarioServicio perfilUsuarioServicio;
    @EJB
    private UsuarioServicio usuarioServicio;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPerfilUsuario() {

        ArrayList<PerfilUsuarioDTO> lista = perfilUsuarioServicio.listarPerfilUsuario();
        GenericEntity<ArrayList<PerfilUsuarioDTO>> entidad;
        entidad = new GenericEntity<ArrayList<PerfilUsuarioDTO>>(lista) {
        };
        return Response
                .ok()
                .entity(entidad)
                .build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPerfilUsuario(List<PerfilUsuarioDTO> perfilUsuarioDTO) {
        perfilUsuarioServicio.agregarPerfilUsuario(perfilUsuarioDTO);
        
        ArrayList<UsuarioDTO> lista = usuarioServicio.listarUsuarios();
        GenericEntity<ArrayList<UsuarioDTO>> entidad;
        entidad = new GenericEntity<ArrayList<UsuarioDTO>>(lista) {
        };
        return Response
                .ok()
                .entity(entidad)
                .build();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{perfilusuario_id}")
    public Response updatePerfilUsuario(@PathParam("perfilusuario_id")int perfilusuario_id,  PerfilUsuarioDTO dto) {
        dto.setId_perfil(perfilusuario_id);
        perfilUsuarioServicio.actualizarPerfilUsuario(dto);
        return Response
                .ok()
                .entity(dto)
                .build();
    }
    
    @DELETE
    @Produces
    @Path("{perfilusuario_id}")
    public Response deletePerfilUsuario(@PathParam("perfilusuario_id")int perfilusuario_id) {
        perfilUsuarioServicio.eliminarPerfilUsuario(perfilusuario_id);
        return Response
                .ok()
                .build();
    }
}
