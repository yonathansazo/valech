/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.recurso;

import com.tic.rbac.dto.PlantillaDTO;
import com.tic.rbac.servicio.PlantillaServicio;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("plantilla")
public class PlantillaRecurso {
    
    @EJB
    private PlantillaServicio plantillaServicio;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlantilla() {

        ArrayList<PlantillaDTO> lista = plantillaServicio.listarPlantillas();
        GenericEntity<ArrayList<PlantillaDTO>> entidad;
        entidad = new GenericEntity<ArrayList<PlantillaDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPersonas(PlantillaDTO dto) {
        plantillaServicio.crearPlantilla(dto);
        return Response
                .ok()
                .entity(dto)
                .build();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id_plantilla}")
    public Response updatePersonas(@PathParam("id_plantilla")int id_plantilla,  PlantillaDTO dto) {
        dto.setId_plantilla(id_plantilla);
        plantillaServicio.actualizarPlantilla(dto);
        return Response
                .ok()
                .entity(dto)
                .build();
    }
    
    @DELETE
    @Produces
    @Path("{id_plantilla}")
    public Response deletePersona(@PathParam("id_plantilla")int id_plantilla) {
        plantillaServicio.eliminarPlantilla(id_plantilla);
        return Response
                .ok()
                .build();
    }
    
}
