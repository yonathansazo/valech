/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.recurso;

import com.tic.rbac.dto.PerfilDTO;
import com.tic.rbac.dto.PerfilUsuarioDTO;
import com.tic.rbac.servicio.PerfilServicio;
import com.tic.rbac.servicio.PerfilUsuarioServicio;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Arothkegel
 */
@Path("perfil")
public class PerfilRecurso {

    @EJB
    private PerfilServicio perfilServicio;

    @EJB
    private PerfilUsuarioServicio perfilUsuarioServicio;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPerfil() {

        ArrayList<PerfilDTO> lista = perfilServicio.listarPerfiles();
        GenericEntity<ArrayList<PerfilDTO>> entidad;
        entidad = new GenericEntity<ArrayList<PerfilDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id_usuario}")
    public Response getPerfilpoUsuario(@PathParam("id_usuario") int id_usuario) {

        List<PerfilUsuarioDTO> lista = perfilUsuarioServicio.listarPerfilesPorUsuario(id_usuario);
        GenericEntity<List<PerfilUsuarioDTO>> entidad;
        entidad = new GenericEntity<List<PerfilUsuarioDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPerfil(List<PerfilDTO> perfilesDTO) {
        perfilServicio.eliminarPerfilesSeleccionados(perfilesDTO);

        ArrayList<PerfilDTO> lista = perfilServicio.listarPerfiles();
        GenericEntity<ArrayList<PerfilDTO>> entidad;
        entidad = new GenericEntity<ArrayList<PerfilDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id_perfil}")
    public Response updatePerfil(@PathParam("id_perfil") int id_perfil, PerfilDTO dto) {
        dto.setId_perfil(id_perfil);
        perfilServicio.actualizarPerfil(dto);
        return Response
                .ok()
                .entity(dto)
                .build();
    }

    @DELETE
    @Produces
    @Path("{id_perfil}")
    public Response deletePerfil(@PathParam("id_perfil") int id_perfil) {
        perfilServicio.eliminarPerfil(id_perfil);
        return Response
                .ok()
                .build();
    }
}
