/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.recurso;

import com.tic.rbac.dto.LlavesDTO;
import com.tic.rbac.dto.LlavesPerfilDTO;
import com.tic.rbac.dto.PerfilDTO;
import com.tic.rbac.servicio.LlavePerfilServicio;
import com.tic.rbac.servicio.LlaveServicio;
import com.tic.rbac.servicio.PerfilServicio;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Arothkegel
 */
@Path("llave")
public class LlaveRecurso {

    @EJB
    private LlaveServicio llaveServicio;
    
    @EJB
    private LlavePerfilServicio llavePerfilServicio;
    
    @EJB
    private PerfilServicio perfilServicio;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPerfil() {

        List<LlavesDTO> lista = llaveServicio.listarTodos();
        GenericEntity<List<LlavesDTO>> entidad;
        entidad = new GenericEntity<List<LlavesDTO>>(lista) {
        };
        return Response
                .ok()
                .entity(entidad)
                .build();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id_perfil}")
    public Response getLLavePerfil(@PathParam("id_perfil")int id_perfil) {

        List<LlavesPerfilDTO> lista = llavePerfilServicio.listarTodosPorPerfil(id_perfil);
        GenericEntity<List<LlavesPerfilDTO>> entidad;
        entidad = new GenericEntity<List<LlavesPerfilDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }

    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addLlavesPerfil(List<LlavesPerfilDTO> llavesPerfilDTO) {
        llavePerfilServicio.agregarPerfilUsuario(llavesPerfilDTO);
        
        ArrayList<PerfilDTO> lista = perfilServicio.listarPerfiles();
        GenericEntity<ArrayList<PerfilDTO>> entidad;
        entidad = new GenericEntity<ArrayList<PerfilDTO>>(lista) {
        };
        
        
        
        return Response
                .ok()
                .entity(entidad)
                .build();
    }
    
    
}
