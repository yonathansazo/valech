/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.recurso;

import com.tic.rbac.dto.LlavesDTO;
import com.tic.rbac.dto.PlantillaDTO;
import com.tic.rbac.dto.UsuarioDTO;
import com.tic.rbac.servicio.UsuarioServicio;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Arothkegel
 */
@Path("usuario")
public class UsuarioRecurso {

    @EJB
    private UsuarioServicio usuarioServicio;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPersonas() {

        ArrayList<UsuarioDTO> lista = usuarioServicio.listarUsuarios();
        GenericEntity<ArrayList<UsuarioDTO>> entidad;
        entidad = new GenericEntity<ArrayList<UsuarioDTO>>(lista) {
        };

        return Response
                .ok()
                .entity(entidad)
                .build();

    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id_usuario}")
    public Response getPersonas(@PathParam("id_usuario") int id_usuario) {

        UsuarioDTO lista = usuarioServicio.listarUsuariosPorId(id_usuario);

        return Response
                .ok()
                .entity(lista)
                .build();

    }
    
    

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id_usuario}")
    public Response addPersonasCredenciales(@PathParam("id_usuario") int id_usuario, UsuarioDTO dto) {
        List<LlavesDTO> llaves = usuarioServicio.validarCredenciales(dto);
        GenericEntity<List<LlavesDTO>> entidad;
        if(llaves.size()>0){
            entidad = new GenericEntity<List<LlavesDTO>>(llaves) {};
        }else{
            entidad = null;
        }
        return Response
                .ok()
                .entity(entidad)
                .build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPersonas(List<UsuarioDTO> usuariosDTO) {
        usuarioServicio.eliminarUsuariosSeleccionados(usuariosDTO);
        
        ArrayList<UsuarioDTO> lista = usuarioServicio.listarUsuarios();
        GenericEntity<ArrayList<UsuarioDTO>> entidad;
        entidad = new GenericEntity<ArrayList<UsuarioDTO>>(lista) {
        };
        return Response
                .ok()
                .entity(entidad)
                .build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id_usuario}")
    public Response updatePersonas(@PathParam("id_usuario") int id_usuario, UsuarioDTO dto) {
        dto.setId_usuario(id_usuario);
        usuarioServicio.actualizarUsuario(dto);
        return Response
                .ok()
                .entity(dto)
                .build();
    }

    @DELETE
    @Produces
    @Path("{id_usuario}")
    public Response deletePersona(@PathParam("id_usuario") int id_usuario) {
        usuarioServicio.eliminarUsuario(id_usuario);
        return Response
                .ok()
                .build();
    }
}
