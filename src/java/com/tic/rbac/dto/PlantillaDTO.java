/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dto;

public class PlantillaDTO {
    
    private int id_plantilla;
    
    private String nombreTipoDocumento;
    
    private int tipo_id;

    public PlantillaDTO() {
    }

    public PlantillaDTO(int id_plantilla, String nombreTipoDocumento, int tipo_id) {
        this.id_plantilla = id_plantilla;
        this.nombreTipoDocumento = nombreTipoDocumento;
        this.tipo_id = tipo_id;
    }

    public int getId_plantilla() {
        return id_plantilla;
    }

    public void setId_plantilla(int id_plantilla) {
        this.id_plantilla = id_plantilla;
    }

    public String getNombreTipoDocumento() {
        return nombreTipoDocumento;
    }

    public void setNombreTipoDocumento(String nombreTipoDocumento) {
        this.nombreTipoDocumento = nombreTipoDocumento;
    }

    public int getTipo_id() {
        return tipo_id;
    }

    public void setTipo_id(int tipo_id) {
        this.tipo_id = tipo_id;
    }

    

   
}
