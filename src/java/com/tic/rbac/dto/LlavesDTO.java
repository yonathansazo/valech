/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dto;

import com.tic.rbac.dominio.*;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

public class LlavesDTO implements Serializable {

    private int id_llaves;

    private String nombreLlave;
    
    private String descripcionLlave;
    
    

    public LlavesDTO() {
        
        
    }

    public LlavesDTO(int id_llaves, String nombreLlave, String descripcionLlave) {
        this.id_llaves = id_llaves;
        this.nombreLlave = nombreLlave;
        this.descripcionLlave = descripcionLlave;
    }

    public int getId_llaves() {
        return id_llaves;
    }

    public void setId_llaves(int id_llaves) {
        this.id_llaves = id_llaves;
    }

    public String getNombreLlave() {
        return nombreLlave;
    }

    public void setNombreLlave(String nombreLlave) {
        this.nombreLlave = nombreLlave;
    }

    public String getDescripcionLlave() {
        return descripcionLlave;
    }

    public void setDescripcionLlave(String descripcionLlave) {
        this.descripcionLlave = descripcionLlave;
    }
    
    

 
}
