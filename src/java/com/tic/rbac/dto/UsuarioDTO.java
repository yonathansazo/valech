/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dto;

import com.tic.rbac.dominio.PerfilUsuario;
import java.util.List;


public class UsuarioDTO {
    
    private int id_usuario;
    private String nombre;
    private String username;
    private String password;
    private String correo;
    private List<PerfilDTO> perfil_usuario;

    public UsuarioDTO() {
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public List<PerfilDTO> getPerfil_usuario() {
        return perfil_usuario;
    }

    public void setPerfil_usuario(List<PerfilDTO> perfil_usuario) {
        this.perfil_usuario = perfil_usuario;
    }

    
}
