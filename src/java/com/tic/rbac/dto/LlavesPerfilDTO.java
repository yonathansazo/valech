/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dto;

import com.tic.rbac.dominio.*;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

public class LlavesPerfilDTO implements Serializable {

    private int id_llavesperfil;
    private int id_llaves;
    private int idPerfil;
    private String nombre_perfil;

    public LlavesPerfilDTO() {
    }

    public LlavesPerfilDTO(int id_llavesperfil, int id_llaves, int idPerfil, String nombre_perfil) {
        this.id_llavesperfil = id_llavesperfil;
        this.id_llaves = id_llaves;
        this.idPerfil = idPerfil;
        this.nombre_perfil = nombre_perfil;
    }

    public int getId_llavesperfil() {
        return id_llavesperfil;
    }

    public void setId_llavesperfil(int id_llavesperfil) {
        this.id_llavesperfil = id_llavesperfil;
    }

    public int getId_llaves() {
        return id_llaves;
    }

    public void setId_llaves(int id_llaves) {
        this.id_llaves = id_llaves;
    }

    public int getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(int idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getNombre_perfil() {
        return nombre_perfil;
    }

    public void setNombre_perfil(String nombre_perfil) {
        this.nombre_perfil = nombre_perfil;
    }

}
