/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.dto;

import com.tic.rbac.dominio.Perfil;
import com.tic.rbac.dominio.Usuario;


public class PerfilUsuarioDTO {
    
    private int id_usuario;
    private int id_perfil;
    private UsuarioDTO usuarioDTO;
    private PerfilDTO perfilDTO;
    private Usuario usuario;

    public PerfilUsuarioDTO() {
    }

    public PerfilUsuarioDTO(int id_usuario, int id_perfil, UsuarioDTO usuarioDTO, PerfilDTO perfilDTO, Usuario usuario) {
        this.id_usuario = id_usuario;
        this.id_perfil = id_perfil;
        this.usuarioDTO = usuarioDTO;
        this.perfilDTO = perfilDTO;
        this.usuario = usuario;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    public UsuarioDTO getUsuarioDTO() {
        return usuarioDTO;
    }

    public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
        this.usuarioDTO = usuarioDTO;
    }

    public PerfilDTO getPerfilDTO() {
        return perfilDTO;
    }

    public void setPerfilDTO(PerfilDTO perfilDTO) {
        this.perfilDTO = perfilDTO;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    
    
}
