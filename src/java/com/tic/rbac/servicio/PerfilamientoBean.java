/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.servicio;

import com.tic.rbac.dto.PerfilDTO;
import com.tic.rbac.dto.PerfilUsuarioDTO;
import com.tic.rbac.dto.UsuarioDTO;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class PerfilamientoBean {

    @EJB
    private UsuarioServicio usuarioServicio;

    @EJB
    private PerfilServicio perfilServicio;

    @EJB
    private PerfilUsuarioServicio perfilUsuarioServicio;

//    public ArrayList<TodoPerfilesUsuarioDTO> listaCompleta() {
//
//        ArrayList<PerfilUsuarioDTO> listaPerfilados = perfilUsuarioServicio.listarPerfilUsuario();
//        ArrayList<UsuarioDTO> usuarios = usuarioServicio.listarUsuarios();
//        ArrayList<TodoPerfilesUsuarioDTO> todoPerfilesUsuarios = new ArrayList<TodoPerfilesUsuarioDTO>();
//
//        for (UsuarioDTO usuario : usuarios) {
//            ArrayList<PerfilDTO> perfiles = perfilServicio.listarPerfiles();
//            for (PerfilUsuarioDTO listaPerfilado : listaPerfilados) {
//                if (usuario.getId_usuario() == listaPerfilado.getUsuario().getId_usuario()) {
//                    for (PerfilDTO perfile : perfiles) {
//                        if (perfile.getId_perfil() == listaPerfilado.getPerfil().getId_perfil()) {
//                            perfile.setCheck(true);
//                        }
//                    }
//                }
//            }
//            TodoPerfilesUsuarioDTO todoPerUsuDTO = new TodoPerfilesUsuarioDTO();
//            todoPerUsuDTO.setUsuario(usuario);
//            todoPerUsuDTO.setPerfiles(perfiles);
//            todoPerfilesUsuarios.add(todoPerUsuDTO);
//        }
//
//        return todoPerfilesUsuarios;
//    }
    
    public String checkear(boolean check){
        
        if (check == true) {
            return "selected";
        }else{
            return "";
        }
        
    }

}
