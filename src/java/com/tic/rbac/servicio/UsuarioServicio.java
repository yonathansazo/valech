/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.servicio;

import com.tic.rbac.dao.PerfilUsuarioDAO;
import com.tic.rbac.dao.UsuarioDAO;
import com.tic.rbac.dominio.PerfilUsuario;
import com.tic.rbac.dominio.Usuario;
import com.tic.rbac.dto.LlavesDTO;
import com.tic.rbac.dto.LlavesPerfilDTO;
import com.tic.rbac.dto.PerfilDTO;
import com.tic.rbac.dto.UsuarioDTO;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;

@Stateless
public class UsuarioServicio {

    @EJB
    private UsuarioDAO usuarioDao;
    @EJB
    private PerfilServicio perfilServicio;

    @EJB
    private LlavePerfilServicio llavePerfilServicio;

    @EJB
    private LlaveServicio llaveServicio;
    
    @EJB
    private PerfilUsuarioDAO perfilUsuarioDao;

    public int crearUsuario(UsuarioDTO dto) {

        Usuario user = new Usuario();
        user.setNombre(dto.getNombre());
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setCorreo(dto.getCorreo());
        return usuarioDao.crearUsuario(user);
    }

    public UsuarioDTO buscarUsuario(int id) {

        Usuario user = usuarioDao.buscarUsuario(id);
        UsuarioDTO userDTO = new UsuarioDTO();
        userDTO.setId_usuario(user.getId_usuario());
        userDTO.setNombre(user.getNombre());
        userDTO.setUsername(user.getUsername());
        userDTO.setPassword(user.getPassword());
        userDTO.setCorreo(user.getCorreo());
        return userDTO;

    }

    public void actualizarUsuario(UsuarioDTO dto) {

        Usuario usuario = new Usuario();
        usuario.setId_usuario(dto.getId_usuario());
        usuario.setNombre(dto.getNombre());
        usuario.setUsername(dto.getUsername());
        usuario.setPassword(dto.getPassword());
        usuario.setCorreo(dto.getCorreo());
        usuarioDao.actualizarUsuario(usuario);

    }

    public void eliminarUsuario(int id) {
        usuarioDao.eliminarUsuario(id);
    }

    public ArrayList<UsuarioDTO> listarUsuarios() {
        ArrayList<UsuarioDTO> listaDTO = new ArrayList<UsuarioDTO>();
        listaDTO = convertirArregloUsuarioADTO(usuarioDao.buscarTodosLosUsuario());
        return listaDTO;

    }

    public List<LlavesDTO> validarCredenciales(UsuarioDTO usuarioDTO) {
        List<LlavesDTO> listaDTO = new ArrayList<LlavesDTO>();
        List<String> llavesPerfiles = new ArrayList<>();
        Usuario usu;
        try {
            usu = usuarioDao.buscarPorUsername(convertirDTOAUsuario(usuarioDTO));
        } catch (EJBException e) {
            usu = null;
        }catch (Exception e) {
            usu = null;
        }

        if (usu != null) {
            List<PerfilDTO> per = perfilServicio.listarPerfilesPorUsuario(usu.getId_usuario());
            for (PerfilDTO per1 : per) {
                List<LlavesPerfilDTO> llavesPer = llavePerfilServicio.listarTodosPorPerfil(per1.getId_perfil());
                for (LlavesPerfilDTO llavesPer1 : llavesPer) {
                    llavesPerfiles.add(llavesPer1.getId_llaves() + "");
                }
            }
            Set<String> hs = new HashSet<>();
            hs.addAll(llavesPerfiles);
            llavesPerfiles.clear();
            llavesPerfiles.addAll(hs);

            for (String id : llavesPerfiles) {
                listaDTO.add(llaveServicio.obtenerPorId(Integer.parseInt(id)));
            }
        }
        return listaDTO;

    }

    public Usuario convertirDTOAUsuario(UsuarioDTO dto) {

        Usuario usuario = new Usuario();
        usuario.setId_usuario(dto.getId_usuario());
        usuario.setNombre(dto.getNombre());
        usuario.setUsername(dto.getUsername());
        usuario.setPassword(dto.getPassword());
        usuario.setCorreo(dto.getCorreo());

        return usuario;
    }

    public List<Usuario> convertirArregloDTOAUsuario(List<UsuarioDTO> dto) {

        List<Usuario> listUsuario = new ArrayList<Usuario>();
        for (UsuarioDTO listUsuarioDTO : dto) {
            listUsuario.add(convertirDTOAUsuario(listUsuarioDTO));
        }
        return listUsuario;
    }

    public UsuarioDTO convertirUsuarioADTO(Usuario usuario) {

        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuarioDTO.setId_usuario(usuario.getId_usuario());
        usuarioDTO.setNombre(usuario.getNombre());
        usuarioDTO.setUsername(usuario.getUsername());
        usuarioDTO.setPassword(usuario.getPassword());
        usuarioDTO.setCorreo(usuario.getCorreo());
        ArrayList<PerfilDTO> lstPerfil = new ArrayList<PerfilDTO>();
        for (PerfilUsuario perfilUsuario : perfilUsuarioDao.listarPerfilUsuarioPorIdUsuario(usuario.getId_usuario())) {
            lstPerfil.add(perfilServicio.buscarPerfil(perfilUsuario.getId_perfil()));
        }
        usuarioDTO.setPerfil_usuario(lstPerfil);
        return usuarioDTO;
    }

    public ArrayList<UsuarioDTO> convertirArregloUsuarioADTO(List<Usuario> usuario) {

        ArrayList<UsuarioDTO> listUsuarioDTO = new ArrayList<UsuarioDTO>();
        for (Usuario listUsuario : usuario) {
            listUsuarioDTO.add(convertirUsuarioADTO(listUsuario));
        }
        return listUsuarioDTO;
    }
    
    public void eliminarUsuariosSeleccionados(List<UsuarioDTO> usuariosDTO){
        
        for (int i = 0; i < usuariosDTO.size(); i++) {
            perfilUsuarioDao.eliminarPerfilUsuarioPorIdUsuario(usuariosDTO.get(i).getId_usuario());
        }
        
        for (int i = 0; i < usuariosDTO.size(); i++) {
            perfilUsuarioDao.eliminarTodoPerfilUsuario(usuariosDTO.get(i).getId_usuario());
        }
        
        for (int i = 0; i < usuariosDTO.size(); i++) {
            usuarioDao.eliminarUsuario(usuariosDTO.get(i).getId_usuario());
        }
        
        
        
    }
    
    public UsuarioDTO listarUsuariosPorId(int id_usuario) {
        
        UsuarioDTO listaDTO = new UsuarioDTO();
        listaDTO = convertirUsuarioADTO(usuarioDao.buscarUsuario(id_usuario));
        return listaDTO;

    }

}
