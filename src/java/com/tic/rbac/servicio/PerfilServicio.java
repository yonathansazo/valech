/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.servicio;

import com.tic.rbac.dao.LlavePerfilDAO;
import com.tic.rbac.dao.PerfilDAO;
import com.tic.rbac.dao.PerfilUsuarioDAO;
import com.tic.rbac.dao.UsuarioDAO;
import com.tic.rbac.dominio.Perfil;
import com.tic.rbac.dominio.PerfilUsuario;
import com.tic.rbac.dominio.Usuario;
import com.tic.rbac.dto.PerfilDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class PerfilServicio {

    @EJB
    private PerfilDAO perfilDao;
    @EJB
    private UsuarioServicio usuarioServicio;
    @EJB
    private PerfilUsuarioServicio perfilUsuarioServicio;
    @EJB
    private UsuarioDAO usuarioDAO;
    @EJB
    private PerfilUsuarioDAO perfilUsuarioDao;
    @EJB
    private LlavePerfilDAO llavePerfilDAO;

    public int crearPerfil(PerfilDTO dto) {

        Perfil perfil = new Perfil();
        perfil.setNombre_perfil(dto.getNombre_perfil());
        return perfilDao.crearPerfil(perfil);

    }

    public PerfilDTO buscarPerfil(int id) {

        Perfil perfil = perfilDao.buscarPerfil(id);
        PerfilDTO dto = new PerfilDTO();
        dto.setId_perfil(perfil.getId_perfil());
        dto.setNombre_perfil(perfil.getNombre_perfil());
        return dto;

    }

    public List<PerfilDTO> listarPerfilesPorUsuario(int id_usuario) {
        
        List<PerfilDTO> listaDto = new ArrayList<>();
        for (PerfilUsuario perfilUsuario : usuarioDAO.obtenerPerfilPorUsuario(id_usuario).getPerfiles()) {
            listaDto.add(convertirPerfilADTO(perfilUsuario.getPerfil()));
            
        }
        
        return listaDto;
    }

    public void actualizarPerfil(PerfilDTO dto) {

        Perfil perfil = new Perfil();
        perfil.setId_perfil(dto.getId_perfil());
        perfil.setNombre_perfil(dto.getNombre_perfil());
        perfilDao.actualizarPerfil(perfil);

    }

    public void eliminarPerfil(int id) {

        perfilDao.eliminarPerfil(id);

    }

    public ArrayList<PerfilDTO> listarPerfiles() {

        ArrayList<PerfilDTO> listaDto = new ArrayList<>();
        List<Perfil> lista = perfilDao.buscarTodosLosPerfiles();
        for (Perfil per : lista) {

            PerfilDTO salida = new PerfilDTO();
            salida.setId_perfil(per.getId_perfil());
            salida.setNombre_perfil(per.getNombre_perfil());
            listaDto.add(salida);
        }
        return listaDto;
    }

    public Perfil convertirDTOAPerfil(PerfilDTO dto) {

        Perfil perfil = new Perfil();
        perfil.setId_perfil(dto.getId_perfil());
        perfil.setNombre_perfil(dto.getNombre_perfil());
        return perfil;
    }

    public List<Perfil> convertirArregloDTOAPerfilPerfil(List<PerfilDTO> dto) {

        List<Perfil> listPerfil = new ArrayList<Perfil>();
        for (PerfilDTO listPerfilDTO : dto) {
            listPerfil.add(convertirDTOAPerfil(listPerfilDTO));
        }
        return listPerfil;
    }

    public PerfilDTO convertirPerfilADTO(Perfil perfil) {

        PerfilDTO dto = new PerfilDTO();
        dto.setId_perfil(perfil.getId_perfil());
        dto.setNombre_perfil(perfil.getNombre_perfil());
        return dto;
    }

    public List<PerfilDTO> convertirArregloPerfilADTO(List<Perfil> perfil) {

        List<PerfilDTO> listPerfilDTO = new ArrayList<PerfilDTO>();
        for (Perfil listPerfil : perfil) {
            listPerfilDTO.add(convertirPerfilADTO(listPerfil));
        }
        return listPerfilDTO;
    }
    
    public void eliminarPerfilesSeleccionados(List<PerfilDTO> perfilesDTO){
        
        
        for (int i = 0; i < perfilesDTO.size(); i++) {
            perfilUsuarioDao.eliminarPerfilUsuarioPorIdPerfil(perfilesDTO.get(i).getId_perfil());
        }
        
        for (int i = 0; i < perfilesDTO.size(); i++) {
            llavePerfilDAO.eliminarTodoPerfilUsuario(perfilesDTO.get(i).getId_perfil());
        }
        
        for (int i = 0; i < perfilesDTO.size(); i++) {
            perfilDao.eliminarPerfil(perfilesDTO.get(i).getId_perfil());
        }
        
        
    }
}
