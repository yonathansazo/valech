/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.servicio;

import com.tic.rbac.dao.LlaveDAO;
import com.tic.rbac.dominio.Llaves;
import com.tic.rbac.dto.LlavesDTO;
import com.tic.rbac.dto.LlavesPerfilDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class LlaveServicio {
    
    @EJB
    private LlaveDAO llaveDao;


    public List<LlavesDTO> listarTodos() {

        List<LlavesDTO> listaDto = new ArrayList<>();
        listaDto = convertirArregloLlaveADTO(llaveDao.listarTodos());
        
        return listaDto;
    }
    
    public LlavesDTO obtenerPorId(int id_llave) {
        
        return convertirLlaveADTO(llaveDao.buscarLlave(id_llave));
    }
    

    public Llaves convertirDTOALlave(LlavesDTO dto) {

        Llaves llaves = new Llaves();
        llaves.setId_llaves(dto.getId_llaves());
        llaves.setNombreLlave(dto.getNombreLlave());
        llaves.setDescripcionLlave(dto.getDescripcionLlave());
        return llaves;
    }

    public List<Llaves> convertirArregloDTOALlave(List<LlavesDTO> dto) {

        List<Llaves> listLlaves = new ArrayList<Llaves>();
        for (LlavesDTO listLlavesDTO : dto) {
            listLlaves.add(convertirDTOALlave(listLlavesDTO));
        }
        return listLlaves;
    }

    public LlavesDTO convertirLlaveADTO(Llaves llaves) {

        LlavesDTO llavesDto = new LlavesDTO();
        llavesDto.setId_llaves(llaves.getId_llaves());
        llavesDto.setNombreLlave(llaves.getNombreLlave());
        llavesDto.setDescripcionLlave(llaves.getDescripcionLlave());
        return llavesDto;
    }

    public List<LlavesDTO> convertirArregloLlaveADTO(List<Llaves> llaves) {

        List<LlavesDTO> listLlavesDTO = new ArrayList<LlavesDTO>();
        for (Llaves listLlaves : llaves) {
            listLlavesDTO.add(convertirLlaveADTO(listLlaves));
        }
        return listLlavesDTO;
    }
}
