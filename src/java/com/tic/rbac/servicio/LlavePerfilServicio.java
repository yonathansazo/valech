/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.servicio;

import com.tic.rbac.dao.LlavePerfilDAO;
import com.tic.rbac.dao.PerfilDAO;
import com.tic.rbac.dominio.LlavesPerfil;
import com.tic.rbac.dominio.Perfil;
import com.tic.rbac.dto.LlavesPerfilDTO;
import com.tic.rbac.dto.RutaDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class LlavePerfilServicio {

    @EJB
    private LlavePerfilDAO llavePerfilDAO;

    @EJB
    private PerfilDAO perfilDao;

    public void agregarPerfilUsuario(List<LlavesPerfilDTO> llavesPerfilDTO) {

        boolean existe = true;

        if (llavesPerfilDTO.get(0).getIdPerfil() != 0) {

            LlavesPerfil llavesPerfil = new LlavesPerfil();
            Perfil perfil = perfilDao.buscarPerfil(llavesPerfilDTO.get(0).getIdPerfil());
            perfil.setNombre_perfil(llavesPerfilDTO.get(0).getNombre_perfil());
            perfilDao.actualizarPerfil(perfil);

            llavePerfilDAO.eliminarTodoPerfilUsuario(llavesPerfilDTO.get(0).getIdPerfil());

            for (LlavesPerfilDTO LlavesPerfilDTO : llavesPerfilDTO) {
                llavePerfilDAO.crearLlavesPerfil(convertirDTOALlavesPerfil(LlavesPerfilDTO));
            }

        } else {
            LlavesPerfil llavesPerfil = new LlavesPerfil();
            Perfil perfilTest = new Perfil();
            
            Perfil perfil = new Perfil();
            int id_perfil = 0;
            
            perfilTest = perfilDao.buscarNombrePerfil(llavesPerfilDTO.get(0).getNombre_perfil());
            
            if(perfilTest == null){
                perfil.setNombre_perfil(llavesPerfilDTO.get(0).getNombre_perfil());
                id_perfil = perfilDao.crearPerfil(perfil);
            }else{
                perfilDao.actualizarPerfil(perfil);
                id_perfil = perfilTest.getId_perfil();
            }
            
            llavePerfilDAO.eliminarTodoPerfilUsuario(id_perfil);

            for (LlavesPerfilDTO LlavesPerfilDTO : llavesPerfilDTO) {
                LlavesPerfilDTO.setIdPerfil(id_perfil);
            }

            for (LlavesPerfilDTO LlavesPerfilDTO : llavesPerfilDTO) {
                llavePerfilDAO.crearLlavesPerfil(convertirDTOALlavesPerfil(LlavesPerfilDTO));
            }
        }

    }

    public RutaDTO obtenerRuta() {
        RutaDTO ruta = llavePerfilDAO.obtenerRuta();
        return ruta;
    }

    public void cambiarRuta(RutaDTO ruta) {
        llavePerfilDAO.cambiarRuta(ruta);

    }

    public List<LlavesPerfilDTO> listarTodosPorPerfil(int id_perfil) {

        List<LlavesPerfilDTO> listaDto = new ArrayList<>();
        listaDto = convertirArregloLlavesPerfilADTO(llavePerfilDAO.listarTodosPorPerfil(id_perfil));

        return listaDto;
    }

    public List<LlavesPerfilDTO> listarTodos() {

        List<LlavesPerfilDTO> listaDto = new ArrayList<>();
        listaDto = convertirArregloLlavesPerfilADTO(llavePerfilDAO.listarTodos());

        return listaDto;
    }

    public LlavesPerfil convertirDTOALlavesPerfil(LlavesPerfilDTO dto) {

        LlavesPerfil llaves = new LlavesPerfil();
        llaves.setIdLlave(dto.getId_llaves());
        llaves.setIdPerfil(dto.getIdPerfil());
        llaves.setId_llavesperfil(dto.getId_llavesperfil());
        return llaves;
    }

    public List<LlavesPerfil> convertirArregloDTOALlavesPerfil(List<LlavesPerfilDTO> dto) {

        List<LlavesPerfil> listLlaves = new ArrayList<LlavesPerfil>();
        for (LlavesPerfilDTO listLlavesDTO : dto) {
            listLlaves.add(convertirDTOALlavesPerfil(listLlavesDTO));
        }
        return listLlaves;
    }

    public LlavesPerfilDTO convertirLlavesPerfilADTO(LlavesPerfil llaves) {

        LlavesPerfilDTO llavesDto = new LlavesPerfilDTO();
        llavesDto.setId_llaves(llaves.getIdLlave());
        llavesDto.setIdPerfil(llaves.getIdPerfil());
        llavesDto.setId_llavesperfil(llaves.getId_llavesperfil());
        Perfil perfil = perfilDao.buscarPerfil(llaves.getIdPerfil());
        llavesDto.setNombre_perfil(perfil.getNombre_perfil());
        return llavesDto;
    }

    public List<LlavesPerfilDTO> convertirArregloLlavesPerfilADTO(List<LlavesPerfil> llaves) {

        List<LlavesPerfilDTO> listLlavesPerfilDTO = new ArrayList<LlavesPerfilDTO>();
        for (LlavesPerfil listLlaves : llaves) {
            listLlavesPerfilDTO.add(convertirLlavesPerfilADTO(listLlaves));
        }
        return listLlavesPerfilDTO;
    }
}
