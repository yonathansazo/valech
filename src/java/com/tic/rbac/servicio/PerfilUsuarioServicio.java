package com.tic.rbac.servicio;

import com.tic.rbac.dao.PerfilDAO;
import com.tic.rbac.dao.PerfilUsuarioDAO;
import com.tic.rbac.dao.UsuarioDAO;
import com.tic.rbac.dominio.PerfilUsuario;
import com.tic.rbac.dominio.Usuario;
import com.tic.rbac.dto.PerfilUsuarioDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class PerfilUsuarioServicio {

    @EJB
    private PerfilUsuarioDAO perfilUsuarioDao;
    @EJB
    private PerfilDAO perfilDAO;
    @EJB
    private UsuarioDAO usuarioDao;
    @EJB
    private PerfilServicio perfilServicio;
    @EJB
    private UsuarioServicio usuarioServicio;

    public void agregarPerfilUsuario(List<PerfilUsuarioDTO> perfilUsuarioDTOList) {

        if (perfilUsuarioDTOList.get(0).getId_usuario() != 0) {

            PerfilUsuario perfilUsuario = new PerfilUsuario();
            perfilUsuarioDao.eliminarTodoPerfilUsuario(perfilUsuarioDTOList.get(0).getId_usuario());

            Usuario usuario = usuarioDao.buscarUsuario(perfilUsuarioDTOList.get(0).getId_usuario());
            usuario.setCorreo(perfilUsuarioDTOList.get(0).getUsuario().getCorreo());
            usuario.setNombre(perfilUsuarioDTOList.get(0).getUsuario().getNombre());
            usuario.setPassword(perfilUsuarioDTOList.get(0).getUsuario().getPassword());
            usuario.setUsername(perfilUsuarioDTOList.get(0).getUsuario().getUsername());
            
            
            
            usuarioDao.actualizarUsuario(usuario);

            for (PerfilUsuarioDTO perfilUsuarioDTO : perfilUsuarioDTOList) {

                if (perfilUsuarioDTO.getId_perfil() != 0 && perfilUsuarioDTO.getId_usuario() != 0) {
                    perfilUsuarioDTO.setPerfilDTO(perfilServicio.buscarPerfil(perfilUsuarioDTO.getId_perfil()));
                    perfilUsuarioDTO.setUsuarioDTO(usuarioServicio.buscarUsuario(perfilUsuarioDTO.getId_usuario()));
                    perfilUsuario = convertirDTOAPerfilUsuario(perfilUsuarioDTO);
                    perfilUsuarioDao.crearPerfilUsuario(perfilUsuario);
                }
            }
            List<PerfilUsuario> lista = perfilUsuarioDao.listarPerfilUsuarioPorIdUsuario(perfilUsuarioDTOList.get(0).getId_usuario());
            usuario.setPerfiles(lista);

        } else {

            PerfilUsuario perfilUsuario = new PerfilUsuario();
            Usuario usuarioTest = new Usuario();
            int id_usuario = 0;
            Usuario usuario = new Usuario();
            usuarioTest = usuarioDao.buscarPorUsername(perfilUsuarioDTOList.get(0).getUsuario());
            if (usuarioTest == null) {
                usuario.setNombre(perfilUsuarioDTOList.get(0).getUsuario().getNombre());
                usuario.setCorreo(perfilUsuarioDTOList.get(0).getUsuario().getCorreo());
                usuario.setUsername(perfilUsuarioDTOList.get(0).getUsuario().getUsername());
                usuario.setPassword(perfilUsuarioDTOList.get(0).getUsuario().getPassword());
                id_usuario = usuarioDao.crearUsuario(usuario);
            } else {
                usuarioDao.actualizarUsuario(usuarioTest);
                id_usuario = usuarioTest.getId_usuario();
            }

            perfilUsuarioDao.eliminarTodoPerfilUsuario(id_usuario);

            for (PerfilUsuarioDTO perfilUsuarioDTO : perfilUsuarioDTOList) {

                perfilUsuarioDTO.setId_usuario(id_usuario);

            }

            for (PerfilUsuarioDTO perfilUsuarioDTO : perfilUsuarioDTOList) {

                perfilUsuarioDTO.setPerfilDTO(perfilServicio.buscarPerfil(perfilUsuarioDTO.getId_perfil()));
                perfilUsuarioDTO.setUsuarioDTO(usuarioServicio.buscarUsuario(perfilUsuarioDTO.getId_usuario()));
                perfilUsuario = convertirDTOAPerfilUsuario(perfilUsuarioDTO);
                perfilUsuarioDao.crearPerfilUsuario(perfilUsuario);

            }

            List<PerfilUsuario> lista = perfilUsuarioDao.listarPerfilUsuarioPorIdUsuario(id_usuario);

            usuario.setPerfiles(lista);

        }

    }

    public PerfilUsuarioDTO buscarPerfilUsuario(int id) {

        PerfilUsuario perfilUser = perfilUsuarioDao.buscarPerfilUsuario(id);
        PerfilUsuarioDTO dto = new PerfilUsuarioDTO();
        return dto;

    }

    public void actualizarPerfilUsuario(PerfilUsuarioDTO dto) {

        PerfilUsuario perfilUser = new PerfilUsuario();
        perfilUser.setId_perfil(dto.getId_perfil());
        perfilUser.setId_usuario(dto.getId_usuario());
        perfilUsuarioDao.actualizarPerfilUsuario(perfilUser);

    }

    public void eliminarPerfilUsuario(int id) {

        perfilUsuarioDao.eliminarPerfilUsuario(id);

    }

    public List<PerfilUsuarioDTO> listarPerfilesPorUsuario(int id_usuario) {

        List<PerfilUsuarioDTO> listaDto = new ArrayList<PerfilUsuarioDTO>();
        for (PerfilUsuario perfilUsuario : perfilUsuarioDao.listarPerfilUsuarioPorIdUsuario(id_usuario)) {
            listaDto.add(convertirPerfilUsuarioADTO(perfilUsuario));

        }

        return listaDto;
    }

    public ArrayList<PerfilUsuarioDTO> listarPerfilUsuario() {

        ArrayList<PerfilUsuarioDTO> lista = new ArrayList<>();
        List<PerfilUsuario> listaPu = perfilUsuarioDao.listarPerfilUsuario();
        for (PerfilUsuario pu : listaPu) {
            pu.setPerfil(perfilServicio.convertirDTOAPerfil(perfilServicio.buscarPerfil(pu.getId_perfil())));
            pu.setUsuario(usuarioServicio.convertirDTOAUsuario(usuarioServicio.buscarUsuario(pu.getId_usuario())));
            lista.add(convertirPerfilUsuarioADTO(pu));
        }
        return lista;

    }

    public PerfilUsuario convertirDTOAPerfilUsuario(PerfilUsuarioDTO dto) {

        PerfilUsuario perfilUsuario = new PerfilUsuario();
        perfilUsuario.setId_perfil(dto.getId_perfil());
        perfilUsuario.setId_usuario(dto.getId_usuario());
        perfilUsuario.setUsuario(usuarioServicio.convertirDTOAUsuario(dto.getUsuarioDTO()));
        perfilUsuario.setPerfil(perfilServicio.convertirDTOAPerfil(dto.getPerfilDTO()));
        return perfilUsuario;
    }

    public List<PerfilUsuario> convertirArregloDTOAPerfilUsuario(List<PerfilUsuarioDTO> dto) {

        List<PerfilUsuario> listPerfilUsuario = new ArrayList<PerfilUsuario>();
        for (PerfilUsuarioDTO listPerfilUsuarioDTO : dto) {
            listPerfilUsuario.add(convertirDTOAPerfilUsuario(listPerfilUsuarioDTO));
        }
        return listPerfilUsuario;
    }

    public PerfilUsuarioDTO convertirPerfilUsuarioADTO(PerfilUsuario perfilUsuario) {

        PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();
        perfilUsuarioDTO.setId_perfil(perfilUsuario.getId_perfil());
        perfilUsuarioDTO.setId_usuario(perfilUsuario.getId_usuario());
        perfilUsuarioDTO.setUsuarioDTO(usuarioServicio.convertirUsuarioADTO(perfilUsuario.getUsuario()));
        perfilUsuarioDTO.setPerfilDTO(perfilServicio.convertirPerfilADTO(perfilUsuario.getPerfil()));
        return perfilUsuarioDTO;
    }

    public ArrayList<PerfilUsuarioDTO> convertirArregloPerfilUsuarioADTO(List<PerfilUsuario> perfilUsuario) {

        ArrayList<PerfilUsuarioDTO> listPerfilUsuarioDTO = new ArrayList<PerfilUsuarioDTO>();
        for (PerfilUsuario listPerfilUsuario : perfilUsuario) {
            listPerfilUsuarioDTO.add(convertirPerfilUsuarioADTO(listPerfilUsuario));
        }
        return listPerfilUsuarioDTO;
    }

}
