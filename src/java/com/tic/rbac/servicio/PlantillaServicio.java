/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tic.rbac.servicio;

import com.tic.carpetas.dao.DocumentoDAO;
import com.tic.carpetas.dominio.Documento;
import com.tic.rbac.dao.PlantillaDAO;
import com.tic.rbac.dominio.Plantilla;
import com.tic.rbac.dto.PlantillaDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class PlantillaServicio {

    @EJB
    private PlantillaDAO plantillaDao;
    
    @EJB
    private DocumentoDAO documentoDAO;

    public void crearPlantilla(PlantillaDTO dto) {

        Plantilla plantilla = new Plantilla();
        plantilla.setSubcategoria(dto.getNombreTipoDocumento());
        plantilla.setId_subcategoria(dto.getTipo_id());
        plantillaDao.crearPlantilla(plantilla);

    }

    public PlantillaDTO buscarPlantilla(int id) {

        Plantilla plantilla = plantillaDao.buscarPlantilla(id);
        PlantillaDTO plantillaDTO = new PlantillaDTO();
        plantillaDTO.setNombreTipoDocumento(plantilla.getSubcategoria());
        plantillaDTO.setTipo_id(plantilla.getId_subcategoria());
        return plantillaDTO;

    }

    public void actualizarPlantilla(PlantillaDTO dto) {

        Plantilla plantilla = new Plantilla();
        plantilla.setId_plantilla(dto.getId_plantilla());
        plantilla.setSubcategoria(dto.getNombreTipoDocumento());
        plantilla.setId_subcategoria(dto.getTipo_id());
        plantillaDao.actualizarPlantilla(plantilla);

    }

    public void eliminarPlantilla(int id) {

        plantillaDao.eliminarPlantilla(id);

    }
    
    public void eliminarPlantillaPorId(int id) {
        
        List<Documento> lis = documentoDAO.buscarDocumentoPorTipoDocumento(id);
        
        if(lis.size()<1){
            plantillaDao.eliminarPlantillaPorId(id);
        }
        

    }
    
    public ArrayList<PlantillaDTO> listarPlantillas() {
        ArrayList<PlantillaDTO> plantillaDTO = new ArrayList<PlantillaDTO>();
        plantillaDTO = convertirArregloPlantillaADTO(plantillaDao.buscarTodosLasPlantillas());
        return plantillaDTO;

    }

    public Plantilla convertirDTOAPlantilla(PlantillaDTO dto) {

        Plantilla plantilla = new Plantilla();
        plantilla.setId_plantilla(dto.getId_plantilla());
        plantilla.setSubcategoria(dto.getNombreTipoDocumento());
        plantilla.setId_subcategoria(dto.getTipo_id());
        return plantilla;
    }

    public List<Plantilla> convertirArregloDTOAPlantilla(List<PlantillaDTO> dto) {

        List<Plantilla> listPlantilla = new ArrayList<Plantilla>();
        for (PlantillaDTO listPlantillaDTO : dto) {
            listPlantilla.add(convertirDTOAPlantilla(listPlantillaDTO));
        }
        return listPlantilla;
    }

    public PlantillaDTO convertirPlantillaADTO(Plantilla plantilla) {

        PlantillaDTO plantillaDTO = new PlantillaDTO();
        plantillaDTO.setId_plantilla(plantilla.getId_plantilla());
        plantillaDTO.setNombreTipoDocumento(plantilla.getSubcategoria());
        plantillaDTO.setTipo_id(plantilla.getId_subcategoria());
        return plantillaDTO;

    }

    public ArrayList<PlantillaDTO> convertirArregloPlantillaADTO(List<Plantilla> plantilla) {

        ArrayList<PlantillaDTO> listPlantillaDTO = new ArrayList<PlantillaDTO>();
        for (Plantilla listPlantilla : plantilla) {
            listPlantillaDTO.add(convertirPlantillaADTO(listPlantilla));
        }
        return listPlantillaDTO;
    }

}
