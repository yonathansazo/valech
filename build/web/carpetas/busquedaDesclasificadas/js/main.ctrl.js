menuApp.controller('controllerBusquedaDesclasificada', function ($scope, $http, $rootScope, MY_WSREST, $route, $timeout, $location) {
    $rootScope.version_global = undefined;
    var uriVersion = MY_WSREST.uriRest + "valech/api/version/";
    var uriVersionDocumento = MY_WSREST.uriRest + "valech/api/versiondocumento/";
    var uriCarpeta = MY_WSREST.uriRest + "valech/api/carpeta/";
    var uriDocumentos = MY_WSREST.uriRest + "valech/api/documento/";
     
    $scope.tipo_documentos = [
        {descripcion: "Persona", valor: true},
        {descripcion: "Administrativo", valor: false}
    ];
    
    
    $scope.currentPage = 0; //Pagina inicial
    var pageSize = 13; // Esta la cantidad de paginas que traera el paginador
    $scope.pages = [];
    var busquedas = [];
    var busqueda = [];
    var itemArray = [];
    $scope.nextPage = 2;
    $scope.backPage = 0;
    
    //Metodo de recarga de pagina
    $scope.reloadRoute = function () {
        $route.reload();
    };
    
    //Metodo para redirigir a otra pagina, se le entrega el route de la pagina
    $scope.changeRoute = function (url, forceReload) {
        $scope = $scope || angular.element(document).scope();
        if (forceReload || $scope.$$phase) { // that's right TWO dollar signs: $$phase
            window.location = url;
        } else {
            $location.path(url);
            $scope.$apply();
        }
    };
    
    //En caso que venga una busqueda cargada, a nivel global, se le entrega como parametro al
    //servicio rest para que liste esa busqueda, caso contrario traera todos los registros.
    if ($rootScope.busquedas_dec !== undefined) {
        busquedas = $rootScope.busquedas_dec;
    }
    
    //Obtención de todos los registros al cargar el el desliegue, al existir una busqueda vigente
    //se lista lo de la busqueda, señalada en 'busquedas' 
    //Inicia Busqueda y paginación
    $http.put(uriVersion + "versionPaginacion", busquedas).then(function (response) {
        $scope.busqueda = response.data;
        $scope.pages = [];
        $scope.final = $scope.busqueda.numeropaginas;
        var ini;
        var fin;
        if ($scope.busqueda.numeropaginas < pageSize) {
            ini = 1;
            fin = $scope.busqueda.numeropaginas;
        } else {
            ini = 1;
            fin = pageSize;
        }
        
        for (var i = ini; i < fin; i++) {
            $scope.pages.push({no: i});
            
        }
        if ($scope.busqueda.numeropaginas > 100) {
            $scope.pages.push({no: 100});
        }
        if ($scope.busqueda.numeropaginas > 1000) {
            $scope.pages.push({no: 1000});
        }
        if ($scope.busqueda.numeropaginas > 10000) {
            $scope.pages.push({no: 10000});
        }
        $scope.pages.push({no: $scope.busqueda.numeropaginas});
    });
    
    $http.put(uriVersion + "versionPaginada", busquedas).then(function (response) {
        $scope.listaVersiones = response.data;
    });
    //Inicia Busqueda y paginación
    
    //Al regalizar una busqueda especifica se utiliza este metodo.
    //Inicio Funcion Buscar
    $scope.buscar = function (busqueda) {
        $scope.currentPage = 0;
        $scope.currentPage = 0;
        if(busqueda.letra_caja === ''){
            busqueda.letra_caja = undefined;
        }
        if(busqueda.letra_carpeta === ''){
            busqueda.letra_carpeta = undefined;
        }
        if(busqueda.tipo_id === ''){
            busqueda.tipo_id = undefined;
        }
        var busquedas = {
            "serie": busqueda.serie || null,
            "comision": busqueda.comision || null,
            "caja": busqueda.caja || null,
            "letra_caja": busqueda.letra_caja || null,
            "carpeta": busqueda.carpeta || null,
            "letra_carpeta": busqueda.letra_carpeta || null,
            "nombre": busqueda.nombre || null,
            "rut": busqueda.rut || null,
            "nro_interno": busqueda.nro_interno || null,
            "tipo_carpeta": busqueda.tipo_carpeta || null,
            "identificador": busqueda.identificador || null,
            "estado": busqueda.estado || null
        };
        $rootScope.busquedas_dec = busqueda;
        $scope.reloadRoute();
    };
    
    
    //Limpia la Busqueda
    $scope.limpiarBusqueda = function () {
        if ($scope.busqueda !== undefined) {
            $scope.busqueda = {};
        }
        $rootScope.busquedas_dec = undefined;
        busquedas = [];
        $scope.reloadRoute();
        
    };
    
    
    //Función que setea el numero de pagina
    //Revisa el hacia adelante y hacia atras
    $scope.setPage = function (index) {
        $scope.currentPage = 0;
        $scope.pages = [];
        if ($rootScope.busquedas_dec !== undefined) {
            var busquedas = {
                "serie": $rootScope.busquedas_declasificaciones.serie,
                "comision": $rootScope.busquedas_declasificaciones.comision,
                "caja": $rootScope.busquedas_declasificaciones.caja,
                "letra_caja": $rootScope.busquedas_declasificaciones.letra_caja,
                "carpeta": $rootScope.busquedas_declasificaciones.carpeta,
                "letra_carpeta": $rootScope.busquedas_declasificaciones.letra_carpeta,
                "nombre": $rootScope.busquedas_declasificaciones.nombre,
                "rut": $rootScope.busquedas_declasificaciones.rut,
                "nro_interno": $rootScope.busquedas_declasificaciones.nro_interno,
                "tipo_carpeta": $rootScope.busquedas_declasificaciones.tipo_carpeta,
                "identificador": $rootScope.busquedas_declasificaciones.identificador,
                "estado": $rootScope.busquedas_declasificaciones.estado
            };
        } else {
            var busquedas = {
                "serie": busqueda.serie,
                "comision": busqueda.comision,
                "caja": busqueda.caja,
                "letra_caja": busqueda.letra_caja,
                "carpeta": busqueda.carpeta,
                "letra_carpeta": busqueda.letra_carpeta,
                "nombre": busqueda.nombre,
                "rut": busqueda.rut,
                "nro_interno": busqueda.nro_interno,
                "tipo_carpeta": busqueda.tipo_carpeta,
                "identificador": busqueda.identificador,
                "estado": busqueda.estado
            };
        }
        
        
        if ($scope.busqueda.numeropaginas < pageSize) {
            ini = 1;
            fin = $scope.busqueda.numeropaginas;
        } else {
            if ((index - 10) > 0 && (index + pageSize) <= $scope.busqueda.numeropaginas) {
                
                
                ini = index;
                fin = index + pageSize;
            } else {
                ini = 1;
                fin = pageSize;
            }
            
        }
        
        for (var i = ini; i < fin; i++) {
            $scope.pages.push({no: i});
            
        }
        $scope.currentPage = index-1;
        if ($scope.busqueda.numeropaginas > 100 && $scope.currentPage < 100) {
            $scope.pages.push({no: 100});
        }
        if ($scope.busqueda.numeropaginas > 1000 && $scope.currentPage < 1000) {
            $scope.pages.push({no: 1000});
        }
        if ($scope.busqueda.numeropaginas > 10000 && $scope.currentPage < 10000) {
            $scope.pages.push({no: 10000});
        }
        
        $scope.nextPage = index + 1;
        $scope.backPage = index - 1;
        $scope.pages.push({no: $scope.busqueda.numeropaginas});
        
        
        $http.put(uriVersion + "versionPaginada/", busquedas).then(function (response) {
            $scope.cabecera = [];
            itemArray = response.data;
            $scope.itemArray = response.data;
            for (var i = 0; i < $scope.itemArray.length; i++) {
                $scope.data = $scope.itemArray[i].carpeta;
                $scope.cabecera.carpeta_id = $scope.itemArray[i].carpeta;
            }
            
        });
    };
    
    
    $scope.verPdf = function (i) {
        console.log(i);
        $http({
            url: '/valech/viewpdf?' + i.documento_id,
            method: "GET",
        }).then(function (response) {
            $scope.message = response.data;
        }, function (response) {
            //fail case
            $scope.message = response;
        });
    };
    
    $scope.VersionEdicion = function(i){
       $rootScope.version_global = i;
       var id_carpeta = i.carpeta_id;
       $rootScope.id_carpeta = id_carpeta;
       $http.get(uriCarpeta + id_carpeta).then(function (response) {
        var carpeta = response.data;
        $rootScope.cabecera_aux = response.data;
        var plantillas = response.data.documentos;
        var lineas = [];
        for (var i = 0; i < plantillas.length; i++) {
                var linea = {};
                linea.paginas = plantillas[i].paginas;
                linea.indice = plantillas[i].documento_id;
                linea.indiceDel = plantillas[i].documento_id;
                linea.vacio = plantillas[i].folioInicial;
                linea.vacio1 = plantillas[i].folioFinal;
                linea.comentario = plantillas[i].comentario;
                linea.checkContar = plantillas[i].preservacion;
                linea.numeropdf = plantillas[i].numeropdf;
                linea.colorboton = "btn-success";
                linea.nombre_cod = plantillas[i].nombreTipoDocumento;
                linea.selected = plantillas[i];

                if (plantillas[i].numeropdf < 1) {
                    linea.boton = false;
                    linea.nopdf = true;
                    linea.edit = true;
                } else {
                    linea.boton = true;
                    linea.nopdf = false;
                    linea.edit = true;
                }
                lineas.push(linea);
            }
        $rootScope.documentos_aux = lineas;
    }).then(function (response) {
        $scope.changeRoute('#descalificarCarpetas');
    });
        
    
       
       
    };
    
    function limpiarOrden() {
            $scope.ordenInterno = "";
            $scope.ordenFecha = "";
            $scope.ordenNombre = "";
            $scope.ordenRut = "";
            $scope.ordenIdentificador = "";
            $scope.ordenCaja = "";
            $scope.ordenCarpeta = "";
            $scope.ordenComision = "";
            $scope.ordenSerie = "";
            $scope.ordenTipo = "";
            $scope.ordenEstado = "";
        }
  
    $scope.ordenarPorNInterno = function (orden) {
        limpiarOrden();
        $scope.ordenInterno = orden;
        if ($scope.hidenro_interno !== true) {
            $scope.hidenro_interno = true;
            $scope.hidenro_interno1 = false;
        } else {
            $scope.hidenro_interno = false;
            $scope.hidenro_interno1 = true;
        }
    };
    $scope.ordenarPorFecha = function (orden) {
        limpiarOrden();
        $scope.ordenFecha = orden;
        if ($scope.hidefecha !== true) {
            $scope.hidefecha = true;
            $scope.hidefecha1 = false;
        } else {
            $scope.hidefecha = false;
            $scope.hidefecha1 = true;
        }
    };
    $scope.ordenarPorNombre = function (orden) {
        limpiarOrden();
        $scope.ordenNombre = orden;
        if ($scope.hidenombre !== true) {
            $scope.hidenombre = true;
            $scope.hidenombre1 = false;
        } else {
            $scope.hidenombre = false;
            $scope.hidenombre1 = true;
        }
    };
    $scope.ordenarPorRut = function (orden) {
        limpiarOrden();
        $scope.ordenRut = orden;
        if ($scope.hiderut !== true) {
            $scope.hiderut = true;
            $scope.hiderut1 = false;
        } else {
            $scope.hiderut = false;
            $scope.hiderut1 = true;
        }
    };
    $scope.ordenarPorIdentificador = function (orden) {
        limpiarOrden();
        $scope.ordenIdentificador = orden;
        if ($scope.hideidentificador !== true) {
            $scope.hideidentificador = true;
            $scope.hideidentificador1 = false;
        } else {
            $scope.hideidentificador = false;
            $scope.hideidentificador1 = true;
        }
    };
    $scope.ordenarPorCaja = function (orden) {
        limpiarOrden();
        $scope.ordenCaja = orden;
        if ($scope.hidecaja !== true) {
            $scope.hidecaja = true;
            $scope.hidecaja1 = false;
        } else {
            $scope.hidecaja = false;
            $scope.hidecaja1 = true;
        }
    };
    $scope.ordenarPorCarpeta = function (orden) {
        limpiarOrden();
        $scope.ordenCarpeta = orden;
        if ($scope.hidecarpeta !== true) {
            $scope.hidecarpeta = true;
            $scope.hidecarpeta1 = false;
        } else {
            $scope.hidecarpeta = false;
            $scope.hidecarpeta1 = true;
        }
    };
    $scope.ordenarPorComision = function (orden) {
        limpiarOrden();
        $scope.ordenComision = orden;
        if ($scope.hidecomision !== true) {
            $scope.hidecomision = true;
            $scope.hidecomision1 = false;
        } else {
            $scope.hidecomision = false;
            $scope.hidecomision1 = true;
        }
    };
    $scope.ordenarPorSerie = function (orden) {
        limpiarOrden();
        $scope.ordenSerie = orden;
        if ($scope.hideserie !== true) {
            $scope.hideserie = true;
            $scope.hideserie1 = false;
        } else {
            $scope.hideserie = false;
            $scope.hideserie1 = true;
        }
    };
    $scope.ordenarPorTipo = function (orden) {
        limpiarOrden();
        $scope.ordenTipo= orden;
        if ($scope.hidetipo !== true) {
            $scope.hidetipo = true;
            $scope.hidetipo1 = false;
        } else {
            $scope.hidetipo = false;
            $scope.hidetipo1 = true;
        }
    };
    $scope.ordenarPorEstado = function (orden) {
        limpiarOrden();
        $scope.ordenEstado= orden;
        if ($scope.hideestado !== true) {
            $scope.hideestado = true;
            $scope.hideestado1 = false;
        } else {
            $scope.hideestado = false;
            $scope.hideestado1 = true;
        }
    };
    
    
    
    
    
    
    
});