menuApp.service('userService', function () {
    return {
        carpeta: '',
        estado: 'success',
        cabecera: {carpeta: "", comision: "", nro_caja: "", califica: "", letra_caja: "", letra_carpeta: "", serie: ""}
    };
});

menuApp.controller('modDocCtrl', function ($scope, $timeout, userService, $http, $route, $rootScope, $location, $uibModal, MY_WSREST) {

    //Bloque ejecución inicio
    //////////////////////////////////////////////////
    var vm = this;
    $scope.mensaje = '';
    var uriCarpeta = MY_WSREST.uriRest + "valech/api/carpeta/";
    var uriDocumentos = MY_WSREST.uriRest + "valech/api/documento/";
    var uriTipoDocumentos = MY_WSREST.uriRest + "valech/api/tipodocumento/";
    var uriPlantilla = MY_WSREST.uriRest + "valech/api/plantilla/";
    var rv;
    var rh;

    $scope.tipo_documentos = [
        {descripcion: "Persona", valor: true},
        {descripcion: "Administrativo", valor: false}
    ];
    
    $scope.tipodoc = true;

    $scope.ordenInicial = '';
    $scope.ordenFinal = '';
    $scope.habilitaFlecha = true;
    $scope.habilitaFlechaFinal = true;


    if ($rootScope.revalidador === undefined) {
        $scope.rv = '';
    } else {
        $scope.rv = true;
    }
    if ($rootScope.rehabilitado === undefined) {
        $scope.rh = '';
    } else {
        $scope.rh = true;
    }

    $rootScope.validador = $scope.rv;
    $rootScope.habilitado = $scope.rh;
    $rootScope.rutbueno = '';
    $rootScope.rutmalo = '';
    $rootScope.bueno = '';


    $scope.paginas = 0;

    //Carga Cabecera
    $scope.cabecera = {
        "apela": false,
        "califica": false,
        "carpeta": '',
        "carpeta_id": '',
        "comision": '',
        "documentos": [],
        "nro_caja": '',
        "rut": ''
    };



    $http.get(uriCarpeta + $rootScope.id_carpeta).then(function (response) {
        var carpeta = response.data;
        console.log("CARPETA");
        console.log(carpeta);
        var plantillas = [];

        if (carpeta.apela) {
            $scope.cabecera.apela = "true";
        } else {
            $scope.cabecera.apela = "false";
        }

        if (carpeta.serie === "C") {
            $scope.cabecera.califica = "true";
        }else if (carpeta.serie === "NC") {
            $scope.cabecera.califica = "false";
        }else {
            $scope.cabecera.califica = "true";
        }
        
        if (carpeta.tipo_carpeta === 'Persona') {
            $scope.cabecera.tipo_carpeta = true;
        }else {
            $scope.cabecera.tipo_carpeta = false;
        }

        $scope.declara = carpeta.declara;

        $scope.cabecera.carpeta = carpeta.carpeta;
        $scope.cabecera.id = carpeta.id;
        $scope.cabecera.nro_caja = carpeta.nro_caja;
        $scope.cabecera.rut = carpeta.rut;
        $scope.cabecera.comision = carpeta.comision,
        $scope.cabecera.dv = carpeta.dv;
        $scope.cabecera.nombres = carpeta.nombres;
        $scope.cabecera.apellidos = carpeta.apellidos;
        $scope.cabecera.identificador = carpeta.identificador;
        $scope.cabecera.serie = carpeta.serie;
        $scope.cabecera.letra_caja = carpeta.letra_caja || '';
        $scope.cabecera.letra_carpeta = carpeta.letra_carpeta || '';
        $scope.cabecera.nombrepdfnotario = carpeta.nombrepdfnotario;
        $scope.cabecera.rutapdfnotario = carpeta.rutapdfnotario;
        plantillas = carpeta.documentos;

        $scope.paginasDoc = carpeta.numeropaginas;

        if (carpeta.numerodocumentos < 1) {
            //Inicio Funcion Cargar Categorias
            $http.get(uriTipoDocumentos).then(function (response) {
                var itemArray = response.data;
                $scope.itemArray = itemArray;
                $scope.selectedItem = $scope.itemArray[0];
                $scope.paginas = 0;
                $scope.paginaspdf = 0;
                $scope.dataDoc = 0;
                $http.get(uriPlantilla).then(function (response) {
                    var plantillas = response.data;
                    var linea = {};
                    //  Iniciar con Plantilla

                    $scope.plantillas = plantillas;
                    for (var i = 0; i < plantillas.length; i++) {
                        linea.nombre_cod = plantillas[i].nombreTipoDocumento;
                        linea.selected = plantillas[i];
                        $scope.agregarModi(i, linea);
                    }

                });
            });
            //Fin Funcion Cargar Categorias

        } else {
            $scope.paginaspdf = 0;

            //  Iniciar con Documentos
            for (var i = 0; i < plantillas.length; i++) {
                var linea = {};
                linea.paginas = plantillas[i].paginas;
                linea.indice = plantillas[i].documento_id;
                linea.indiceDel = plantillas[i].documento_id;
                linea.vacio = plantillas[i].folioInicial;
                linea.vacio1 = plantillas[i].folioFinal;
                linea.comentario = plantillas[i].comentario;
                linea.checkContar = plantillas[i].preservacion;
                linea.numeropdf = plantillas[i].numeropdf;
                linea.colorboton = "btn-success";
                linea.nombre_cod = plantillas[i].nombreTipoDocumento;
                linea.selected = plantillas[i];

                if (plantillas[i].numeropdf < 1) {
                    linea.boton = false;
                    linea.nopdf = true;
                    linea.edit = true;
                } else {
                    linea.boton = true;
                    linea.nopdf = false;
                    linea.edit = true;
                }
                $scope.dataDel = plantillas[i].documento_id;
                $scope.boton = true;
                $scope.lineas.push(linea);
                
            }

            $scope.plantillas = plantillas;
        }
    });
    
    
    //Fin Bloque ejecución inicio
    //////////////////////////////////////////////////
    $scope.abrirModalDescalifica = function(){
        $rootScope.cabecera_aux = $scope.cabecera;
        console.log($rootScope.cabecera_aux);
        $rootScope.documentos_aux = $scope.lineas;
        $scope.changeRoute('#descalificarCarpetas');
    }
    
    $scope.abrirModalDocumentos = function(linea, plantillas, lineas, indice){
        
        var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'cambiaModalDocumento.html',
                controller: 'cambiarDocumentoController',
                resolve: {
                }
            });
            
        if(isNaN(linea.paginas)){
            console.log('if');
            linea.paginas = 0;
        }
            
           $rootScope.linea_aux = linea;
           $rootScope.plantillas = plantillas;
           $rootScope.lineas_aux = lineas;
           $rootScope.indice_aux = indice;
        
    }


//Metodo para redirigir a otra pagina, se le entrega el route de la pagina
    $scope.changeRoute = function (url, forceReload) {
        $scope = $scope || angular.element(document).scope();
        if (forceReload || $scope.$$phase) { // that's right TWO dollar signs: $$phase
            window.location = url;
        } else {
            $location.path(url);
            $scope.$apply();
        }
    };

    //Agregar Linea
    $scope.agregarModificar = function (index, linea) {
        var nuevaLinea = {};
        var lineasCopiadas = [];
        var objetoCopiado = [];
        var lineaCopiada = {};
        lineasCopiadas = angular.copy($scope.lineas);
        objetoCopiado = angular.copy(lineasCopiadas[$scope.lineas.length - 1].selected);
        lineaCopiada = angular.copy(lineasCopiadas[$scope.lineas.length - 1]);
        nuevaLinea = angular.copy(linea);
        nuevaLinea.indice = 0;
        nuevaLinea.indiceDel = 0;
        nuevaLinea.boton = false;
        nuevaLinea.nopdf = false;
        nuevaLinea.numeropdf = 0;
        nuevaLinea.edit = false;
        $scope.lineas.splice(index + 1, 0, nuevaLinea);
        $scope.lineas[$scope.lineas.length - 1].vacio = lineaCopiada.folioInicial;
        $scope.lineas[$scope.lineas.length - 1].vacio1 = lineaCopiada.folioFinal;
        $scope.plantillas[$scope.lineas.length - 1] = objetoCopiado;
        if ($scope.lineas.length <= 1) {
            $scope.disabledRestar = true;
        } else {
            $scope.disabledRestar = false;
        }
    };
    
    


    //   Agregar Linea
    $scope.agregarModi = function (index, linea) {
        var siguiente = index + 1;
        if (numero <= 1) {
            $scope.disabledRestar = false;
        }
        numero++;
        //Inserto una nueva linea debajo de la linea en donde se presionó agregar
        //splice(index, 0, item);

        $scope.lineas.splice(index + 1, 0, angular.copy(linea));
    };


    //    Restar Linea y eliminar registro
    $scope.restar = function (index, idDoc) {

        if ($scope.lineas.length <= 1) {
            $scope.disabledRestar = true;
        } else {
            $scope.lineas.splice(index, 1);
            $http.delete(uriDocumentos + idDoc).then(function (response) {
            });
            $scope.disabledRestar = false;
        }
    };



    $scope.eliminarDocumentos = function () {
        $scope.AbrirEliminarDocumentosModal();
    }

    $scope.imprimir = function () {

        var divName = 'informe';
        var printContents = document.getElementById(divName).innerHTML;
        var popupWin = window.open('', 'Sellos', '_blank', 'width=100%,height=100%');
        popupWin.document.open();
        popupWin.document.write('<html><head><link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"></head><body onload="window.print()" style: width:100%; height:100%;>' + printContents + '</body></html>');
        popupWin.document.close();

    }


    $scope.AbrirEliminarModal = function () {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'EliminarModalIngreso.html',
            controller: 'EliminarModalCtrller',
            resolve: {
            }
        });
        ;
    };

    $scope.AbrirEliminarDocumentosModal = function () {
        $rootScope.eliminarcarpeta = $rootScope.id_carpeta;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'AbrirEliminarDocumentosModal.html',
            controller: 'EliminarDocModalCtrller',
            resolve: {
            }
        });
        
    };

//cambiar
//Inicio Funcion Guardar
    $scope.guardar = function (cabecera) {

        var cont = 0;
        var alerta = true;
        $scope.calcularPaginasPdf();
        var documentos = [];

        if(cabecera.tipo_carpeta){
           cabecera.tipo_carpeta = "Persona";
        }else{
           cabecera.tipo_carpeta = "Administrativo";
        }
        
        var cabecera = {
            "apela": cabecera.apela,
            "califica": cabecera.califica,
            "carpeta": cabecera.carpeta,
            "carpeta_id": userService.carpeta,
            "nro_caja": cabecera.nro_caja,
            "rut": cabecera.rut,
            "dv": $scope.cabecera.dv,
            "nombres": cabecera.nombres,
            "apellidos": cabecera.apellidos,
            "identificador": cabecera.identificador,
            "nombrepdfnotario": cabecera.nombrepdfnotario,
            "rutapdfnotario": cabecera.rutapdfnotario,
            "comision": cabecera.comision,
            "serie": cabecera.serie,
            "tipo_carpeta": cabecera.tipo_carpeta,
            "letra_carpeta": cabecera.letra_carpeta,
            "letra_caja": cabecera.letra_caja,
            "documentos": documentos
        };
        $http.put(uriCarpeta, cabecera).then(function (response) {
        });
        $scope.bueno = true;
        $timeout(function () {
            $scope.bueno = false;
        }, 1500);

    };
    //Fin Funcion Guardar


    $scope.guardarYCerrar = function (cabecera) {
        $scope.AbrirEliminarModal();

    }

    //Calcular DV
    //<editor-fold defaultstate="collapsed" desc="Función que calcula el DV">

    $scope.calcularDv = function (rut, dv) {
        var r_dv;
        if (rut !== null && rut.length > 6) {
            var nuevo_numero = rut.toString().split("").reverse().join("");
            ;
            for (var i = 0, j = 2, suma = 0; i < nuevo_numero.length; i++, ((j === 7) ? j = 2 : j++)) {
                suma += (parseInt(nuevo_numero.charAt(i)) * j);
            }
            var n_dv = 11 - (suma % 11);
            var r_dv = ((n_dv === 11) ? 0 : ((n_dv === 10) ? "K" : n_dv));
        } else if (rut < 7) {
            var r_dv = '';
        }

        var rutcompleto = rut + dv;
        var rutvalidar = rut + r_dv;
        if (rutcompleto === rutvalidar) {
            $rootScope.rutbueno = true;
            $rootScope.rutmalo = false;
            $rootScope.validador = true;
        }
        if (rutcompleto !== rutvalidar) {
            $rootScope.rutmalo = true;
            $rootScope.rutbueno = false;
            $rootScope.validador = false;
        }
        $timeout(function () {
            $scope.rutbueno = false;
            $scope.rutmalo = false;
        }, 1500);
    };







    $scope.reloadRoute = function () {
        $rootScope.id_carpeta = undefined;
        userService.carpeta = 0;
        $scope.contarPaginas();
        $route.reload();
    };


    $rootScope.$on('Reload', function (event, reload) {
        if (reload) {
            $route.reload();
        }
    });

    $rootScope.$on('change', function (event, reload) {
        $scope.changeRoute('#busquedaCarpetas');
    });

    $scope.estado = 'primary';
    $scope.cabecera = {
        carpeta: ""
    };
    userService.cabecera = $scope.cabecera;


//    Cargar comisiones
    $scope.comisiones = [{nombre: 'V - I', id: 0}, {nombre: 'V - II', id: 1}];
//    Cargar Categorias

    $scope.lineas = [];
    //Para contar cuantas lineas hay
    var numero;
    //Inicialicio el Array lineas con una linea vacía.
    var cargado = false; //Para que cargue solo una vez

    if (!cargado) {
        if ($scope.lineas === null) {
            $scope.lineas = [
                linea
            ];
            numero = 1;
        } else {
            $scope.lineas = $scope.lineas;
            numero = $scope.lineas.length;
        }
        cargado = true;
    }

    //Calcular Paginas
    $scope.dataDoc = 0;
    $scope.$watch($scope.contarPaginas = function () {
        var total = 0;
        var pag = $scope.lineas;
        for (var i = 0, _len = pag.length; i < _len; i++) {

            if (!pag[i].checkContar) {
                pag[i].paginas = pag[i].folioFinal - pag[i].folioInicial;
                pag[i].paginas = pag[i].paginas + 1;
                total += parseInt(pag[i].paginas);
            }else{
                pag[i].paginas = 0;
            }
        }
        $scope.total = total;
        if (isNaN(total)) {

            $scope.habilitaImpresion = true;

        } else {
            $scope.habilitaImpresion = false;
        }
    });


    $scope.paginaspdf = 0;

    $scope.cambiacolor = function (index) {

        $scope.lineas[index].colorboton = "btn-danger";
        $timeout(function () {
            $scope.lineas[index].colorboton = "btn-success";
        }, 10500);
    };



    //Calculo segun elementos en el array y el valor del campo paginas
    $scope.calcularPaginasPdf = function () {
        var total = 0;
        var pag = $scope.lineas;
        for (var i = 0, _len = pag.length; i < _len; i++) {
            total += parseInt(pag[i].numeropdf);
            $scope.paginaspdf += parseInt(pag[i].numeropdf);
        }
        return total;
    };
    
    
    $rootScope.seleccionDocumento = function(documento, indice){
        $scope.lineas[indice].nombre_cod = documento.nombreTipoDocumento;
        $scope.lineas[indice].selected = documento;
    };

});

menuApp.controller('cambiarDocumentoController', function ($rootScope, $scope, $uibModalInstance, $uibModal, $http, $rootScope, $location, MY_WSREST) {
    
    var uriTipoDocumentos = MY_WSREST.uriRest + "valech/api/tipodocumento/";
    //Trae las categorias
    $scope.setnewservice = function (valor) {
        $http.get(uriTipoDocumentos + valor).then(function (response) {
            var itemArraySelect = response.data;
            $scope.itemArray = itemArraySelect;
        });
    };
    
    var linea = $rootScope.linea_aux;
    var plantillas = $rootScope.plantillas;
    var lineas = $rootScope.lineas_aux;
    var indice = $rootScope.indice_aux;
    
    $scope.guardarModificacion = function(documento, indice){
        $rootScope.seleccionDocumento(documento, indice);
        $uibModalInstance.close();        
    }
    
    $scope.cancelarfModificacion = function(){
        $uibModalInstance.close();
        
    }

    
});

menuApp.controller('EliminarModalCtrller', function ($scope, $uibModalInstance, $http, $rootScope, $location, MY_WSREST) {
    $scope.acepto = function () {
        $uibModalInstance.close();
        $rootScope.$emit('change');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

menuApp.controller('EliminarDocModalCtrller', function ($scope, $uibModalInstance, $http, $rootScope, MY_WSREST) {
    var urielimina = MY_WSREST.uriRest + "valech/api/carpeta/eliminaDocumentos/";
    var valor = $rootScope.eliminarcarpeta;
    $scope.acepto = function () {

        $http.post(urielimina + valor).then(function (response) {
            $rootScope.$emit('Reload', response.data);
        });
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

menuApp.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];
        if (angular.isArray(items)) {
            var keys = Object.keys(props);
            items.forEach(function (item) {
                var itemMatches = false;
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});
//Controlador PDF
menuApp.directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;
                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);
menuApp.service('fileUpload', ['$http', function ($http) {
        this.uploadFileToUrl = function (file, uploadUrl) {
            var fd = new FormData();
            fd.append('file', file);
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function (response) {
                $scope.estado = response.data;
            }, function (response) {
                //fail case
            });
        };
    }]);
menuApp.controller('myCtrl', ['$scope', 'fileUpload', '$http', '$timeout', 'userService', '$rootScope', function ($scope, fileUpload, $http, $timeout, userService, $rootScope) {
        $scope.uploadFile = function () {
            var file = $scope.myFile;
            var uploadUrl = "/valech/upload";
            var fd = new FormData();
            fd.append('file', file);
            $http.post(uploadUrl + "?carpeta=" + userService.cabecera.carpeta + "&caja=" + userService.cabecera.nro_caja
                    + "&letra_caja=" + userService.cabecera.letra_caja + "&letra_carpeta=" + userService.cabecera.letra_carpeta + "&serie=" + userService.cabecera.serie
                    + "&comision=" + userService.cabecera.comision + "&califica="
                    + userService.cabecera.califica + "&tipodoc=" + $scope.linea.selected.tipo_id
                    + "&doc_id=" + $scope.linea.indice + "&linea=" + $scope.$index
                    + "&folioinicial=" + $scope.linea.folioInicial + "&foliofinal=" + $scope.linea.folioFinal
                    + "&numeropaginas=" + $scope.linea.paginas + "&preservacion=" + $scope.linea.checkContar
                    + "&comentario=" + $scope.linea.comentario + "&paginaspdf=" + $scope.datapdf
                    + "&paginaspdf=" + $scope.datapdf + "&declara=false" + "&version_id=0", fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).then(function (response) {
                var array = response.data.split('-');
                if (array[5] < 1) {
                    $scope.linea.boton = false;
                    $scope.linea.nopdf = true;
                } else {
                    $scope.linea.boton = true;
                    $scope.linea.nopdf = false;
                }
                $scope.linea.indice = array[1];
                $scope.estado = array[3];
                $scope.data = array[1];
                $scope.dataDel = array[1];
                userService.carpeta = array[2];
                $rootScope.habilitado = array[4];
                $scope.verdadero = array[3];
                $scope.linea.numeropdf = array[5];
                $scope.linea.edit = array[6];
                $scope.datapdf = array[5];
                $scope.myFile = '';
                $scope.linea.colorboton = "btn-warning";
                $timeout(function () {
                    $scope.linea.colorboton = "btn-success";
                }, 2500);

            }, function (response) {
                //fail case
            });
        };
    }]);
menuApp.controller('verPdfCtrl', ['$scope', 'fileUpload', '$http', 'userService', function ($scope, fileUpload, $http, userService) {

        $scope.verPdf = function () {
            $http({
                url: '/valech/viewpdf?' + $scope.linea.indice,
                method: "GET",
            }).then(function (response) {
                $scope.message = response.data;
            }, function (response) {
                //fail case
                $scope.message = response;
            });
        };
    }]);

