package com.tic.carpetas.dominio;

import com.tic.carpetas.dominio.CategoriaDocumento;
import com.tic.carpetas.dominio.Documento;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-21T15:21:25")
@StaticMetamodel(TipoDocumento.class)
public class TipoDocumento_ { 

    public static volatile ListAttribute<TipoDocumento, Documento> documentos;
    public static volatile SingularAttribute<TipoDocumento, CategoriaDocumento> categoriadocumento;
    public static volatile SingularAttribute<TipoDocumento, Integer> tipo_id;
    public static volatile SingularAttribute<TipoDocumento, String> nombreTipoDocumento;

}