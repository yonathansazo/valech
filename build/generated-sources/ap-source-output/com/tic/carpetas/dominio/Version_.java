package com.tic.carpetas.dominio;

import com.tic.carpetas.dominio.VersionDocumento;
import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-21T15:21:25")
@StaticMetamodel(Version.class)
public class Version_ { 

    public static volatile SingularAttribute<Version, Boolean> apela;
    public static volatile SingularAttribute<Version, String> estado;
    public static volatile SingularAttribute<Version, String> nro_interno;
    public static volatile SingularAttribute<Version, String> declaracion;
    public static volatile SingularAttribute<Version, String> encabezado;
    public static volatile SingularAttribute<Version, Integer> version_id;
    public static volatile ListAttribute<Version, VersionDocumento> versionDocumento;
    public static volatile SingularAttribute<Version, String> nombre;
    public static volatile SingularAttribute<Version, Boolean> califica;
    public static volatile SingularAttribute<Version, String> rut;
    public static volatile SingularAttribute<Version, String> conclusion;
    public static volatile SingularAttribute<Version, Timestamp> fecha;
    public static volatile SingularAttribute<Version, String> letra_caja;
    public static volatile SingularAttribute<Version, String> letra_carpeta;
    public static volatile SingularAttribute<Version, String> tipo_carpeta;
    public static volatile SingularAttribute<Version, String> serie;
    public static volatile SingularAttribute<Version, Integer> comision;
    public static volatile SingularAttribute<Version, Integer> carpeta_id;
    public static volatile SingularAttribute<Version, Integer> documento_id;
    public static volatile SingularAttribute<Version, Integer> caja;
    public static volatile SingularAttribute<Version, Integer> carpeta;
    public static volatile SingularAttribute<Version, String> identificador;

}