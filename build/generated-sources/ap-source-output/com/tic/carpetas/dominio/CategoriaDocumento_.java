package com.tic.carpetas.dominio;

import com.tic.carpetas.dominio.TipoDocumento;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-21T15:21:25")
@StaticMetamodel(CategoriaDocumento.class)
public class CategoriaDocumento_ { 

    public static volatile ListAttribute<CategoriaDocumento, TipoDocumento> tipodocumentos;
    public static volatile SingularAttribute<CategoriaDocumento, Integer> categoria_id;
    public static volatile SingularAttribute<CategoriaDocumento, String> nombreCategoria;

}