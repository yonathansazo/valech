package com.tic.carpetas.dominio;

import com.tic.carpetas.dominio.Carpeta;
import com.tic.carpetas.dominio.TipoDocumento;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-21T15:21:25")
@StaticMetamodel(Documento.class)
public class Documento_ { 

    public static volatile SingularAttribute<Documento, Boolean> preservacion;
    public static volatile SingularAttribute<Documento, String> rutaDocumento;
    public static volatile SingularAttribute<Documento, String> comentario;
    public static volatile SingularAttribute<Documento, Boolean> activa;
    public static volatile SingularAttribute<Documento, Integer> paginas;
    public static volatile SingularAttribute<Documento, String> letra_caja;
    public static volatile SingularAttribute<Documento, String> nombreDocumento;
    public static volatile SingularAttribute<Documento, String> letra_carpeta;
    public static volatile SingularAttribute<Documento, Integer> carpeta_id;
    public static volatile SingularAttribute<Documento, Integer> documento_id;
    public static volatile SingularAttribute<Documento, Integer> numeropdf;
    public static volatile SingularAttribute<Documento, Integer> tipo_id;
    public static volatile SingularAttribute<Documento, Integer> folioInicial;
    public static volatile SingularAttribute<Documento, Integer> folioFinal;
    public static volatile SingularAttribute<Documento, Carpeta> carpeta;
    public static volatile SingularAttribute<Documento, TipoDocumento> tipodocumento;

}