package com.tic.carpetas.dominio;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-21T15:21:25")
@StaticMetamodel(VersionDocumento.class)
public class VersionDocumento_ { 

    public static volatile SingularAttribute<VersionDocumento, Integer> pagina;
    public static volatile SingularAttribute<VersionDocumento, String> nombre_documento;
    public static volatile SingularAttribute<VersionDocumento, Integer> documento_id;
    public static volatile SingularAttribute<VersionDocumento, Integer> version_documento_id;
    public static volatile SingularAttribute<VersionDocumento, Integer> version_id;
    public static volatile SingularAttribute<VersionDocumento, String> identificador;
    public static volatile SingularAttribute<VersionDocumento, String> ruta_documento;

}