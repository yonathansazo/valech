package com.tic.carpetas.dominio;

import com.tic.carpetas.dominio.Documento;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-21T15:21:25")
@StaticMetamodel(Carpeta.class)
public class Carpeta_ { 

    public static volatile SingularAttribute<Carpeta, String> apellidos;
    public static volatile SingularAttribute<Carpeta, Boolean> apela;
    public static volatile SingularAttribute<Carpeta, String> rutapdfnotario;
    public static volatile SingularAttribute<Carpeta, String> declara;
    public static volatile SingularAttribute<Carpeta, Boolean> activa;
    public static volatile SingularAttribute<Carpeta, String> nombres;
    public static volatile SingularAttribute<Carpeta, Boolean> califica;
    public static volatile SingularAttribute<Carpeta, String> rut;
    public static volatile ListAttribute<Carpeta, Documento> documentos;
    public static volatile SingularAttribute<Carpeta, Character> dv;
    public static volatile SingularAttribute<Carpeta, Integer> nro_caja;
    public static volatile SingularAttribute<Carpeta, String> letra_caja;
    public static volatile SingularAttribute<Carpeta, Integer> comision;
    public static volatile SingularAttribute<Carpeta, String> serie;
    public static volatile SingularAttribute<Carpeta, String> letra_carpeta;
    public static volatile SingularAttribute<Carpeta, String> tipo_carpeta;
    public static volatile SingularAttribute<Carpeta, Integer> carpeta_id;
    public static volatile SingularAttribute<Carpeta, Integer> numeropdf;
    public static volatile SingularAttribute<Carpeta, String> nombrepdfnotario;
    public static volatile SingularAttribute<Carpeta, Integer> numerodocumentos;
    public static volatile SingularAttribute<Carpeta, Integer> numeropaginas;
    public static volatile SingularAttribute<Carpeta, Integer> carpeta;
    public static volatile SingularAttribute<Carpeta, String> identificador;

}