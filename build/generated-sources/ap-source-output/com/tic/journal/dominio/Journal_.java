package com.tic.journal.dominio;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-21T15:21:25")
@StaticMetamodel(Journal.class)
public class Journal_ { 

    public static volatile SingularAttribute<Journal, Integer> id_usuario;
    public static volatile SingularAttribute<Journal, Integer> id_evento;
    public static volatile SingularAttribute<Journal, Date> fecha_evento;
    public static volatile SingularAttribute<Journal, Integer> id_modulo;
    public static volatile SingularAttribute<Journal, Integer> id_journal;

}