package com.tic.journal.dto;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-21T15:21:25")
@StaticMetamodel(JournalDTO.class)
public class JournalDTO_ { 

    public static volatile SingularAttribute<JournalDTO, Integer> id_usuario;
    public static volatile SingularAttribute<JournalDTO, Integer> id_evento;
    public static volatile SingularAttribute<JournalDTO, Date> fecha_evento;
    public static volatile SingularAttribute<JournalDTO, Integer> id_modulo;
    public static volatile SingularAttribute<JournalDTO, Integer> id_journal;

}