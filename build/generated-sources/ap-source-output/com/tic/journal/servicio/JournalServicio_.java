package com.tic.journal.servicio;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-21T15:21:25")
@StaticMetamodel(JournalServicio.class)
public class JournalServicio_ { 

    public static volatile SingularAttribute<JournalServicio, Integer> id_usuario;
    public static volatile SingularAttribute<JournalServicio, Integer> id_evento;
    public static volatile SingularAttribute<JournalServicio, Date> fecha_evento;
    public static volatile SingularAttribute<JournalServicio, Integer> id_modulo;
    public static volatile SingularAttribute<JournalServicio, Integer> id_journal;

}