package com.tic.rbac.dominio;

import com.tic.rbac.dominio.Perfil;
import com.tic.rbac.dominio.Usuario;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-21T15:21:25")
@StaticMetamodel(PerfilUsuario.class)
public class PerfilUsuario_ { 

    public static volatile SingularAttribute<PerfilUsuario, Integer> perfilusuario_id;
    public static volatile SingularAttribute<PerfilUsuario, Integer> id_usuario;
    public static volatile SingularAttribute<PerfilUsuario, Integer> id_perfil;
    public static volatile SingularAttribute<PerfilUsuario, Usuario> usuario;
    public static volatile SingularAttribute<PerfilUsuario, Perfil> perfil;

}