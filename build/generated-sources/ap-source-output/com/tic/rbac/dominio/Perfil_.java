package com.tic.rbac.dominio;

import com.tic.rbac.dominio.PerfilUsuario;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-21T15:21:25")
@StaticMetamodel(Perfil.class)
public class Perfil_ { 

    public static volatile SingularAttribute<Perfil, String> nombre_perfil;
    public static volatile SingularAttribute<Perfil, Integer> id_perfil;
    public static volatile SingularAttribute<Perfil, Boolean> check;
    public static volatile ListAttribute<Perfil, PerfilUsuario> usuarios;

}