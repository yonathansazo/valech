menuApp.controller('appCtrl', function ($scope, $uibModal, $log, $http, $rootScope, $route, MY_WSREST) {
    var uriUsuarios = MY_WSREST.uriRest + "valech/api/usuario/";
    var uriLlaves = MY_WSREST.uriRest + "valech/api/llave/";
    var uriPerfiles = MY_WSREST.uriRest + "valech/api/perfil/";
    var uriPerfilUsuario = MY_WSREST.uriRest + "valech/api/perfilusuario/";
    var uriRuta = MY_WSREST.uriRest + "valech/api/ruta/";



//Cargar listas de usuarios y perfiles
    obtenerRuta();
    listarUsuarios();
    listarPerfiles();
    listarLlaves();


    $rootScope.$on("CallParentMethodUsuario", function (event, listarUsuariosDos) {
        $scope.usuarios = listarUsuariosDos;
        $rootScope.usuarios = listarUsuariosDos;
    });

    $rootScope.$on("CallParentMethodPerfil", function (event, listarPerfilesDos) {
        $scope.perfiles = listarPerfilesDos;
        $rootScope.perfiles = listarPerfilesDos;
    });

    function obtenerRuta() {
        $http.get(uriRuta).then(function (response) {
            var rutaArchivo = response.data;
            $scope.ruta = rutaArchivo.rutaArchivo;
        });
    }
    $scope.mensaje = '';
    $scope.ruta = '';
    $scope.guardarRuta = function (ruta) {
        var rutaArchivos = {'rutaArchivo': ruta};
        $http.post(uriRuta, rutaArchivos).then(function (response) {
            obtenerRuta();
            $scope.ruta = '';
            $scope.mensaje = 'Se guardaran en ' + response.data.rutaArchivo;
        });
    }



    $scope.perfiles = [{}];
    function listarPerfiles() {
        $http.get(uriPerfiles).then(function (response) {
            $scope.perfiles = response.data;
            $rootScope.perfilesValidacion = response.data;
            $rootScope.perfiles = response.data;
        });
    }
    ;

    $scope.usuarios = [{}];
    function listarUsuarios() {
        $http.get(uriUsuarios).then(function (response) {
            $scope.usuarios = response.data;
            $rootScope.usuariosValidacion = $scope.usuarios;
        });
    }
    ;

    $scope.llavesCombo = [{}];
    function listarLlaves() {
        $http.get(uriLlaves).then(function (response) {
            $scope.llavesCombo = response.data;
            $rootScope.llavesCombo = response.data;
        });
    }
    ;

    $scope.eliminarPerfiles = function (perfilesSeleccionados) {

        var perfilesEliminar = [];
        for (var i = 0; i < perfilesSeleccionados.length; i++) {
            perfilesEliminar.push({
                "id_perfil": perfilesSeleccionados[i]
            });
        }
        $http.post(uriPerfiles, perfilesEliminar).then(function (response) {
            $rootScope.$emit("CallParentMethodPerfil", response.data);
        });

    };

    $scope.eliminarUsuarios = function (seleccionUsuario) {

        var usuariosEliminar = [];
        for (var i = 0; i < seleccionUsuario.length; i++) {
            usuariosEliminar.push({
                "id_usuario": seleccionUsuario[i]
            });
        }
        $http.post(uriUsuarios, usuariosEliminar).then(function (response) {

            $rootScope.$emit("CallParentMethodUsuario", response.data);

        });

    };

    $scope.guardarPerfilLlave = function () {
        var documentoLlave = [];
        for (var i = 0; i < $scope.seleccionLlaves.length; i++) {
            documentoLlave.push({
                "idPerfil": $scope.perfilesUsuarios,
                "id_llaves": $scope.seleccionLlaves[i]});
        }
        $http.post(uriLlaves, documentoLlave).then(function (response) {
        });
        $route.reload();
    };

    $scope.perfilesUsuarios = [{}];
    $scope.listarPerfilesUsuario = function (usuario) {
        $http.get(uriPerfiles + usuario).then(function (response) {
            $scope.selectedPerfil = response.data;
        });
    };
    $scope.$watch('selectedPerfil', function (nowSelected) {
        $scope.perfilesUsuarios = [];
        if (!nowSelected) {
            return;
        }
        angular.forEach(nowSelected, function (val) {
            $scope.perfilesUsuarios.push(val.id_perfil);

        });
    });


    $scope.seleccionLlaves = [{}];
    $scope.listarPerfilesLlaves = function (perfil) {
        $http.get(uriLlaves + perfil).then(function (response) {
            $scope.selectedllaves = response.data;
        });
    };

    $scope.$watch('selectedllaves', function (nowSelected) {
        $scope.seleccionLlaves = [];
        if (!nowSelected) {
            return;
        }
        angular.forEach(nowSelected, function (val) {
            $scope.seleccionLlaves.push(val.id_llaves);

        });
    });

//Metodos modal usuario - perfil
    $scope.user = [];

    $scope.AbrirUsuario = function (size) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalUsuario.html',
            controller: 'ModalUsuarioCtrl',
            size: size,
            resolve: {
                user: function () {
                    return $scope.user;
                }
            }
        });
    };

    $scope.AbrirUsuarioModificar = function (usuario) {

        $rootScope.usuario = usuario;

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalUsuarioModificar.html',
            controller: 'ModalUsuarioModificarCtrl',
            resolve: {
                user: function () {
                    return $scope.user;
                }
            }
        });
    };

    $scope.AbrirPerfilModificar = function (perfil) {

        $rootScope.perfil = perfil;

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalPerfilModificar.html',
            controller: 'ModalPerfilModificarCtrl',
            resolve: {
                user: function () {
                    return $scope.user;
                }
            }
        });
    };

    $scope.perfil = [];

    $scope.AbrirPerfil = function (size) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ModalPerfil.html',
            controller: 'ModalPerfilCtrl',
            size: size,
            resolve: {
                perfil: function () {
                    return $scope.perfil;
                }
            }
        });
    };

});


//Controladores modal usuario - perfil
menuApp.controller('ModalUsuarioCtrl', function ($scope, $uibModalInstance, user, $http, $rootScope, $route, MY_WSREST) {

    var uriPerfilUsuario = MY_WSREST.uriRest + "valech/api/perfilusuario/";

    $scope.ok = function (crearUsuario, perfiles) {
        var usuario = {'correo': crearUsuario.correo, 'nombre': crearUsuario.nombreUsuario, 'password': crearUsuario.password, 'username': crearUsuario.username};
        var documentoPerfil = [];
        var perfilesSeleccionados = [];
        perfilesSeleccionados = perfiles;
        for (var i = 0; i < perfilesSeleccionados.length; i++) {
            documentoPerfil.push({
                "usuario": usuario,
                "id_perfil": perfilesSeleccionados[i]});
        }
        $http.post(uriPerfilUsuario, documentoPerfil).then(function (response) {
            $rootScope.$emit("CallParentMethodUsuario", response.data);
        });
        $uibModalInstance.close();

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.tipo = "password";
    $scope.cambiarTipo = function () {

        if ($scope.tipo === "password") {
            $scope.tipo = "text";
        } else {
            $scope.tipo = "password";
        }

    };

});

menuApp.controller('ModalPerfilCtrl', function ($scope, $uibModalInstance, perfil, $http, $rootScope, MY_WSREST) {

    var uriLlaves = MY_WSREST.uriRest + "valech/api/llave/";

    $scope.ok = function (crearPerfil) {

        var documentoLlave = [];
        $scope.perfilExiste = false;



        for (var i = 0; i < $scope.seleccionLlaves.length; i++) {
            documentoLlave.push({
                "nombre_perfil": crearPerfil.nombrePerfil,
                "id_llaves": $scope.seleccionLlaves[i]});
        }


        $http.post(uriLlaves, documentoLlave).then(function (response) {
            $rootScope.$emit("CallParentMethodPerfil", response.data);
        });
        $uibModalInstance.close();


    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

menuApp.controller('ModalUsuarioModificarCtrl', function ($scope, $uibModalInstance, user, $http, $rootScope, MY_WSREST) {

    var uriUsuarios = MY_WSREST.uriRest + "valech/api/usuario/";
    var uriPerfilUsuario = MY_WSREST.uriRest + "valech/api/perfilusuario/";



    $scope.crearUsuario = {};
    $scope.perfilesAsignados = [];
    if ($rootScope.usuario.length === 1) {


        $http.get(uriUsuarios + $scope.usuario[0]).then(function (response) {
            var usuarioSelected = response.data;

            $scope.crearUsuario.nombre = usuarioSelected.nombre;
            $scope.crearUsuario.username = usuarioSelected.username;
            $scope.crearUsuario.password = usuarioSelected.password;
            $scope.crearUsuario.correo = usuarioSelected.correo;

            for (var i = 0; i < usuarioSelected.perfil_usuario.length; i++) {

                $scope.perfilesAsignados.push(usuarioSelected.perfil_usuario[i].id_perfil);


            }

            $scope.perfilesUsuarios = $scope.perfilesAsignados;

        });

    }

    var documentoPerfil = [];
    $scope.modificarUsuario = function (usuario, perfiles) {

        for (var i = 0; i < perfiles.length; i++) {
            documentoPerfil.push({
                "nombre": usuario.nombreUsuario,
                "usuario": usuario,
                "id_usuario": $rootScope.usuario,
                "id_perfil": perfiles[i]});
        }

        $http.post(uriPerfilUsuario, documentoPerfil).then(function (response) {

            $rootScope.$emit("CallParentMethodUsuario", response.data);

        });
        $uibModalInstance.close();


    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.tipo = "password";
    $scope.cambiarTipo = function () {

        if ($scope.tipo === "password") {
            $scope.tipo = "text";
        } else {
            $scope.tipo = "password";
        }

    };



});

menuApp.controller('ModalPerfilModificarCtrl', function ($scope, $uibModalInstance, user, $http, $rootScope, MY_WSREST) {

    var uriLlaves = MY_WSREST.uriRest + "valech/api/llave/";
    $scope.crearPerfil = {};
    $scope.llavesAsignadas = [];
    if ($rootScope.perfil.length === 1) {

        $http.get(uriLlaves + $rootScope.perfil[0]).then(function (response) {
            var perfilesSelected = response.data;
            $scope.crearPerfil.nombrePerfil = perfilesSelected[0].nombre_perfil;

            for (var i = 0; i < perfilesSelected.length; i++) {

                $scope.llavesAsignadas.push(perfilesSelected[i].id_llaves);


            }

            $scope.seleccionLlaves = $scope.llavesAsignadas;

        });

    }

    $scope.modificarPerfil = function (perfil, llaves) {

        var documentoLlave = [];
        for (var i = 0; i < $scope.seleccionLlaves.length; i++) {
            documentoLlave.push({
                "idPerfil": $rootScope.perfil,
                "id_llaves": $scope.seleccionLlaves[i],
                "nombre_perfil": perfil
            });

        }

        $http.post(uriLlaves, documentoLlave).then(function (response) {
            $rootScope.$emit("CallParentMethodPerfil", response.data);
        });

        $uibModalInstance.close();




    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


});