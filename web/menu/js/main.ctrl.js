/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


menuApp.filter('keyboardShortcut', function ($window) {
    return function (str) {
        if (!str)
            return;
        var keys = str.split('-');
        var isOSX = /Mac OS X/.test($window.navigator.userAgent);

        var seperator = (!isOSX || keys.length > 2) ? '+' : '';

        var abbreviations = {
            M: isOSX ? '' : 'Ctrl',
            A: isOSX ? 'Option' : 'Alt',
            S: 'Shift'
        };

        return keys.map(function (key, index) {
            var last = index == keys.length - 1;
            return last ? key : abbreviations[key];
        }).join(seperator);
    };
});

menuApp.config(function ($mdIconProvider) {
    $mdIconProvider
            .iconSet("call", 'resources/img/icons/sets/communication-icons.svg', 24)
            .iconSet("social", 'resources/img/icons/sets/social-icons.svg', 24);
});


menuApp.controller('DemoBasicCtrl', function DemoCtrl($mdDialog, $rootScope, $scope, $location, $route) {
    $rootScope.llaves = [];

    $scope.cerrarSession = function () {
        $rootScope.loggedUser = null;
        $rootScope.llaves = null;

        $rootScope.adminmenu = false;
        $rootScope.planmenu = false;
        $rootScope.incamenu = false;
        $rootScope.bucamenu = false;
        $rootScope.edicplan = false;
        $rootScope.lectplan = false;
        $rootScope.ediccarp = false;
        $rootScope.lectcarp = false;
        $rootScope.creacarp = false;
        $rootScope.carpmenu = false;
        $rootScope.sesion = false;


        $location.path('login');

    }

    this.settings = {
        printLayout: true,
        showRuler: true,
        showSpellingSuggestions: true,
        presentationMode: 'edit'
    };

    this.sampleAction = function (name, ev) {
        $mdDialog.show($mdDialog.alert()
                .title(name)
                .textContent('You triggered the "' + name + '" action')
                .ok('Great')
                .targetEvent(ev)
                );
    };
    this.direcciona = function (name, ev) {
        $mdDialog.show($mdDialog.alert()
                .title(name)
                .textContent('You triggered the "' + name + '" action')
                .ok('Great')
                .targetEvent(ev)
                );
    };

});



menuApp.config(function ($routeProvider) {


    //Aquí se configuran las rutas, se le indica el controlador a usar y la vista a mostrar
    //Indicamos que si no se le indica una ruta o la ruta no es correcta nos llevará
    //a una vista por defecto.
    $routeProvider.when('/busquedaDescalificadas', {
        templateUrl: 'carpetas/busquedaDesclasificadas/busquedaDesclasificada.html',
        controller: 'controllerBusquedaDesclasificada'
    });
    $routeProvider.when('/descalificarCarpetas', {
        templateUrl: 'carpetas/descalificaCarpetas/descalificaCarpetas.html',
        controller: 'controllerDescalificar'
    });
    $routeProvider.when('/busquedaCarpetas', {
        templateUrl: 'carpetas/busquedaCarpetas/busquedaCarpetas.html',
        controller: 'tablaCtrl'
    });
    $routeProvider.when('/ingresoCarpetas', {
        templateUrl: 'carpetas/ingresoCarpetas/ingresoDocumento.html',
        controller: 'docCtrl'
    });
    $routeProvider.when('/modificaCarpetas', {
        templateUrl: 'carpetas/ModificaCarpetas/ModificaCarpetas.html',
        controller: 'modDocCtrl'
    });
    $routeProvider.when('/usuarioperfil', {
        templateUrl: 'perfilamiento/perfilesUsuario/perfilamiento.html',
        controller: 'appCtrl'
    });
    $routeProvider.when('/moduloperfil', {
        templateUrl: 'perfilamiento/perfilesModulo/perfilmoduloaccion.html',
        controller: 'appCtrl'
    });
    $routeProvider.when('/categoria', {
        templateUrl: 'categoria/categoria.html',
        controller: 'Ctrl'
    });
    $routeProvider.when('/login', {
        templateUrl: 'login/login.html',
        controller: 'loginCtrl'
    });
    $routeProvider.when('/landing', {
        templateUrl: 'landing/landing.html',
        controller: ''
    });
    $routeProvider.otherwise({redirectTo: '/login'});
});

menuApp.run(function ($rootScope, $location) {

    // register listener to watch route changes
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        if ($rootScope.loggedUser == null) {
            // no logged user, we should be going to #login
            if (next.templateUrl == "login/login.html") {
                // already going to #login, no redirect needed
            } else {
                // not going to #login, we should redirect now
                $location.path("/login");
            }
        }
    });
})

menuApp.controller('AddOrderController', function ($scope) {

    $scope.message = 'This is Add new order screen';

});


menuApp.controller('ShowOrdersController', function ($scope) {

    $scope.message = 'This is Show orders screen';

});

