menuApp.controller("Ctrl", function ($scope, $http, $mdDialog, MY_WSREST) {

    var uriCategorias = MY_WSREST.uriRest + "valech/api/categoria/";
    var uriSubCategorias = MY_WSREST.uriRest + "valech/api/tipodocumento/";
    var uriPlantilla = MY_WSREST.uriRest + "valech/api/plantilla/";
    listarCategorias();
    listarPlantilla();
    $scope.estadoCategoria = false;


    var categorias = [];
    function listarCategorias() {
    delete $http.defaults.headers.common['X-Requested-With'];
        return $http({
            method: 'GET',
            url: uriCategorias,
            dataType: 'jsonp',
         }).then(function (response) {
                categorias = response.data;
            $scope.categorias = categorias;
            }, function (response) {
                //fail case
            });
        
        
//        $http.get(uriCategorias).then(function (response) {
//            categorias = response.data;
//            $scope.categorias = categorias;
//        });
    }


    var plantilla = [];
    function listarPlantilla() {
        $http.get(uriPlantilla).then(function (response) {
            plantilla = response.data;
            $scope.plantillas = plantilla;
        });
    }


    $scope.agregarSubCategoria = function (categoria) {
        var subCategorias = [];
        $http.get(uriSubCategorias + categoria).then(function (response) {
            subCategorias = response.data;
            $scope.tipoDocumento = subCategorias;
        });
    }

    function buscarSubcategorias(categoria) {
        var subCategorias = [];
        $http.get(uriSubCategorias + categoria).then(function (response) {
            subCategorias = response.data;
            $scope.tipoDocumento = subCategorias;
        });
    }


    $scope.agregarCategoria = function (seleccionSub) {
        
        var sub = {'tipo_id': seleccionSub.tipo_id, 'nombreTipoDocumento': seleccionSub.nombreTipoDocumento};
        $http.post(uriPlantilla, sub).then(function (response) {
            $scope.pintarPlantillas();
        });
    };
    $scope.pintarPlantillas = function () {
        $http.get(uriPlantilla).then(function (response) {
            plantilla = response.data;
            $scope.plantillas = plantilla;
        });
    }

    $scope.eliminarplantilla = function (id) {
        $http.delete(uriPlantilla + id.id_plantilla).then(function (response) {
            $scope.pintarPlantillas();
        });
    };
    $scope.crearCategoria = function (nombre) {
        
        var categoria = {'nombreCategoria': nombre};
        if (nombre !== undefined) {
            $http.post(uriCategorias, categoria).then(function (response) {
                listarCategorias();
                $scope.addCat = '';
            });
        }

    };
    $scope.crearSubCategoria = function (nombre, categoria) {
        var subcategoria = {'nombreTipoDocumento': nombre, 'categoria_id': categoria};
        if (nombre !== undefined) {
            $http.post(uriSubCategorias, subcategoria).then(function (response) {
                buscarSubcategorias(categoria);
                $scope.addSub = '';
            });
        }

    };

    $scope.eliminarCategoria = function (categoria) {
        $http.delete(uriCategorias + categoria).then(function (response) {
            listarCategorias();
            buscarSubcategorias(0);
            $scope.pintarPlantillas();
        });
    };

    $scope.modificarCategoria = function (categoria, nombre, estado, valorCat) {

        var catMod = {'nombreCategoria': nombre};
        $scope.addCat = valorCat;
        if (estado) {
            $http.put(uriCategorias + categoria, catMod).then(function (response) {
                listarCategorias();
                $scope.addCat = '';
            });
        }


    };

    $scope.eliminarSubCategoria = function (subcategoria, categoria) {
        $http.delete(uriSubCategorias + subcategoria).then(function (response) {
            listarCategorias();
            buscarSubcategorias(categoria);
            $scope.pintarPlantillas();
        });
    };




});
