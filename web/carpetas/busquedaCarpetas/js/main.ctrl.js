menuApp.controller('tablaCtrl', ['$scope', '$document', '$http', '$rootScope', '$uibModal', '$route', 'MY_WSREST', function ($scope, $document, $http, $rootScope, $uibModal, $route, MY_WSREST) {

        //Servicio Rest asociado a la obtención de datos de carpeta (La carpeta no trae documentos)
        var uriCarpeta = MY_WSREST.uriRest + "valech/api/carpeta/";
        var uriTipoDocumentos = MY_WSREST.uriRest + "valech/api/tipodocumento/";
        //Trae las categorias
        $http.get(uriTipoDocumentos).then(function (response) {
            console.log(response.data);
            $scope.lista_categoria = response.data;
        });
        
        $scope.busqueda = {};
       
    
    $scope.aplicaCategoria = function(i){
           console.log(i);
           $scope.busqueda.tipo_id = i.tipo_id;
    };
        //$scope.comisiones = [{nombre: 'V - I', id: 0}, {nombre: 'V - II', id: 1}];

        $scope.tipo_documentos = [
            {descripcion: "Persona", valor: true},
            {descripcion: "Administrativo", valor: false}
        ];

        $scope.currentPage = 0; //Pagina inicial
        var pageSize = 13; // Esta la cantidad de paginas que traera el paginador
        $scope.pages = [];
        var busquedas = [];
        var busqueda = [];
        var itemArray = [];
        $scope.nextPage = 2;
        $scope.backPage = 0;

        //Metodo de recarga de pagina
        $scope.reloadRoute = function () {
            $route.reload();
        };

        //Metodo para redirigir a otra pagina, se le entrega el route de la pagina
        $scope.changeRoute = function (url, forceReload) {
            $scope = $scope || angular.element(document).scope();
            if (forceReload || $scope.$$phase) { // that's right TWO dollar signs: $$phase
                window.location = url;
            } else {
                $location.path(url);
                $scope.$apply();
            }
        };

        //En caso que venga una busqueda cargada, a nivel global, se le entrega como parametro al
        //servicio rest para que liste esa busqueda, caso contrario traera todos los registros.
        if ($rootScope.busquedas !== undefined) {
            busquedas = $rootScope.busquedas;
        }

        //Obtención de todos los registros al cargar el el desliegue, al existir una busqueda vigente
        //se lista lo de la busqueda, señalada en 'busquedas' 
        //Inicia Busqueda y paginación
        $http.put(uriCarpeta + "carpetaPaginacion/", busquedas).then(function (response) {
            $scope.busqueda = response.data;
            $scope.pages = [];
            var ini;
            var fin;
            $scope.final = $scope.busqueda.numeropaginas;
            if ($scope.busqueda.numeropaginas < pageSize) {
                ini = 1;
                fin = $scope.busqueda.numeropaginas;
            } else {
                ini = 1;
                fin = pageSize;
            }

            for (var i = ini; i < fin; i++) {
                $scope.pages.push({no: i});

            }
            if ($scope.busqueda.numeropaginas > 100) {
                $scope.pages.push({no: 100});
            }
            if ($scope.busqueda.numeropaginas > 1000) {
                $scope.pages.push({no: 1000});
            }
            if ($scope.busqueda.numeropaginas > 10000) {
                $scope.pages.push({no: 10000});
            }
            $scope.pages.push({no: $scope.busqueda.numeropaginas});
        });
        
        console.log('BUSQUEDAS');
        console.log(busquedas);
        $http.put(uriCarpeta + "carpetaPaginada/", busquedas).then(function (response) {
            $scope.cabecera = [];
            itemArray = response.data;
            $scope.itemArray = response.data;
            for (var i = 0; i < $scope.itemArray.length; i++) {
                $scope.data = $scope.itemArray[i].carpeta;
                $scope.cabecera.carpeta_id = $scope.itemArray[i].carpeta;
            }

        });
        //Inicia Busqueda y paginación

        //Al regalizar una busqueda especifica se utiliza este metodo.
        //Inicio Funcion Buscar
        $scope.buscar = function (busqueda, categorias) {
            
            if(categorias.length > 0){
            var cat = '(';
                for(i = 0; i < categorias.length; i++){
                       console.log(i);
                    cat += categorias[i].id;
                    if(i +1 < categorias.length){
                        cat+= ', ';
                    }else{
                        cat+=')';
                    }
                }
            }else{
                var cat = null;
            }
            
            $scope.currentPage = 0;
             if(busqueda.letra_caja === ''){
                busqueda.letra_caja = undefined;
            }
            if(busqueda.letra_carpeta === ''){
                busqueda.letra_carpeta = undefined;
            }
            if(busqueda.tipo_id === ''){
                busqueda.tipo_id = undefined;
            }
            var busquedas = {
                "serie": busqueda.serie || null,
                "carpeta": busqueda.carpeta || null,
                'letra_carpeta': busqueda.letra_carpeta || null,
                "carpeta_id": busqueda.carpeta || null,
                "nro_caja": busqueda.nro_caja || null,
                'letra_caja': busqueda.letra_caja || null,
                "rut": busqueda.rut || null,
                "nombres": busqueda.nombres || null,
                "apellidos": busqueda.apellidos || null,
                "identificador": busqueda.identificador || null,
                "comision": busqueda.comision || null,
                "tipo_id": cat || null,
                "tipo_carpeta": busqueda.tipo_carpeta || null
            };
            busqueda.tipo_id = cat;
            console.log('busquedas');
            console.log(busquedas);
            $rootScope.busquedas = busqueda;

            $scope.reloadRoute();
        };


        //Limpia la Busqueda
        $scope.limpiarBusqueda = function () {
            if ($scope.busqueda !== undefined) {
                $scope.busqueda = {};
            }
            $rootScope.busquedas = undefined;
            busquedas = [];
            $scope.reloadRoute();

        };


        //Función que setea el numero de pagina
        //Revisa el hacia adelante y hacia atras
        $scope.setPage = function (index) {
            $scope.currentPage = 0;
            $scope.pages = [];
            if ($rootScope.busquedas !== undefined) {
                console.log("$rootScope.busquedas");
                console.log($rootScope.busquedas);
                var busquedas = {
                    "califica": $rootScope.busquedas.califica,
                    "serie": $rootScope.busquedas.serie,
                    "carpeta": $rootScope.busquedas.carpeta,
                    "letra_carpeta": $rootScope.busquedas.letra_carpeta,
                    "carpeta_id": $rootScope.busquedas.carpeta,
                    "nro_caja": $rootScope.busquedas.nro_caja,
                    "letra_caja": $rootScope.busquedas.letra_caja,
                    "rut": $rootScope.busquedas.rut,
                    "nombres": $rootScope.busquedas.nombres,
                    "apellidos": $rootScope.busquedas.apellidos,
                    "comision": $rootScope.busquedas.comision,
                    "tipo_carpeta": $rootScope.busquedas.tipo_carpeta,
                    "tipo_id": $rootScope.busquedas.tipo_id,
                    "numeropdf": (index)
                };
            } else {
                var busquedas = {
                    "califica": busqueda.califica,
                    "serie": busqueda.serie,
                    "carpeta": busqueda.carpeta,
                    "letra_carpeta": busqueda.letra_carpeta,
                    "carpeta_id": busqueda.carpeta,
                    "nro_caja": busqueda.nro_caja,
                    "letra_caja": busqueda.letra_caja,
                    "rut": busqueda.rut,
                    "nombres": busqueda.nombres,
                    "apellidos": busqueda.apellidos,
                    "comision": busqueda.comision,
                    "tipo_carpeta": busqueda.tipo_carpeta,
                    "numeropdf": (index)
                };
            }


            if ($scope.busqueda.numeropaginas < pageSize) {
                ini = 1;
                fin = $scope.busqueda.numeropaginas;
            } else {
                if ((index - 10) > 0 && (index + pageSize) <= $scope.busqueda.numeropaginas) {


                    ini = index;
                    fin = index + pageSize;
                } else {
                    ini = 1;
                    fin = pageSize;
                }

            }

            for (var i = ini; i < fin; i++) {
                $scope.pages.push({no: i});

            }
            $scope.currentPage = index-1;
            if ($scope.busqueda.numeropaginas > 100 && $scope.currentPage < 100) {
                $scope.pages.push({no: 100});
            }
            if ($scope.busqueda.numeropaginas > 1000 && $scope.currentPage < 1000) {
                $scope.pages.push({no: 1000});
            }
            if ($scope.busqueda.numeropaginas > 10000 && $scope.currentPage < 10000) {
                $scope.pages.push({no: 10000});
            }

            $scope.nextPage = index + 1;
            $scope.backPage = index - 1;
            $scope.pages.push({no: $scope.busqueda.numeropaginas});
            
            $http.put(uriCarpeta + "carpetaPaginada/", busquedas).then(function (response) {
                $scope.cabecera = [];
                itemArray = response.data;
                $scope.itemArray = response.data;
                for (var i = 0; i < $scope.itemArray.length; i++) {
                    $scope.data = $scope.itemArray[i].carpeta;
                    $scope.cabecera.carpeta_id = $scope.itemArray[i].carpeta;
                }

            });
        };



        //Funcion para eliminar un valor del listado
        $scope.eliminar = function (valor) {
            var cabecera = {"nombreDocumento": 'nombreDocumento'};
            $http.post(uriCarpeta + valor, 1).then(function (response) {
            });
            $route.reload();
        };


        //Modificación de carpeta, obtinene los datos importantes para reconocer una carpeta
        //Los datos conforman la llave primaria y luego redirige hacia la pagina de ingreso
        //Ahora a la pagina de modificación
        $scope.modificarCarpeta = function (carpeta_id, carpeta, caja, comision, califica) {
            $rootScope.id_carpeta = carpeta_id;
            $rootScope.carpeta = carpeta;
            $rootScope.nro_caja = caja;
            $rootScope.comision = comision;
            $rootScope.califica = califica;
            $rootScope.revalidador = true;
            $rootScope.rehabilitado = true;
            $scope.changeRoute('#modificaCarpetas');
        };


        //Modal de Eliminación, verifica la eliminación de algun recurso
        $scope.AbrirEliminarModal = function (valor) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'EliminarModal.html',
                controller: 'EliminarModalCtrl',
                resolve: {
                }
            });
            $rootScope.eliminarcarpeta = valor;
        };

        //Función reload llamada desde el modal al eliminar un registro
        $rootScope.$on('Reload', function (event, reload) {
            if (reload) {
                $route.reload();
            }
        });


        //Metodos de Orden, talvez sean quitados
        //////////////////////////////////////////////////////
        function limpiarOrden() {
            $scope.ordenCaja = "";
            $scope.ordenCarpeta = "";
            $scope.ordenRut = "";
            $scope.ordenNombre = "";
            $scope.ordenApellido = "";
            $scope.ordenPagPdf = "";
            $scope.ordenDocs = "";
            $scope.ordenPag = "";
            $scope.ordenestado = "";
            $scope.ordenComision = "";
            $scope.ordenCalifica = "";
        }


        $scope.ordenarPorCaja = function (orden) {
            limpiarOrden();
            $scope.ordenCaja = orden;
            if ($scope.hidecaja !== true) {
                $scope.hidecaja = true;
                $scope.hidecaja1 = false;
            } else {
                $scope.hidecaja = false;
                $scope.hidecaja1 = true;
            }
        };

        $scope.ordenarPorCarpeta = function (orden) {
            limpiarOrden();
            $scope.ordenCarpeta = orden;
            if ($scope.hidecarpeta !== true) {
                $scope.hidecarpeta = true;
                $scope.hidecarpeta1 = false;
            } else {
                $scope.hidecarpeta = false;
                $scope.hidecarpeta1 = true;
            }
        };

        $scope.ordenarPorRut = function (orden) {
            limpiarOrden();
            $scope.ordenRut = orden;
            if ($scope.hiderut !== true) {
                $scope.hiderut = true;
                $scope.hiderut1 = false;
            } else {
                $scope.hiderut = false;
                $scope.hiderut1 = true;
            }
        };

        $scope.ordenarPorNombre = function (orden) {
            limpiarOrden();
            $scope.ordenNombre = orden;
            if ($scope.hidenombre !== true) {
                $scope.hidenombre = true;
                $scope.hidenombre1 = false;
            } else {
                $scope.hidenombre = false;
                $scope.hidenombre1 = true;
            }
        };

        $scope.ordenarPoApellido = function (orden) {
            limpiarOrden();
            $scope.ordenApellido = orden;
            if ($scope.hideapellido !== true) {
                $scope.hideapellido = true;
                $scope.hideapellido1 = false;
            } else {
                $scope.hideapellido = false;
                $scope.hideapellido1 = true;
            }
        };

        $scope.ordenarPorPagPdf = function (orden) {
            limpiarOrden();
            $scope.ordenPagPdf = orden;
            if ($scope.hidepagpdf !== true) {
                $scope.hidepagpdf = true;
                $scope.hidepagpdf1 = false;
            } else {
                $scope.hidepagpdf = false;
                $scope.hidepagpdf1 = true;
            }
        };

        $scope.ordenarPorDocs = function (orden) {
            limpiarOrden();
            $scope.ordenDocs = orden;
            if ($scope.hidedocs !== true) {
                $scope.hidedocs = true;
                $scope.hidedocs1 = false;
            } else {
                $scope.hidedocs = false;
                $scope.hidedocs1 = true;
            }
        };

        $scope.ordenarPorPag = function (orden) {
            limpiarOrden();
            $scope.ordenPag = orden;
            if ($scope.hidepag !== true) {
                $scope.hidepag = true;
                $scope.hidepag1 = false;
            } else {
                $scope.hidepag = false;
                $scope.hidepag1 = true;
            }
        };

        $scope.ordenarPorEstado = function (orden) {
            limpiarOrden();
            $scope.ordenestado = orden;
            if ($scope.hideestado !== true) {
                $scope.hideestado = true;
                $scope.hideestado1 = false;
            } else {
                $scope.hideestado = false;
                $scope.hideestado1 = true;
            }
        };

        $scope.ordenarPoComision = function (orden) {
            limpiarOrden();
            $scope.ordenComision = orden;
            if ($scope.hidecomision !== true) {
                $scope.hidecomision = true;
                $scope.hidecomision1 = false;
            } else {
                $scope.hidecomision = false;
                $scope.hidecomision1 = true;
            }
        };

        $scope.ordenarPoCalifica = function (orden) {
            limpiarOrden();
            $scope.ordenCalifica = orden;
            if ($scope.hidecalifica !== true) {
                $scope.hidecalifica = true;
                $scope.hidecalifica1 = false;
            } else {
                $scope.hidecalifica = false;
                $scope.hidecalifica1 = true;
            }
        };
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        
        $scope.example14model = [];
    $scope.example14settings = {
        scrollableHeight: '200px',
        scrollable: true,
        enableSearch: true
    };
    
   
        
    $scope,mostrarSeleccion = function(){
        console.log($scope.example14model);
    };
        
        
        
        

    }]);

menuApp.controller('EliminarModalCtrl', function ($scope, $uibModalInstance, $http, $rootScope, MY_WSREST) {
    var urielimina = MY_WSREST.uriRest + "valech/api/carpeta/";
    var valor = $rootScope.eliminarcarpeta;
    $scope.acepto = function () {

        $http.delete(urielimina + valor).then(function (response) {
            $rootScope.$emit('Reload', response.data);
        });
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

menuApp.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});